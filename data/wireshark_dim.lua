-- Wireshark plugin for DIM protocol

dim_dns_proto = Proto("DIM_DNS", "DIM DNS Protocol")
dim_dic_proto = Proto("DIM_DIC", "DIM Client Protocol")
dim_dis_proto = Proto("DIM_DIS", "DIM Server Protocol")

dim_servers = {}

function mkuri(addr, port)
    return tostring(addr) .. ":" .. port
end

function add_server(addr, port)
    dim_servers[mkuri(addr, port)] = true
end

function flush(buffer, offset, tree)
    if offset > 0 then
        local range = buffer(0, offset)
        tree:add(range, "Unknown")
        return buffer(offset):tvb()
    end
    return buffer
end

function check_buffer(buffer, size, pinfo)
    if buffer:len() < size then
        pinfo.desegment_len = size - buffer:len()
        pinfo.desegment_offset = 0
        return false
    end
    return true
end

function is_dna_hdr(buffer, offset)
    return buffer:len() >= offset + 12 and
        buffer(8 + offset, 4):le_uint() == 0xfeadfead
end

function dna_pkt(buffer, tree)
    local dna = tree:add(buffer(0, 12), "DNA Header")
    dna:add(buffer(0, 4), "Size: " .. buffer(0, 4):le_uint())
    dna:add(buffer(4, 4), "Data Size: " .. buffer(4, 4):le_uint())
    dna:add(buffer(8, 4), string.format("Magic: 0x%.8X", buffer(8, 4):le_uint()))
    return buffer(12):tvb()
end

function is_dna_tst(buffer, offset)
    return buffer:len() >= offset + 12 and
        buffer(8 + offset, 4):le_uint() == 0x11131517
end

function dna_tst_pkt(buffer, tree)
    local dna = tree:add(buffer(0, 12), "DNA Test")
    dna:add(buffer(0, 4), "Size: " .. buffer(0, 4):le_uint())
    dna:add(buffer(4, 4), "Data Size: " .. buffer(4, 4):le_uint())
    dna:add(buffer(8, 4), string.format("Magic: 0x%.8X", buffer(8, 4):le_uint()))
    return buffer(12):tvb()
end

function is_dna_net(buffer, offset)
    return buffer:len() >= offset + 84 and
        buffer(0, 4):le_uint() == 0xc0dec0de
end

function dna_net_pkt(buffer, tree)
    local sub = tree:add(buffer(0, 84), "DNA Net")
    sub:add(buffer(0, 4), string.format("Code: 0x%.8X", buffer(0, 4):le_uint()))
    sub:add(buffer(4, 40), "Node: " .. buffer(4, 40):string())
    sub:add(buffer(44, 40), "Task: " .. buffer(44, 40):string())
    return buffer(84):tvb()
end

function is_dic_dns(buffer, offset)
    return buffer:len() >= 144 + offset and buffer(4, 4):le_uint() == 2 and
        buffer(0, 4):le_uint() == 144
end

e_srcType = {
    [0] = "None",
    [1] = "DIS",
    [2] = "DIC",
    [3] = "DNS",
    [4] = "DNA",
    [5] = "USR"
}

function dic_dns_pkt(buffer, tree)
    local sub = tree:add(buffer(0, 144), "DIC -> DNS")
    sub:add(buffer(0, 4), "Size: " .. buffer(0, 4):le_uint())
    sub:add(buffer(4, 4), "SrcType: " .. (e_srcType[buffer(4, 4):le_uint()] or "UNKNOWN"))
    sub:add(buffer(8, 132), "Service: " .. buffer(8, 132):string())
    sub:add(buffer(140, 4), "ServiceID: " .. buffer(140, 4):le_uint())
    return buffer(144):tvb()
end

function is_dis_dns(buffer, offset)
    return buffer:len() >= 108 + offset and buffer(4, 4):le_uint() == 1 and
        buffer(0, 4):le_uint() == (buffer(104, 4):le_uint() * 268 + 108)
end

function dis_dns_pkt(buffer, pinfo, tree)
    local size = buffer(0, 4):le_uint()
    if not check_buffer(buffer, size, pinfo) then
        return buffer(buffer:len())
    end

    local sub = tree:add(buffer(0, size), "DIS -> DNS")
    sub:add(buffer(0, 4), "Size: " .. size)
    sub:add(buffer(4, 4), "SrcType: " .. (e_srcType[buffer(4, 4):le_uint()] or "UNKNOWN"))
    sub:add(buffer(8, 40), "Node: " .. buffer(8, 40):string())
    sub:add(buffer(48, 36), "Task: " .. buffer(48, 36):string())
    sub:add(buffer(84, 4), "Address: " .. tostring(buffer(84, 4):ipv4()))
    sub:add(buffer(88, 4), "Pid: " .. buffer(88, 4):le_uint())
    sub:add(buffer(92, 4), "Port: " .. buffer(92, 4):le_uint())
    sub:add(buffer(96, 4), "Protocol: " .. buffer(96, 4):le_uint())
    sub:add(buffer(100, 4), string.format("Format: 0x%.8X", buffer(100, 4):le_uint()))
    sub:add(buffer(104, 4), "Nb. Services: " .. buffer(104, 4):le_uint())
    add_server(buffer(84, 4):ipv4(), buffer(92, 4):le_uint())
    add_server(pinfo.src, buffer(92, 4):le_uint())

    local nb_srv = buffer(104, 4):le_uint()
    if (nb_srv == 0) then
        return buffer(108):tvb()
    end

    local offset = 108
    sub = sub:add(buffer(108, nb_srv * 268), "Services")
    while (buffer:len() - offset >= 268) do
        local srv = sub:add(buffer(offset, 268), "Service: " .. buffer(offset, 132):string())
        srv:add(buffer(offset, 132), "Name: " .. buffer(offset, 132):string())
        srv:add(buffer(offset + 132, 4), "ServiceID: " .. buffer(offset + 132, 4):le_uint())
        srv:add(buffer(offset + 136, 132), "Definition: " .. buffer(offset + 136, 132):string())
        offset = offset + 268
    end
    return buffer(offset):tvb()
end

e_dnsDisTypes = {
    [0] = "REGISTER",
    [1] = "KILL",
    [2] = "STOP",
    [3] = "EXIT",
    [4] = "SOFT_EXIT"
}

function is_dns_dis(buffer, offset)
    return buffer:len() >= 8 + offset and e_dnsDisTypes[buffer(4, 4):le_uint()]
        and buffer(0, 4):le_uint() == 8
end

function dns_dis_pkt(buffer, tree)
    local size = buffer(0, 4):le_uint()
    if size < buffer:len() then
        -- XXX: there's extra data but size is wrong
        size = buffer:len()
    end

    local sub = tree:add(buffer(0, size), "DNS -> DIS")
    sub:add(buffer(0, 4), "Size: " .. buffer(0, 4):le_uint())
    sub:add(buffer(4, 4), "Type: " .. (e_dnsDisTypes[buffer(4, 4):le_uint()] or "UNKNOWN"))
    if (size > 8) then
        sub:add(buffer(8, size - 8), "Data")
    end
    return buffer(size):tvb()
end

function is_dns_dic(buffer, offset)
    return buffer:len() >= 236 + offset and buffer(0, 4):le_uint() == 236
end

function dns_dic_pkt(buffer, tree)
    local sub = tree:add(buffer(0, 140), "DNS -> DIC")
    sub:add(buffer(0, 4), "Size: " .. buffer(0, 4):le_uint())
    sub:add(buffer(4, 4), "ServiceID: " .. buffer(4, 4):le_uint())
    sub:add(buffer(8, 132), "Definition: " .. buffer(8, 132):string())
    sub:add(buffer(140, 40), "Node: " .. buffer(140, 40):string())
    sub:add(buffer(180, 36), "Task: " .. buffer(180, 36):string())
    sub:add(buffer(216, 4), "Address: " .. tostring(buffer(216, 4):ipv4()))
    sub:add(buffer(220, 4), "Pid: " .. buffer(220, 4):le_uint())
    sub:add(buffer(224, 4), "Port: " .. buffer(224, 4):le_uint())
    sub:add(buffer(228, 4), "Protocol: " .. buffer(228, 4):le_uint())
    sub:add(buffer(232, 4), string.format("Format: 0x%.8X", buffer(232, 4):le_uint()))

    add_server(buffer(216, 4):ipv4(), buffer(224, 4):le_uint())
    return buffer(236):tvb()
end


e_dicTypes = {
    [0x01] = 'ONCE_ONLY',
    [0x02] = 'TIMED',
    [0x04] = 'MONITORED',
    [0x08] = 'COMMAND',
    [0x10] = 'DELETE',
    [0x20] = 'MONIT_ONLY',
    [0x40] = 'UPDATE',
    [0x80] = 'TIMED_ONLY',
    [0x100] = 'MONIT_FIRST'
}
dicTypesMask = 0x1ff
dicTypesStamped = 0x1000

function is_dic_dis(buffer, offset)
    return buffer:len() >= 152 + offset and buffer(0, 4):le_uint() >= 152
        and e_dicTypes[bit.band(buffer(140, 4):le_uint(), dicTypesMask)]
end

function dic_dis_pkt(buffer, pinfo, tree)
    local size = buffer(0, 4):le_uint()
    if not check_buffer(buffer, size, pinfo) then
        return buffer(buffer:len())
    end

    local sub = tree:add(buffer(0, size), "DIC -> DIS")
    sub:add(buffer(0, 4), "Size: " .. size)
    sub:add(buffer(4, 132), "Name: " .. buffer(4, 132):string())
    sub:add(buffer(136, 4), "ServiceID: " .. buffer(136, 4):le_uint())

    local type = buffer(140, 4):le_uint()
    sub:add(buffer(140, 4), "Type: " .. (e_dicTypes[bit.band(type, dicTypesMask)] or "UNKNOWN") ..
        ((bit.band(type, dicTypesStamped) ~= 0) and " STAMPED" or ""))
    sub:add(buffer(144, 4), "Timeout: " .. buffer(144, 4):le_uint())
    sub:add(buffer(148, 4), string.format("Format: 0x%.8X", buffer(148, 4):le_uint()))
    if (size > 152) then
        sub:add(buffer(152, size - 152), "Data")
    end
    return buffer(size):tvb()
end

function is_dis_dic(buffer, offset)
    return buffer:len() >= 8 + offset
end

function is_stamped(buffer)
    return buffer:len() >= 24 and
        (buffer(12, 4):le_uint() == 0xc0dec0de or
         bit.band(buffer(0, 4):le_uint(), 0xFFFF0000) == 0xc0de0000)
end

function dis_dic_pkt(buffer, pinfo, tree)
    local sub = tree:add(buffer(0, size), "DIS -> DIC")
    local size = buffer(0, 4):le_uint()

    if not check_buffer(buffer, size, pinfo) then
        return buffer(buffer:len())
    end

    sub:add(buffer(0, 4), "Size: " .. size)
    sub:add(buffer(4, 4), "ServiceID: " .. buffer(4, 4):le_uint())
    size = size - 8
    buffer = buffer(8):tvb()
    if (size == 0) then
        return buffer
    end
    if (is_stamped(buffer)) then
        sub:add(buffer(0, 8), "TimeStamp: " .. buffer(4, 4):le_uint() ..
            "." .. bit.band(buffer(0, 4):le_uint(), 0x0000FFFF))
        sub:add(buffer(8, 4), "Quality: " .. buffer(8, 4):le_uint())
        sub:add(buffer(12, 12), "Reserved")
        size = size - 24
        buffer = buffer(24):tvb()
    end
    sub:add(buffer(0, size), "Data")
    return buffer(size):tvb()
end

function dim_dns_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "DIM/DNS"
    local tree = tree:add(dim_dns_proto, buffer(), "DIM Protocol Data")
    local offset = 0
    while buffer:len() > offset do
        local found = true
        if is_dna_hdr(buffer, offset) then
            buffer = dna_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_tst(buffer, offset) then
            buffer = dna_tst_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_net(buffer, offset) then
            buffer = dna_net_pkt(flush(buffer, offset, tree), tree)
        elseif is_dic_dns(buffer, offset) then
            buffer = dic_dns_pkt(flush(buffer, offset, tree), tree)
        elseif is_dis_dns(buffer, offset) then
            buffer = dis_dns_pkt(flush(buffer, offset, tree), pinfo, tree)
        elseif is_dns_dis(buffer, offset) then
            buffer = dns_dis_pkt(flush(buffer, offset, tree), tree)
        elseif is_dns_dic(buffer, offset) then
            buffer = dns_dic_pkt(flush(buffer, offset, tree), tree)
        else
            found = false
            offset = offset + 1
        end
        if found then
            offset = 0
        end
    end
    flush(buffer, offset, tree)
end

function dim_dic_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "DIM/DIC"

    local tree = tree:add(dim_dns_proto, buffer(), "DIM Protocol Data")
    local offset = 0
    while buffer:len() > offset do
        local found = true
        if is_dna_hdr(buffer, offset) then
            buffer = dna_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_tst(buffer, offset) then
            buffer = dna_tst_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_net(buffer, offset) then
            buffer = dna_net_pkt(flush(buffer, offset, tree), tree)
        elseif is_dic_dis(buffer, offset) then
            buffer = dic_dis_pkt(flush(buffer, offset, tree), pinfo, tree)
        else
            found = false
            offset = offset + 1
        end
        if found then
            offset = 0
        end
    end
    flush(buffer, offset, tree)
end

function dim_dic_heuristic(buffer, pinfo, tree)
    if dim_servers[mkuri(pinfo.dst, pinfo.dst_port)] then
        dim_dic_proto.dissector(buffer, pinfo, tree)
        return true
    end
    return false
end
dim_dic_proto:register_heuristic("tcp", dim_dic_heuristic)

function dim_dis_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "DIM/DIS"

    local tree = tree:add(dim_dns_proto, buffer(), "DIM Protocol Data")
    local offset = 0
    while buffer:len() > offset do
        local found = true
        if is_dna_hdr(buffer, offset) then
            buffer = dna_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_tst(buffer, offset) then
            buffer = dna_tst_pkt(flush(buffer, offset, tree), tree)
        elseif is_dna_net(buffer, offset) then
            buffer = dna_net_pkt(flush(buffer, offset, tree), tree)
        elseif is_dis_dic(buffer, offset) and
               dim_servers[mkuri(pinfo.src, pinfo.src_port)] then
            -- a reply from a DIM server
            buffer = dis_dic_pkt(flush(buffer, offset, tree), pinfo, tree)
        else
            found = false
            offset = offset + 1
        end
        if found then
            offset = 0
        end
    end
    flush(buffer, offset, tree)
end


function dim_dis_heuristic(buffer, pinfo, tree)
    if dim_servers[mkuri(pinfo.src, pinfo.src_port)] then
        dim_dis_proto.dissector(buffer, pinfo, tree)
        return true
    end
    return false
end
dim_dis_proto:register_heuristic("tcp", dim_dis_heuristic)

table = DissectorTable.get("tcp.port")
table:add(2505, dim_dns_proto)
table:add_for_decode_as(dim_dns_proto)
table:add_for_decode_as(dim_dic_proto)
table:add_for_decode_as(dim_dis_proto)
