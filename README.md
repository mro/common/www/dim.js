# DIM Javascript implementation

Distributed Information Management protocol implementation for Javascript.

## Build

Npm and Node.js are prerequisites to build this library:
```bash
# I'd recommend adding this in your .bashrc
export PATH="./node_modules/.bin:$PATH"

# Install dependencies and tools
npm install
```

### Testing

To run unit-tests (in development mode):
```bash
# This is for additional debugging
export DEBUG=dim:*

# Run unit-tests tests
npm test
```

## Documentation

Some examples and API documentation are [available here](/API.md).
