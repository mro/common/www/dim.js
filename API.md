# DIM.js API documentation

## DIC : DIM Client

This section describes how to fetch values or send commands to a DIM Service
Provider.

### DicValue : monitoring a value

To monitor a value:
```js
const { DicValue, DnsClient } = require('dim');

/* creating a DnsClient */
var dnsClient = new DnsClient('dnsserver.cern.ch', 2505);

/* creating a DicValue object */
var dicValue = new DicValue('/test', null, dnsClient);

/* drop our ref on DnsClient, leave it to DicValue, see DnsClient doc */
dnsClient.unref();

/* promise API to retrieve first value */
dicValue.then((value) => {
    console.log('initial value: ', value)
    console.log('value is held and will be updated: ', dicValue.value);
});
dicValue.on('value', (value) => console.log('value update', value));

/* release the object once done */
dicValue.release();
```

**Note**: By using a DnsClient object:
-   The connection can be shared with other.
-   The value will automatically be bound to dic-servers, handling disconnect/reconnect
-   The ServiceDefinition will be fetched from the DnsServer

### DicValue : fetch a single value
To fetch a value:
```js
const { DicValue, DnsClient, ServiceInfo, NodeInfo } = require('dim');

// Request a service using a DNS
DicValue.get('/test', 'tcp://dnsserver.cern.ch:2505')
.then((value) => console.log(value));

// A DnsClient object can also be used:
var dns = new DnsClient('dnsserver.cern.ch', 2505);
DicValue.get('/test', dns)
.then((value) => console.log(value));

/* drop our ref on DnsClient, leave it to DicValue, see DnsClient doc */
dnsClient.unref();

// Direct requests are also supported:
DicValue.get(new ServiceInfo('/test', 'I'), NodeInfo.wrap('tcp://node:1234'))
.then((value) => console.log(value));
```

**Note**: If only the service name is provided in direct request, then a Buffer value will be returned.

**Note**: undefined is returned if service doesn't exist or DisNode is offline.

### DicCmd : send a command

To send a command:
```js
const { DicCmd, DnsClient, ServiceInfo, NodeInfo } = require('dim');

// Request using a DNS
DicCmd.invoke('/test', 12, 'tcp://dnsserver.cern.ch:2505')
.then(() => console.log('success'));

// Request using a dns object
var dns = new DnsClient('dnsserver.cern.ch', 2505);
DicCmd.invoke('/test', 12, dns);

/* drop our ref on DnsClient, leave it to DicCmd, see DnsClient doc */
dnsClient.unref();

// Direct request, complete ServiceInfo is required
DicCmd.invoke(new ServiceInfo('/test', 'I|CMD'), 12, NodeInfo.wrap('tcp://node:1234'));
```

### DicRpc : make a RPC

To make an RPC:
```js
const { DicRpc, DnsClient, ServiceInfo, NodeInfo } = require('dim');

// Request using a DNS
DicRpc.invoke('DIS_DNS/SERVICE_INFO', '*', 'tcp://dnsserver.cern.ch:2505')
.then((value) => console.log(value));

// Other examples are similar to DicCmd
```

### DicDns : request service information

To request information about a service, or a set of services:
```js
const { DicDns } = require('dim');

// Request information about all services, a single wildcard can be used as a prefix or postfix
DicDns.serviceInfo('*', 'tcp://dnsserver.cern.ch:2505')
.then((services) => {
    // services is a map of serviceName -> ServiceDefinition, ex:
    //  {
    //    'TOF_VPP5.State': ServiceDefinition { type: 2, params: [ [Object] ] },
    //    'HV_15/Setting': ServiceDefinition { type: 1, params: [ [Object] ] },
    //    ...
    //  }
    console.log(services);
});
```


## DIS : DIM Service Provider

This section describes how to implement a DIM Service Provider.

To create services and commands:
```js
const { DisNode, DnsClient } = require('dim');

var disNode = new DisNode();

/* register the node on a dns */
var dnsClient = new DnsClient('dnsserver.cern.ch', 2505);
dnsClient.register(disNode);

/* drop our ref on DnsClient, leave it to DisNode, see DnsClient doc */
dnsClient.unref();

// Create a basic service:
var testService = disNode.addService('/test', 'I', 42);
// Value update
testService.setValue(44);
disNode.removeService('/test');

// Create a basic command:
disNode.addCmd('/cmd', 'I', (req) => console.log('argument is ', req.value));
disNode.removeCmd('/cmd');

// close the connection and release resources:
disNode.close();
```


## DNS : DIM Name Service

This section describes Name Service client and server parts.

### DnsServer : running a server

To create a DnsServer:
```js
const { DnsServer } = require('dim');

// To start a server
var server = new DnsServer();
server.listen().then(() => console.log('ready to serve'));

/* close the server once done */
server.close();
```

### DnsClient : querying information

To query information about a service:
```js
const { DnsClient } = require('dim');

var client = new DnsClient('localhost', 2505);

// To query a service once
client.query('/TEST')
.then((serviceInfo) => console.log(serviceInfo));

// To listen to further services updates
client.on('service:/TEST', (serviceInfo) => console.log(serviceInfo));

// A 'wrap' method also exist to construct DnsClient from various arguments
client = DnsClient.wrap(client); /* refcounts existing client */
client.unref();
/* really drop our client for next example */
client.unref();

/* create a new client from url */
client = DnsClient.wrap(server.url());

// release the client:
client.unref();
```

### DnsClient : registering a service provider

To register services:
```js
const { DnsClient, DisNode } = require('dim');

var client = new DnsClient('localhost', 2505);
var node = new DisNode();
node.addService('/TEST', 'I', 42);
node.addCmd('/CMD', 'I', (req) => console.log('called'));

// Will ask the node to start listening
client.register(node)
.then(() => console.log('all set'));
client.unref(); /* we can release our ref, it's now refferenced by DisNode */

// close the connection once done and release resources:
node.close();
```

### DnsClient refcount

DnsClient objects are shared by clients and service providers, thus those
objects are refcounted.

New DnsClient objects are created with a reference already set, a call to
_unref_ must be made to release resources once the client is not needed
anymore.

The following methods does increment the refcount:
-   _DnsClient.ref_
-   _DnsClient.wrap_
-   _DnsClient.register_

The following methods decrement this refcount:
-   _DnsClient.unref_
-   _DnsClient.unregister_

Objects such as _DisNode_, _DicService_ and _DicValue_ will increment/decrement
the refcount as needed.
