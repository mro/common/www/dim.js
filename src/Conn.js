// @ts-check
const
  { EventEmitter } = require('events'),
  q = require('q'),
  net = require('net'),
  debug = require('debug')('dim:conn'),
  _ = require('lodash'),
  { isAnyAddress } = require('./utils'),
  { DEFAULT_HOST, DEFAULT_LOCALHOST } = require('./Consts'),

  { DnaStream, DnaHeader, DnaNet } = require('./packets/Dna'),
  Errors = require('./Errors');

/**
 * @param  {TCPConn|TCPServer} self
 * @param  {string} host
 * @param  {number} [port]
 */
function addrArg(self, host, port) {
  if (_.isNil(port)) {
    /* $FlowIgnore: lastIndexOf works well with strings */
    var split = _.lastIndexOf(host, ':');
    if (split >= 0) {
      self.host = host.slice(0, split);
      self.port = _.toNumber(host.slice(split + 1)) || 0;
    }
    else {
      self.host = host;
      self.port = 0;
    }
  }
  else {
    self.host = host;
    self.port = port;
  }
}

/**
 * TCP Connection class
 * @details emits:
 * - packet : when packets are received
 * - connect : the connection has been established
 * - close : the connection has been closed
 */
class TCPConn extends EventEmitter {
  /**
   * create a dim tcp connection
   * @param {string} host host to connect to
   * @param {number} [port] port to connect to
   *
   * @details "ipv4:port" uri string can also be used as host argument
   */
  constructor(host, port) {
    super();
    this.connected = false;
    this._reader = new DnaStream((pkt) => this.emit('packet', pkt));

    this.host = DEFAULT_LOCALHOST;
    this.port = 0;
    addrArg(this, host, port);
    this.host = isAnyAddress(this.host) ? '127.0.0.1' : this.host;
  }

  close() {
    var sock = this.sock;
    if (sock) {
      debug('closing');
      this.sock = null;
      sock.end();
      sock.destroy();
    }
    this.connected = false;
  }

  /**
   * @param {net.Socket} sock
   */
  _setSock(sock) {
    this.connected = true;
    sock.setTimeout(0);
    sock.removeAllListeners('timeout');
    sock.setNoDelay();
    sock.setKeepAlive(true, TCPConn.KEEPALIVE_INTERVAL);

    sock.on('data', this._reader.add.bind(this._reader));
    sock.on('error', this.close.bind(this));
    sock.once('close', (hadError) => {
      debug(`socket closed (hadError: ${hadError})`);
      sock.removeAllListeners();
      this.close();
      this.emit('close', hadError);
    });
    this.sock = sock;
  }

  connect() {
    var deferred = q.defer();
    this.close(); /* close socket if it was already open */

    var sock = new net.Socket();
    this.sock = sock;

    /** @type {(ErrorClass: any, message: string, err?: any) => any} sockConnError */
    var sockConnError = (ErrorClass, message, err) => {
      debug('connection error:', _.get(err, 'message', err));
      this.close();
      sock.removeAllListeners();
      this.emit('close', true);
      deferred.reject(new ErrorClass(message));
    };
    var errCB = _.partial(sockConnError, Errors.NotFound, 'failed to connect');
    var closeCB = _.partial(sockConnError, Errors.Interrupted,
      'connection closed');
    sock.once('error', errCB);
    sock.once('close', closeCB);
    sock.setTimeout(TCPConn.CONN_TIMEOUT, errCB);

    sock.connect(this.port, this.host, () => {
      sock.removeListener('error', errCB);
      sock.removeListener('close', closeCB);
      this._setSock(sock);
      debug('connected');
      this.send(new DnaNet());
      this.emit('connect');
      deferred.resolve();
    });

    return deferred.promise;
  }

  /**
   * @param  {DnaHeader|Buffer} [buffer]
   * @return {boolean}
   */
  send(buffer) {
    if (buffer instanceof DnaHeader) {
      buffer = buffer.raw;
    }
    else if (!buffer) {
      debug('no packet to send');
      return false;
    }

    if (this.sock) {
      this.sock.write(buffer);
      debug('packet sent len:%i', buffer.length);
      return true;
    }
    debug('packet not sent (no connection)');
    return false;
  }
}
TCPConn.CONN_TIMEOUT = 5000;
TCPConn.KEEPALIVE_INTERVAL = 15000;

/**
 * TCP server
 *
 * @details emits the following signals:
 * - listening : server is in listening state
 * - connect : an incoming connection is open
 * - close : server is not listening anymore
 */
class TCPServer extends EventEmitter {
  /**
   * @param {string} host
   * @param {number} [port]
   */
  constructor(host, port) {
    super();
    this.listening = false;

    this.host = DEFAULT_LOCALHOST;
    this.port = 0;
    addrArg(this, _.defaultTo(host, DEFAULT_HOST), port);
  }

  close() {
    if (this.server) {
      this.server.close();
      debug('closing');
      this.server = null;
    }
    this.listening = false;
  }

  /**
   * @param  {net.Socket} sock
   */
  _connection(sock) {
    debug('incoming connection: %s:%d', sock.remoteAddress, sock.remotePort);
    /* @ts-ignore: connection is incoming remoteAddress must be set */
    var conn = new TCPConn(sock.remoteAddress, sock.remotePort);
    conn._setSock(sock);
    this.emit('connect', conn);
  }

  /**
   *  Start listening for incoming connections
   * @param  {((reason: any, value: any) => any)=} callback incoming connection callback
   * @return {Q.Promise<any>}
   */
  listen(callback) {
    var deferred = q.defer();
    /** @type {net.Server} */
    var server = net.createServer(this._connection.bind(this));

    var errCB = () => {
      debug('connection error');
      this.close();
      server.removeAllListeners();
      this.emit('close', true);
      deferred.reject(new Error('failed to listen'));
    };
    server.once('error', errCB);

    this.server = server;
    // $FlowFixMe: ?
    this.server.listen(this.port, this.host, () => {
      this.listening = true;
      server.removeListener('error', errCB);
      server.once('error', this.close.bind(this));
      server.once('close', (/** @type {boolean} hadError */ hadError) => {
        debug(`server socket closed (hadError: ${hadError})`);
        server.removeAllListeners();
        this.close();
        this.emit('close', hadError);
      });
      const addr = server.address();
      this.port = _.get(addr, 'port', this.port);
      this.host = _.get(addr, 'address', this.host);
      debug('listening on: %s:%d', this.host, this.port);
      this.emit('listening');
      deferred.resolve();
    });
    // @ts-ignore
    return deferred.promise.nodeify(callback);
  }
}

module.exports = { TCPConn, TCPServer };
