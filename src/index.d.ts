// / <reference types="node" />
/* eslint-disable max-len,max-lines */
import { EventEmitter } from 'events';

export = dim;
export as namespace dim;

declare namespace dim {
  type BufferIn = any;
  type BufferOut = any;

  export class NodeInfo {
    static local(host: string | number, port?: (number | null) | undefined, task?: (string | null) | undefined): NodeInfo;

    static fromBuffer(buffer: BufferIn | Buffer): NodeInfo | null;

    static parseUrl(u: string): { name: any; port: any; } | null;

    static wrap(object: NodeInfo | string | any): NodeInfo;

    constructor(info?: Partial<{
        host: number;
        port: number;
        name: string;
        task: string;
        pid: number;
        protocol: number;
        format: number;
    }> | undefined);

    host: number;

    port: number;

    name: string;

    task: string;

    pid: number;

    protocol: number;

    format: number;

    url(): string;

    setAddress(host: string | number, port?: number | undefined): void;

    isValid(): boolean;

    toBuffer(buffer: BufferOut | Buffer): boolean;
  }
  namespace NodeInfo {
      const NAME_MAX: number;
      const TASK_MAX: number;
  }
  export class ServiceDefinition {
    static parse(description?: string | any): ServiceDefinition | null;

    static _parseParams(params: string): Array<ServiceDefinition.Param>;

    static toStringParams(params: Array<ServiceDefinition.Param> | null): string;

    constructor(other?: Partial<ServiceDefinition> | undefined);

    type: number | null;

    params: Array<ServiceDefinition.Param> | null;

    returns: Array<ServiceDefinition.Param> | null;

    isValid(): boolean;

    toString(trailSep: boolean): string;

    unpackParams(buffer: BufferIn | Buffer, format?: number | undefined): any;

    unpackReturns(buffer: BufferIn | Buffer, format?: number | undefined): any;

    packParams(values: any, buffer: BufferOut | Buffer, format?: number | undefined): any;

    packReturns(values: any, buffer: BufferOut | Buffer, format?: number | undefined): any;

    paramsSize(values: any): number;
  }
  namespace ServiceDefinition {
    export const PARAM_ANY: string;
    export const Fmt: {
      I: ParamFmt,
      C: ParamFmt,
      L: ParamFmt,
      S: ParamFmt,
      F: ParamFmt,
      D: ParamFmt,
      X: ParamFmt
    };
    interface Param {
        fmt: string;
        count: ParamCount;
    }
    namespace NamedFmt {
      const Int16: string;
      const Int32: string;
      const Int64: string;
      const Char: string;
      const String: string;
      const Float: string;
      const Double: string;
    }
    export enum Type {
      RPC,
      CMD,
      SRV
    }
    export type ParamCount = string | number;
    export interface ParamFmt {
        name: string;
        size: number;
    }
  }

  export class ServiceInfo {
    static fromBuffer(buffer: BufferIn | Buffer): ServiceInfo | null;

    constructor(name?: string | null | undefined, definition?: ServiceDefinition | string | null | undefined, sid?: number | null | undefined, node?: NodeInfo | null | undefined);

    name: string | null | undefined;

    definition: string | ServiceDefinition | null | undefined;

    sid: number | null;

    node: NodeInfo | null | undefined;

    removed: boolean;

    isCmd: boolean;

    getSid(packed: boolean): number | null;

    toBuffer(buffer: BufferOut | Buffer): any;
  }
  export namespace packets {
    export interface DnaNetInfo {
      name: string;
      task: string;
    }
    export class DnaHeader {
      static parseBuffer(buffer: Buffer): {
        dataSize: number;
        magic: number;
      } | null;

      static fromBuffer(buffer: Buffer): DnaHeader | null;

      constructor(dataSize?: (number | null) | undefined, magic?: (number | null) | undefined);

      raw: Buffer;

      isValid(): boolean;

      data(): Buffer;
    }
    export namespace DnaHeader {
      const LEN: number;
      const MAGIC: number;
      const TST_MAGIC: number;
    }
    export class DnaStream {
      constructor(cb: (data: Buffer, magic: number) => any, timeout?: (number | null) | undefined);

      cb: (data: Buffer, magic: number) => any;

      timeout: number;

      setTimeout(delay: number): void;

      clearTimeout(): void;

      add(data: Buffer | null): void;
    }
    export namespace DnaStream {
      const TIMEOUT: number;
    }
    export class DnaNet extends DnaHeader {
      static parse(buffer: Buffer | BufferIn): DnaNetInfo | null;

      constructor(node?: (string | null) | undefined, task?: (string | null) | undefined);
    }
    export namespace DnaNet {
      export const LEN: number;
      export const MAGIC: number;
    }
    export interface DicRepInfo<T=any> {
      value?: T;
      data: Buffer;
      timestamp?: number | undefined;
      quality?: number | undefined;
      sid: number;
      removed?: true | undefined;
    }

    export interface DnsDupInfo {
      name: string,
      services: Array<number>,
      task: string,
      pid: number,
      port: number
    }

    export namespace DicReq {
      enum Type {
        ONCE_ONLY, TIMED, MONITORED, COMMAND, DELETE,
        MONIT_ONLY, UPDATE, TIMED_ONLY, MONIT_FIRST
      }
    }
  }
  export namespace Errors {
    export class Timeout extends DimError {
      constructor(message: string);
    }
    export namespace Timeout {
      const CODE: string;
    }
    export class NotFound extends DimError {
      constructor(message: string);
    }
    export namespace NotFound {
      const CODE: string;
    }
    export class Busy extends DimError {
      constructor(message: string);
    }
    export namespace Busy {
      const CODE: string;
    }
    export class Interrupted extends DimError {
      constructor(message: string);
    }
    export namespace Interrupted {
      const CODE: string;
    }
    class DimError extends Error {
      constructor(code: string, message: string);

      code: string;
    }
    namespace DimError {
      const CODE: string;
    }
  }
  type TCPConn = any;
  type TCPServer = any;
  type DisConn = any;

  export class DisService<T=any> extends EventEmitter {
    constructor(definition?: ServiceDefinition | string, value?: T | undefined);

    definition: ServiceDefinition | null;

    value: T | undefined;

    quality: number;

    timestamp: number;

    setValue(value: T): void;

    getValue(): any;
  }

  export class DisCmd<T=any> extends DisService<T> {
    constructor(definition: ServiceDefinition | string, callback: (req: DicReqInfo<T>, conn: DisConn) => void);

    handle(req: DicReqInfo<T>, conn: TCPConn): void;
  }

  export interface DicReqInfo<T=any> {
      service: string;
      sid: number;
      type: number;
      stamped: boolean;
      timeout: number;
      format: number;
      data: Buffer | null;
      value?: T;
  }

  export class DisRpc<ARGS=any, RET=any> {
    constructor(
      definition: ServiceDefinition | string,
      callback: (req: DicReqInfo<ARGS>, conn: DisConn) => RET);

    value: (req: DicReqInfo<ARGS>, conn: DisConn) => RET;

      definition: ServiceDefinition | null;

        rpcIn: DisCmd | undefined;

        rpcOut: DisService | undefined;

        handle(req: DicReqInfo<ARGS>, conn: DisConn): Promise<void>;
  }

  type DisServiceInfo = { service: DisService, info: ServiceInfo };

  export class DisNode extends EventEmitter {
    constructor(addr?: (string | null) | undefined, port?: (number | null) | undefined, nodeInfo?: (NodeInfo | null) | undefined, ...args: any[]);

    info: NodeInfo;

    sid: number;

    cid: number;

    serviceList: any;

    clientList: any;

    register(dns: DnsClient | string): Promise<any>;

    serviceInfo(): ServiceInfo[];

    isService(name: string): boolean;

    addService<T>(name: string, definition: string | ServiceDefinition | DisService<T>, value?: any | undefined): DisService<T> | null;

    removeService(name: string): boolean;

    addCmd(name: string, definition: DisCmd | ServiceDefinition | string, cb?: ((req: DicReqInfo, conn: DisConn) => any) | null | undefined): DisCmd | null;

    removeCmd(name: string): void;

    addRpc(name: string, definition: DisRpc | ServiceDefinition | string, cb?: ((req: DicReqInfo, conn: DisConn) => any) | null | undefined): DisRpc | null;

    removeRpc(name: string): void;

    getServiceBySid(sid: number): DisServiceInfo | null;

    listen(): Promise<DisNode>;

    isListening(): any;

    close(): void;

    nextSID(): number;

    setExitHandler(req: DicReqInfo, conn: DisConn): void;
  }

  export class DnsClient extends EventEmitter {
    static wrap(dns?: ((DnsClient | string) | null) | undefined): DnsClient | null;

    constructor(host: string, port?: (number | null) | undefined, opts?: ({
        retryDelay?: number;
    } | null) | undefined);

    opts: {
        retryDelay?: number | undefined;
    } | null | undefined;

    sid: number;

    ref(): void;

    unref(): void;

    connect(): Promise<void>;

    query(serviceName: string): Promise<ServiceInfo>;

    refreshQuery(serviceName: string): void;

    getServiceInfo(service: string): ServiceInfo | null;

    register(disNode: DisNode): Promise<any>;

    unregister(disNode: DisNode | null): void;

    close(): void;
  }
  namespace DnsClient {
    export const CONN_RETRY: number;
  }

  export class DisClient {
    constructor(info: NodeInfo | string | any, opts?: ({
        retryDelay: number;
    } | null) | undefined);

    info: NodeInfo;

    sid: number;

    opts: {
        retryDelay: number;
    } | null | undefined;

    nextSID(): number;

    url(): any;

    connect(): Promise<any> | null;

    close(): void;

    request<T=any>(
      service: string | ServiceInfo,
      options?: {
        timeout?: number | undefined;
        stamped?: boolean | undefined;
        raw?: boolean | undefined;
      }|null|undefined,
      callback?: ((reason: Error|null|undefined, value: packets.DicRepInfo<T>|undefined) => any)|null|undefined
    ): Promise<packets.DicRepInfo<T>|undefined>;

    cmd(
      service: ServiceInfo|string,
      args: any, options?: {
        timeout?: number | undefined;
        retryDelay?: number | undefined;
      }|null|undefined,
      callback?: ((reason: Error|null|undefined, value: undefined) => any)|null|undefined
    ): Promise<void>;

    monitor<T=any>(
      service: ServiceInfo|string,
      options: {
        stamped?: boolean|undefined;
        type?: packets.DicReq.Type|undefined;
        timeout?: number|undefined;
      },
      callback?: ((rep: packets.DicRepInfo<T>|undefined, req: any) => any)|null|undefined
    ): DisClient.SubId;

    interval<T=any>(
      service: ServiceInfo|string,
      interval?: number|null|undefined,
      options?: {
        stamped?: boolean | undefined;
        timeout?: number | undefined;
      }|null|undefined,
      callback?: ((rep: packets.DicRepInfo<T>|undefined, req: any) => any)|null|undefined
    ): DisClient.SubId;

    abort(sid: DisClient.SubId): void;
  }
  namespace DisClient {
    export type SubId = number;
    export const INTERVAL: number;
    export const TIMEOUT: number;
    export const CONN_RETRY_DELAY: number;
  }

  export class DicService extends EventEmitter {
    static lookup(service: string | ServiceInfo, dns?: (DnsClient | string | null) | undefined): Promise<ServiceInfo>;

    constructor(service: string | ServiceInfo, dns?: ((DnsClient| NodeInfo | string) | null) | undefined);

    name: string;

    node: DisClientRef | null;

    release(): void;

    setDns(dns?: ((DnsClient| string) | null) | undefined): void;

    dns: DnsClient| null | undefined;

    setServiceInfo(info: ServiceInfo): void;

    info: ServiceInfo | undefined;

    setNode(nodeInfo?: (NodeInfo | null) | undefined): DisClientRef | null;
  }
  namespace DicService {
    var registry: any;
  }
  export class DisClientRef extends DisClient {}

  export class DnsServer extends EventEmitter {
    constructor(host?: (string | null) | undefined, port?: (number | null) | undefined);

    cid: number;

    dis: DisNode;

    url(): string;

    serviceInfo(service: string): ServiceInfo | null;

    listen(): Promise<DnsServer>;

    close(): void;
  }
  export class DicValue<T> extends DicService {
    static get<T>(service: string | ServiceInfo, dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined, raw?: boolean | undefined): Promise<T | void>;

    constructor(service: ServiceInfo | string, options?: ({
      timeout?: number;
      stamped?: boolean;
    } | null) | undefined, dns?: ((DnsClient| string | NodeInfo) | null) | undefined);

    value: T | null | undefined;

    options: {
      timeout?: number | undefined;
      stamped?: boolean | undefined;
    } | null | undefined;

    timestamp: number | null;

    sid: number | undefined;

    then(fulfill: (value?: any) => any, reject: (reason?: any) => any): Promise<any>;

    promise(): Promise<T | void>;
  }
  namespace DicValue {
    var GET_TIMEOUT: number;
  }

  export class DicRpc extends DicService {
    static invoke(service: string | ServiceInfo, args: any, dns?: ((DnsClient| NodeInfo | string) | null) | undefined, timeout?: number | undefined): Promise<packets.DicRepInfo>;

    constructor(service: string | ServiceInfo, dns?: string | NodeInfo | DnsClient| null | undefined);

    rpcInSid: number | undefined;

    invoke(args: any, timeout?: number | undefined): Promise<packets.DicRepInfo>;
  }
  namespace DicRpc {
    export const TIMEOUT: number;
  }

  export const DicCmd: {
    TIMEOUT: number;
    invoke: (service: string | ServiceInfo, args: any, dns?: string | NodeInfo | DnsClient| null | undefined, timeout?: number | null | undefined) => Promise<void>;
  };

  export namespace DicDis {
    export function serviceList (taskName: string, dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<void | {
        [serviceName: string]: ServiceDefinition;
    }>;

    export class DicServicesList<T={ [serviceName: string]: ServiceDefinition }> extends DicValue<T> {
      constructor(taskName: string, dns?: ((DnsClient| string | NodeInfo) | null) | undefined, options?: ({
        timeout?: number;
        stamped?: boolean;
      } | null) | undefined);
    }
  }

  export namespace DicDns {
    export function serviceInfo(name: string, dns: string | DnsClient): Promise<{ [serviceName: string]: ServiceDefinition }>;

    export function serverList(dns: (DnsClient| NodeInfo | string) | null, timeout?: (number | null) | undefined): Promise<void |
    {
      task: string;
      node: string;
      pid: number;
      removed?: boolean;
    }[]>;

    export class DicServerList<T=Array<{task: string, node: string, pid: number }>> extends DicValue<T> {
      constructor(dns?: ((DnsClient| string | NodeInfo) | null) | undefined, options?: ({
        timeout?: number;
        stamped?: boolean;
      } | null) | undefined);
      db: { [taskName: string]: {task: string, node: string, pid: number } }
    }
  }

  export namespace Consts {
    export const Service: {
      NAME_MAX: number,
      DEF_MAX: number
    };

    export const Node: {
      NAME_MAX: number,
      TASK_MAX: number
    };

    export const Dns: {
      PORT_DEFAULT: number
    };

    export const VersionNumber: number;

    export const DEFAULT_HOST: number;
  }
}
