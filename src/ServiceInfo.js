// @ts-check

const
  _ = require('lodash'),
  { BufferIn, BufferOut } = require('./utils/IOBuffer'),
  ServiceDefinition = require('./ServiceDefinition'),
  { Service } = require('./Consts');

/**
 * @typedef {import('./NodeInfo')} NodeInfo
*/

const SID_IS_CMD = 0x10000000;
const SID_REMOVED = 0x80000000;
const SID_MASK = 0xFFFFFF;

class ServiceInfo {
  /**
   * ServiceInfo constructor
   * @param {?string=} name service name
   * @param {?(ServiceDefinition|string)=} definition service definition
   * @param {?number=} sid service tracking id (DNS)
   * @param {?NodeInfo=} node service NodeInfo
   */
  /* eslint-disable-next-line max-len */
  constructor(name, definition, sid, node) {
    this.name = name;
    // $FlowFixMe: checked in parse
    this.definition = ServiceDefinition.parse(definition) || definition;
    this.sid = _.defaultTo(sid, null);
    this.node = node;
    this.removed = false;
    this.isCmd = (_.get(this.definition, 'type') ===
      ServiceDefinition.Type.CMD);
  }

  /**
   * @param  {boolean} packed
   * @return {?number}
   */
  getSid(packed) {
    if (!packed) {
      return this.sid;
    }
    else {
      var sid = this.sid || 0;
      /* js operations are on int32 type, apply a null cast operation */
      if (this.removed) { sid = (sid | SID_REMOVED) >>> 0; }
      if (this.isCmd) { sid = (sid | SID_IS_CMD) >>> 0; }
      return sid;
    }
  }

  /**
   * @param  {BufferOut|Buffer} buffer
   * @return {any}
   */
  toBuffer(buffer) {
    const out = BufferOut.wrap(buffer);

    out.writeString(this.name || "", Service.NAME_MAX);
    out.writeUInt32(this.getSid(true) || 0);
    out.writeString(_.toString(this.definition), Service.DEF_MAX);
    return !out.fail;
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @return {? ServiceInfo}
   */
  static fromBuffer(buffer) {
    buffer = BufferIn.wrap(buffer);
    var name = buffer.readString(Service.NAME_MAX);
    var sid = buffer.readUInt32();
    var def = buffer.readString(Service.DEF_MAX);
    if (buffer.fail) {
      return null;
    }

    var info = new ServiceInfo(name, def, sid & SID_MASK);
    if (sid & SID_IS_CMD) { info.isCmd = true; }
    if (sid & SID_REMOVED) { info.removed = true; }

    return info;
  }
}

module.exports = ServiceInfo;
