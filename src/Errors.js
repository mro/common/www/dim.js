// @ts-check

class DimError extends Error {
  /**
  * @param {string} code
  * @param {string} message
  */
  constructor(code, message) {
    super(message);
    this.code = code;
  }
}
DimError.CODE = 'EUNKNOWN';

class Timeout extends DimError {
  /**
  * @param {string} message
  */
  constructor(message) { super(Timeout.CODE, message); }
}
Timeout.CODE = 'ETIMEDOUT';

class NotFound extends DimError {
  /**
  * @param {string} message
  */
  constructor(message) { super(NotFound.CODE, message); }
}
NotFound.CODE = 'ENOENT';

class Busy extends DimError {
  /**
  * @param {string} message
  */
  constructor(message) { super(Busy.CODE, message); }
}
Busy.CODE = 'EBUSY';

class Interrupted extends DimError {
  /**
  * @param {string} message
  */
  constructor(message) { super(Interrupted.CODE, message); }
}
Interrupted.CODE = 'EINTR';

module.exports = { Timeout, NotFound, Busy, Interrupted };
