
import { expectType } from 'tsd';
import {
  Consts, DicCmd, DicDns, DicReqInfo,
  DicRpc, DicValue, DisClient, DisCmd, DisNode, DisRpc,
  DisService, DnsClient,
  DnsServer, NodeInfo, ServiceDefinition, ServiceInfo, packets } from '.';

/* DIC */
(async function() {
  var dnsClient: DnsClient = new DnsClient('dnsserver.cern.ch', 2505);

  /* DicValue */
  var v1 = new DicValue('/test', null, dnsClient);
  v1 = new DicValue("test", { timeout: 1000, stamped: true }, dnsClient);
  v1.release();

  var v = new DicValue<string>(new ServiceInfo('test', 'C', null,
    NodeInfo.local('localhost', 1234)));

  expectType<string | void>(await v.promise());
  v.on('value', (value: string|void) => expectType<string|void>(value));

  expectType<number | void>(
    await DicValue.get<number>("test", "proxy-1", 1000));

  /* DicRpc */
  expectType<packets.DicRepInfo>(
    await DicRpc.invoke("test", [ 12, "test" ], "proxy-2", DicRpc.TIMEOUT));

  /* DicCmd */
  expectType<void>(
    await DicCmd.invoke(new ServiceInfo('/test', 'I|CMD'), 12,
      NodeInfo.wrap('tcp://node:1234')));

  expectType<{ [serviceName: string]: ServiceDefinition }>(
    await DicDns.serviceInfo('*', dnsClient));

  dnsClient.unref();
}());

/* DIS */
(async function() {
  var node = new DisNode();

  // Test empty DisService constructor
  var srvEmpty: DisService<any> = new DisService();

  var cmd: DisCmd<number> = new DisCmd(ServiceDefinition.NamedFmt.Int16 + ':1',
    (req: DicReqInfo<number>) => expectType<number | undefined>(req.value));

  node.addCmd('test', cmd);
  node.removeCmd('test');

  node.addCmd('test', ServiceDefinition.NamedFmt.Int16 + ':1',
    (req: DicReqInfo<number>) => expectType<number | undefined>(req.value));

  var rpc: DisRpc<number, number> = new DisRpc('I:1;I:1|RPC',
    (req: DicReqInfo<number>): number => {
      expectType<number | undefined>(req.value);
      return 42;
    });

  node.addRpc('test', rpc);
  node.removeRpc('test');

  node.addRpc('test', 'I:1;I:1|RPC', (req: DicReqInfo<number>): number => {
    expectType<number | undefined>(req.value);
    return 42;
  });

  var serv: DisService<number> | null = node.addService('/TEST', 'I', 42);
  (serv as DisService<number>).setValue(42);

  var cli = new DisClient('tcp://localhost:1234');
  var rep = await cli.request<number>(
    new ServiceInfo('/test', 'C', null, cli.info));
  expectType<number|undefined>(rep && rep.value);

  expectType<void>(
    await cli.cmd('/test', 42, null,
      (err: Error|null|undefined) => console.log(err)));

  var sid = cli.monitor<string>('/test',
    { stamped: true, type: packets.DicReq.Type.TIMED_ONLY, timeout: 2 },
    (rep: packets.DicRepInfo<string>|undefined, req: any) => console.log(rep));
  expectType<DisClient.SubId>(sid);
  cli.abort(sid);

  sid = cli.interval('/test', 2, null,
    (rep: packets.DicRepInfo<string>|undefined) => console.log(rep));
  cli.abort(sid);
}());

/* DNS */
(async function() {
  var dnsClient = new DnsClient('proxy-2', Consts.Dns.PORT_DEFAULT);

  expectType<ServiceInfo>(await dnsClient.query('/test'));
  dnsClient.unref();

  var server = new DnsServer();
  await server.listen();

  server.close();
}());
