// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  debug = require('debug')('dim:client'),

  { DicReq, DicRep, DicCmd } = require('../packets/DicDis'),
  ServiceDefinition = require('../ServiceDefinition'),
  NodeInfo = require('../NodeInfo'),
  { Timeout } = require('../Errors'),
  { TCPConn } = require('../Conn');

/**
 * @typedef {import('../packets/DicDis')} DicDis
 * @typedef {import('../ServiceInfo')} ServiceInfo
 * @typedef {number} SubId
 * @typedef {{
 *  service: string|ServiceInfo,
 *  type: dimPackets.DicReq.Type,
 *  opts: { stamped?: boolean, timeout?: number, args?: Buffer, raw?: boolean },
 *  callback: ((rep: dimPackets.DicRepInfo|undefined, req: ServiceReg) => any)|null|undefined
 *  timeout?: Timeout }} ServiceReg
 */

class DisClient {
  /**
   * DisClient constructor
   * @param {NodeInfo|string|any} info related node info
   * @param {{retryDelay: number}?=} opts options
   *
   * @details valid options:
   *  - retryDelay connection retry delay, defaults to DisClient.RETRY_DELAY
   *    set to 0 to disable.
   */
  constructor(info, opts) {
    this.info = NodeInfo.wrap(info);
    this._conn = new TCPConn(this.info.name, this.info.port);
    this.sid = 1;
    this.opts = opts;

    _.bindAll(this, [ '_onPacket', '_onClose' ]);
    this._conn.on('packet', this._onPacket);
    this._conn.on('close', this._onClose);
    /** @type {{ [subId: number]: ServiceReg }} */
    this._sidMap = {};

    /** @type {?NodeJS.Timeout} */
    this._timer = null;

    /** @type {function|null} */
    this.onRemoteClose = null;
  }

  /**
   * @return {number}
   */
  nextSID() {
    this.sid = (this.sid + 1) & 0xFFFFFFFF; /* limit sid to 32 bits */
    return this.sid;
  }

  url() {
    return _.invoke(this.info, 'url');
  }

  /**
   * @return {?Q.Promise<any>}
   */
  connect() {
    if (!this._connProm) {
      this._clearTimer();
      this._connProm = this._conn.connect()
      .then(() => {
        debug('connected on %s', this.url());
        _.forEach(this._sidMap, (req, sid) => this._send(req, _.toNumber(sid)));
        return this;
      });
      // catch unhandled errors
      this._connProm.catch(
        (err) => debug('connection failed on: %s message: %s',
          this.url(), err.message));
    }
    return this._connProm;
  }

  close() {
    debug('closing:', this.url());
    this._connProm = null;
    this._clearTimer();
    this._conn.removeListener('packet', this._onPacket);
    this._conn.removeListener('close', this._onClose);
    this._conn.close();
    _.forEach(this._sidMap, (req) => _.invoke(req, 'callback', undefined, req));
    this.onRemoteClose = null;
  }

  _clearTimer() {
    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
  }

  _onClose() {
    var retry = _.get(this.opts, 'retryDelay', DisClient.CONN_RETRY_DELAY);

    this._connProm = null;
    if (retry) {
      this._clearTimer();
      this._timer = setTimeout(this.connect.bind(this), retry);
    }
    debug('remote close:', this.url());
    _.forEach(this._sidMap, (req) => _.invoke(req, 'callback', undefined, req));
    _.invoke(this, 'onRemoteClose');
  }

  /**
   * @param  {SubId} sid
   * @return {ServiceReg}
   */
  _take(sid) {
    var ret = this._sidMap[sid];
    delete this._sidMap[sid];
    return ret;
  }

  /**
   * @param  {Buffer} pkt
   */
  _onPacket(pkt) {
    var rep = DicRep.parse(pkt);
    if (!rep) {
      debug('failed to parse packet');
      return;
    }

    var req = this._sidMap[rep.sid];
    if (!req) {
      debug('no such request sid:%d', rep.sid);
      return;
    }
    else if (req.opts.stamped !== _.has(rep, 'timestamp')) {
      debug('parsing reply again (timestamp error)');
      rep = DicRep.parse(pkt, req.opts.stamped);
      if (!rep) {
        debug('something went wrong parsing stamped reply');
        return;
      }
    }
    var value;
    if (_.get(req.service, 'definition.type') === ServiceDefinition.Type.RPC) {
      value = _.invoke(req.service, 'definition.unpackReturns', rep.data);
    }
    else if (!_.get(req.opts, 'raw', false)) {
      value = _.invoke(req.service, 'definition.unpackParams', rep.data);
    }
    if (value !== undefined) {
      rep.value = value;
    }
    /* do not trust the user, callback may not be a real callback or may throw */
    _.invoke(req, 'callback', rep, req);
  }

  /**
   * @param {any} req
   * @param {SubId} sid
   * @return {boolean}
   */
  _send(req, sid) {
    if (!this._conn.connected) {
      this.connect();
      return false;
    }

    if (req.type === DicReq.Type.COMMAND) {
      this._sendCmd(req.service, req.opts.args, req.opts, sid);
      req.callback(null, req);
    }
    else {
      let name = _.get(req.service, 'name', req.service);
      if (_.get(req.service, 'definition.type') ===
          ServiceDefinition.Type.RPC) {
        name += '/RpcOut';
      }
      debug('request: service:%s sid:%d type:%s opts:%o', name, sid,
        _.get(DicReq.TypeStr, req.type, 'unknown'), req.opts);
      this._conn.send(new DicReq(name, req.type, req.opts, sid));
    }
    return true;
  }

  /**
   * @param {ServiceInfo|string} service
   * @param {any} data
   * @param {any} opts
   * @param {number} sid
   */
  _sendCmd(service, data, opts, sid) {
    var name = _.get(service, 'name', service);
    var size = _.invoke(service, 'definition.paramsSize', data);

    if (_.get(service, 'definition.type') === ServiceDefinition.Type.RPC) {
      name += '/RpcIn';
    }
    debug('command: service:%s sid:%d', name, sid);
    if (_.isNil(size)) {
      /* either definition is not a ServiceDefinition object, or something is
         weird with data */
      this._conn.send(new DicCmd(name, data, opts, sid));
    }
    else {
      var pkt = new DicCmd(name, size, opts, sid);
      _.invoke(service, 'definition.packParams', data, pkt.writer);
      this._conn.send(pkt);
    }
  }

  /**
   * @template T=any
   * @param  {ServiceInfo|string} service
   * @param  {dimPackets.DicReq.Type} type
   * @param  {any}                opts
   * @return {Q.Promise<dimPackets.DicRepInfo<T>|undefined>}
   */
  _request(service, type, opts) {
    var def = q.defer();
    var sid = this.nextSID();
    /** @type {ServiceReg} */
    var req = this._sidMap[sid] = {
      service: service, type: type, opts: opts,
      callback:
        (/** @type {dimPackets.DicRepInfo|null|undefined} */ rep, /** @type {ServiceReg} */ req) => {
          delete this._sidMap[sid];
          if (_.get(req, 'timeout')) {
            // @ts-ignore checked above
            clearTimeout(req.timeout);
            delete req.timeout;
          }
          def.resolve(rep);
        }
    };

    if (req.opts.timeout) {
      // @ts-ignore lodash ts definition bug
      req.timeout = _.delay(() => {
        def.reject(new Timeout('request time-out for sid:' + sid));
        delete this._sidMap[sid];
      }, req.opts.timeout); /* timeout is in milliseconds */
    }

    this._send(req, sid);
    return def.promise;
  }

  /**
   * @template T=any
   * @brief Request service once
   * @param {string|ServiceInfo} service service name
   * @param {{timeout?: number, stamped?: boolean, raw?: boolean}?=} options request options
   * @param {((reason: ?Error, value: dimPackets.DicRepInfo<T>|undefined) => void)?=} callback
   * @return {Q.Promise<dimPackets.DicRepInfo<T>|undefined>}
   *
   * @details available options:
   * - {Integer} timeout: request timeout value (default: DisClient.TIMEOUT)
   * - {Boolean} stamped: make a stamped request (default: false)
   */
  request(service, options, callback) {
    return this._request(service, DicReq.Type.ONCE_ONLY, {
      stamped: _.get(options, 'stamped', false),
      timeout: _.get(options, 'timeout', DisClient.TIMEOUT),
      raw: _.get(options, 'raw', false)
      // @ts-ignore: null or undefined is valid
    }).nodeify(callback);
  }

  /**
   * @brief Send a command
   * @param {ServiceInfo|string} service  command service
   * @param {any} args     Command arguments
   * @param {{ timeout?: ?number, retryDelay?: ?number }?=} options  request options
   * @param {((reason: any, value: any) => any)?=} callback result callback
   * @return {Q.Promise<void>}
   *
   * @details available options:
   * - {Integer} timeout: command timeout value (default: DisClient.TIMEOUT)
   */
  cmd(service, args, options, callback) {
    if (this._conn.connected) {
      this._sendCmd(service, args, options, this.nextSID());
      // @ts-ignore nodeify argument can be null or undefined
      return q(null).nodeify(callback);
    }

    // @ts-ignore special case in _send
    return this._request(service, DicReq.Type.COMMAND, {
      args: args, timeout: _.get(options, 'timeout', DisClient.TIMEOUT)
      // @ts-ignore nodeify argument can be null or undefined
    }).nodeify(callback);
  }

  /**
   * @template T=any
   * @param {ServiceInfo|string} service
   * @param {{stamped?: boolean, type?: dimPackets.DicReq.Type, timeout?: number}} options
   * @param {((rep: dimPackets.DicRepInfo<T>|undefined, req: ServiceReg) => any)?=} callback result callback
   * @return {SubId}
   */
  monitor(service, options, callback) {
    var sid = this.nextSID();
    var req = this._sidMap[sid] = {
      service: service,
      type: _.get(options, 'type', DicReq.Type.MONITORED),
      opts: {
        stamped: _.get(options, 'stamped', false),
        timeout: _.get(options, 'timeout', 0)
      },
      callback: callback
    };

    this._send(req, sid);
    return sid;
  }

  /**
   * @brief periodically retrieve a value
   * @template T=any
   * @param {ServiceInfo|string} service  service to retrieve
   * @param {?number=} interval interval in milliseconds
   * @param {?{ stamped?: boolean, timeout?: number }=} options  options
   * @param {((rep: dimPackets.DicRepInfo<T>|undefined, req: ServiceReg) => any)?=} callback value update callback
   * @return {SubId} an Sid that can be aborted (see abort())
   */
  interval(service, interval, options, callback) {
    return this.monitor(service,
      _.assign({ timeout: interval || DisClient.INTERVAL,
        type: DicReq.Type.TIMED_ONLY }, options),
      callback);
  }

  /**
   * @param {SubId} sid
   */
  abort(sid) {
    var req = this._take(sid);
    if (req && this._conn.connected) {
      this._conn.send(new DicReq(_.get(req.service, 'name', req.service),
        DicReq.Type.DELETE, null, sid));
    }
  }
}

DisClient.INTERVAL = 30000; /* in milliseconds */
DisClient.TIMEOUT = 5000; /* in milliseconds */
DisClient.CONN_RETRY_DELAY = 15000; /* in milliseconds */

module.exports = DisClient;
