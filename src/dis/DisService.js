// @ts-check
const
  { EventEmitter } = require('events'),
  ServiceDefinition = require('../ServiceDefinition'),
  _ = require('lodash');

/**
 * @template T
 */
class DisService extends EventEmitter {
  /**
   * @brief create a DIM Service
   * @param {ServiceDefinition|string|undefined} definition the service definition
   * @param {T=} value initial service value
   */
  constructor(definition, value) {
    super();
    this.definition = (definition instanceof ServiceDefinition) ? definition :
      ServiceDefinition.parse(definition);
    this.value = value;
    this.quality = 0;
    this.timestamp = _.now();
  }

  /**
   * @param {T} value
   */
  setValue(value) {
    if (!_.isEqual(value, this.value)) {
      this.value = value;
      this.timestamp = _.now();
      this.emit('update', this.value);
    }
  }

  /**
   * @return {any} value may be transformed before being sent
   */
  getValue() { return this.value; }
}

module.exports = DisService;
