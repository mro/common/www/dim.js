// @ts-check
const
  DisService = require('./DisService'),
  ServiceDefinition = require('../ServiceDefinition'),
  debug = require('debug')('dim:server'),
  _ = require('lodash');

/**
 * @typedef {import('../Conn')['TCPConn']} TCPConn
 * @typedef {import('./types').DisConn} DisConn
 */
/**
 * @typedef {import('../packets/types').DicReqInfo} DicReqInfo<T>
 * @template T
 */

/**
 * @template T=any
 * @extends DisService<(req: DicReqInfo<T>, conn: DisConn) => void>
 */
class DisCmd extends DisService {
  /**
   * @brief create a DIM Command

   * @param {ServiceDefinition|string} definition the command definition
   * @param {(req: DicReqInfo<T>, conn: DisConn) => void} callback the command callback
   */
  constructor(definition, callback) {
    super(definition, callback);
    if (this.definition) {
      this.definition.type = ServiceDefinition.Type.CMD;
    }
  }

  setValue() {}

  getValue() {}

  /**
   * @param {DicReqInfo<T>} req
   * @param {TCPConn} conn
   */
  handle(req, conn) {
    var value = _.invoke(this.definition, 'unpackParams', req.data);
    if (value !== undefined) {
      req.value = value;
    }
    else {
      debug('failed to decode argument', req.data);
    }
    _.invoke(this, 'value', req, conn);
  }
}

module.exports = DisCmd;
