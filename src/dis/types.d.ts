
export as namespace dimDis;

import { DnaNetInfo } from '../packets/types';
import { TCPConn } from '../Conn';

export class DisConn extends TCPConn {
  subscribed: Array<DisSub>

  net: DnaNetInfo

  cid: number

  exitHandler?: number
}
