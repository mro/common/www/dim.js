/* eslint-disable max-lines */
// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('dim:server'),
  { EventEmitter } = require('events'),

  NodeInfo = require('../NodeInfo'),
  ServiceInfo = require('../ServiceInfo'),
  ServiceDefinition = require('../ServiceDefinition'),
  DisService = require('./DisService'),
  DisCmd = require('./DisCmd'),
  DisRpc = require('./DisRpc'),
  utils = require('../utils'),
  { DicReq, DicRep } = require('../packets/DicDis'),
  { DnaNet } = require('../packets/Dna'),
  { TCPServer } = require('../Conn'),
  { ServiceList, ClientList } = require('./DisNode_builtin'),
  { VersionNumber, DEFAULT_HOST } = require('../Consts'),

  DnsClient = require('../dns/DnsClient');

/**
 * @typedef {import('./types').DisConn} DisConn
 * @typedef {import('../packets/types').DnaNetInfo} DnaNetInfo
 * @typedef {import('../Conn')['TCPConn']} TCPConn
 *
 * @typedef {number} SubId
 * @typedef {string} ServiceName
 * @typedef {number} ServiceId
 * @typedef {number} ConnId
 */

/**
 * @template T=any
 * @typedef {import('../packets/types').DicReqInfo} DicReqInfo
 */

class DisServiceInfo {
  /**
   * @param {DisService<any>} service
   * @param {ServiceInfo} info
   */
  constructor(service, info) {
    this.service = service;
    this.info = info;
    /** @type {Array<DisSub>} */
    this.clients = [];
    /** @type {?function} */
    this.cleanup = null;
  }

  remove() {
    if (!_.get(this, 'info.removed')) {
      _.set(this, 'info.removed', true);
      var clients = this.clients;
      this.clients = [];

      _.forEach(clients, (c) => {
        c.remove(); /* remove subscription */
        c.update(null, false); /* send notification */
      });
      _.invoke(this, 'cleanup');
    }
  }

  /**
   * @param {DisConn} conn
   * @param {SubId} subId
   */
  unsubscribe(conn, subId) {
    debug('unsubscribe: service:%s cid:%d subId:%d', this.info.name,
      _.get(conn, 'cid'), subId);
    _.invoke(_.find(this.clients, { conn, subId }), 'remove');
  }

  /**
   * @param {DisConn} conn
   * @param {DicReqInfo<any>} req
   */
  subscribe(conn, req) {
    if (req.type === DicReq.Type.ONCE_ONLY) {
      debug('service request:%s cid:%d subId:%d', this.info.name,
        _.get(conn, 'cid'), req.sid);
      sendUpdate(_.invoke(this.service, 'getValue'), this, conn, req.sid,
        req.format, req.stamped);
    }
    else if (req.type === DicReq.Type.COMMAND) {
      debug('command request:%s cid:%d subId:%d', this.info.name,
        _.get(conn, 'cid'), req.sid);
      _.invoke(this.service, 'handle', req, conn);
    }
    else {
      debug('subscribe: service:%s cid:%d subId:%d type:%s timeout:%d',
        this.info.name, _.get(conn, 'cid'), req.sid,
        _.get(DicReq.TypeStr, req.type, 'unknown'), req.timeout);
      var sub = new DisSub(this, conn, req);
      if ((req.type !== DicReq.Type.MONIT_ONLY) &&
          (req.type !== DicReq.Type.UPDATE)) {
        sub.update(sub.fetch(), false);
      }
    }
  }
}

class DisSub {
  /**
   * @param {DisServiceInfo} serviceInfo
   * @param {DisConn} conn
   * @param {DicReqInfo<any>} req
   */
  constructor(serviceInfo, conn, req) {
    this.srv = serviceInfo;
    this.conn = conn;
    this.subId = req.sid;
    this.type = req.type;
    this.stamped = req.stamped;
    this.format = req.format;

    if ((req.timeout !== 0) &&
        (this.type !== DicReq.Type.MONIT_ONLY) &&
        (this.type !== DicReq.Type.MONIT_FIRST)) {
      this.timer = setInterval(() => {
        this.update(this.fetch(), false);
      }, req.timeout);
    }
    this.conn.subscribed = this.conn.subscribed || {};
    this.conn.subscribed[this.subId] = this;
    this.srv.clients.push(this);
  }

  /**
   * @return {any}
   */
  fetch() {
    return _.invoke(this.srv, 'service.getValue');
  }

  /**
   * @param  {any} value
   * @param {?boolean=} isMod
   */
  update(value, isMod) {
    if (this.type === DicReq.Type.TIMED_ONLY && isMod) { return; }

    sendUpdate(value, this.srv, this.conn, this.subId, this.format,
      this.stamped);
  }

  remove() {
    if (this.timer) {
      clearInterval(this.timer);
      // @ts-ignore
      this.timer = null;
    }
    _.unset(this.conn.subscribed, this.subId);
    _.pull(this.srv.clients, this);
  }
}

/**
 * @param {any} value value to update
 * @param {DisServiceInfo} srv
 * @param {DisConn} conn
 * @param {SubId} subId
 * @param {number} format
 * @param {boolean} stamped
 */
function sendUpdate(value, srv, conn, subId, format, stamped) {
  var size = (!_.isNil(value)) ?
    _.invoke(srv, 'service.definition.paramsSize', value) : null;
  var stamp, quality;

  if (stamped) {
    stamp = _.get(srv, 'service.timestamp');
    quality = _.get(srv, 'service.quality');
  }

  var pkt;
  if (_.get(srv, 'info.removed')) {
    pkt = DicRep.createRemoval(subId);
  }
  else if (_.isNil(size)) {
    /* either definition is not a ServiceDefinition object, or something is
       weird with data */
    pkt = new DicRep(value, stamp, quality, subId);
  }
  else {
    pkt = new DicRep(size, stamp, quality, subId);
    _.invoke(srv, 'service.definition.packParams', value, pkt.writer, format);
  }
  conn.send(pkt);
}


/**
 * DisNode
 *
 * @details emits:
 * - duplicate:services : duplicate services detected
 *     (args: [ service_name ], dupNode, dnsCli)
 * - duplicate:taskName : duplicate taskName detected, DisNode not registered
 *     (args: dupNode, dnsCli)
 * - update : a service has been modified (args: ServiceInfo)
 * - close : when service stops to listen for requests
 * - exit : on exit handler or direct exit calls
 *
 * @details
 * This class adds the following information on connection items:
 * - net: The contents of incoming DnaNet packets
 * - subscribed: an subId -> DisSub subscription map
 * - exitHandler: an Integer code to send on exit (in 'exit' events)
 */
class DisNode extends EventEmitter {
  /**
   * Create a DIM server
   * @param {?string=} addr optional address to listen to
   * @param {?number=} port optional port to listen to (default: random)
   * @param {?NodeInfo=} nodeInfo optional NodeInfo (default: NodeInfo.local)
   */
  constructor(addr, port, nodeInfo) {
    super();

    if (arguments.length !== 3) {
      let i = 0;
      addr = port = nodeInfo = null;
      if (_.isString(arguments[i])) { addr = arguments[i++]; }
      if (_.isInteger(arguments[i])) { port = arguments[i++]; }
      if (arguments[i] instanceof NodeInfo) { nodeInfo = arguments[i++]; }
    }
    addr = _.defaultTo(addr, DEFAULT_HOST);
    this.info = nodeInfo || NodeInfo.local(addr, port);

    _.bindAll(this, [ '_onConnect' ]);

    this._conn = new TCPServer(addr, _.defaultTo(port, 0));
    this._conn.on('connect', this._onConnect);
    this.sid = 1; /* per service ID */
    this.cid = 1; /* per connection ID */

    /** @type {{ [connId: number]: DisConn }} */
    this._connMap = {};
    /** @type {{ [serviceName: string]: DisServiceInfo }} */
    this._srvMap = {};
    /** @type {{ [serviceId: number]: ServiceName }} */
    this._sidMap = {};

    this.serviceList = new ServiceList();
    this.addService(this.info.task + '/SERVICE_LIST', this.serviceList);
    this.addService(this.info.task + '/VERSION_NUMBER', 'L:1', VersionNumber);

    this.clientList = new ClientList();
    this.addService(this.info.task + '/CLIENT_LIST', this.clientList);

    this.addCmd(this.info.task + '/SET_EXIT_HANDLER', 'L:1',
      this.setExitHandler.bind(this));

    this.addCmd(this.info.task + '/EXIT', 'L:1',
      (req) => this.emit('exit', req.value));
  }

  /**
   * @param {DnsClient|string} dns
   * @return {Q.Promise<any>}
   */
  register(dns) {
    var cli = DnsClient.wrap(dns);
    if (!cli) { return q.reject('failed to wrap DNS'); }

    return cli.register(this)
    .thenResolve(cli) // @ts-ignore: checked above
    .finally(() => cli.unref());
  }

  serviceInfo() {
    return _.map(this._srvMap, 'info');
  }

  /**
   * @param  {ServiceName} name
   * @return {boolean}
   */
  isService(name) {
    var srv = _.get(this._srvMap, name);
    return (srv && !_.get(srv.info, 'removed', false)) ? true : false;
  }

  /**
   * @template T
   * @param {string} name
   * @param {DisService<T>} service
   * @param {ServiceDefinition} definition
   * @return {DisServiceInfo}
   */
  _add(name, service, definition) {
    var info = new ServiceInfo(name, definition, this.nextSID(), this.info);

    var srv = this._srvMap[name] = new DisServiceInfo(service, info);
    /* @ts-ignore: we're sure it's a ServiceID there (since we've just created it) */
    this._sidMap[info.sid] = name;

    if (definition.type === ServiceDefinition.Type.SRV) {
      this._hookUpdate(srv);
    }
    this.emit('update', info);
    return srv;
  }

  /**
   * @param {DisServiceInfo} srv
   */
  _hookUpdate(srv) {
    var up = this._onServiceUpdate.bind(this, srv);
    srv.cleanup = srv.service.removeListener.bind(srv.service, 'update', up);
    srv.service.on('update', up);
  }

  /**
   * @param {DisServiceInfo} srvInfo
   * @param {any} value
   * @param {DisConn} conn
   */
  _onServiceUpdate(srvInfo, value, conn) {
    if (_.isNil(conn)) {
      _.forEach(srvInfo.clients, (c) => c.update(value, true));
    }
    else {
      _.forEach(srvInfo.clients, (c) => {
        if (c.conn === conn) { c.update(value, true); }
      });
    }
  }

  /**
   * @template T
   * @param {ServiceName} name
   * @param {DisService<T>|ServiceDefinition|string} definition
   * @param {any=} value
   * @return {?DisService<T>}
   */
  addService(name, definition, value) {
    if (this.isService(name)) {
      debug('service already registered:', name);
      return null;
    }
    var service = (definition instanceof DisService) ?
      definition : new DisService(definition, value);

    if (!service.definition) {
      debug('unable to add service: missing definition');
      return null;
    }

    this._add(name, service, service.definition);
    /* $FlowIgnore */
    this.serviceList.add(name, service.definition);

    return service;
  }

  /**
   * @param {ServiceName} name
   * @return {boolean}
   */
  removeService(name) {
    var srv = _.get(this._srvMap, name);
    if (_.isNil(srv)) { return false; }

    _.unset(this._srvMap, name);
    // @ts-ignore: checked above
    _.unset(this._sidMap, srv.info.sid);
    _.invoke(srv, 'remove');

    this.serviceList.remove(name);
    this.emit('update', srv.info);
    return true;
  }

  /**
   * @template T=any
   * @param {ServiceName} name
   * @param {DisCmd<T>|ServiceDefinition|string} definition
   * @param {((req: DicReqInfo<T>, conn: DisConn) => any)?=} cb
   * @return {?DisCmd<T>}
   */
  addCmd(name, definition, cb) {
    if (this.isService(name)) {
      debug('service already registered:', name);
      return null;
    }
    var cmd;
    if (definition instanceof DisCmd) {
      cmd = definition;
    }
    else if (cb) {
      cmd = new DisCmd(definition, cb);
    }
    if (!cmd || !cmd.definition) {
      return null;
    }

    this._add(name, cmd, cmd.definition);
    /* $FlowIgnore */
    this.serviceList.add(name, cmd.definition);

    return cmd;
  }

  /**
   * @param  {ServiceName} name
   */
  removeCmd(name) { this.removeService(name); }

  /**
   * @template ARGS=any
   * @template RET=any
   * @param {ServiceName} name
   * @param {DisRpc<ARGS, RET>|ServiceDefinition|string} definition
   * @param {((req: DicReqInfo<ARGS>, conn: DisConn) => any)?=} cb
   * @return {?DisRpc<ARGS, RET>}
   */
  addRpc(name, definition, cb) {
    if (this.isService(name + '/RpcIn') || this.isService(name + '/RpcOut')) {
      debug('service already registered:', name);
      return null;
    }
    var service;
    if (definition instanceof DisRpc) {
      service = definition;
    }
    else if (cb) {
      service = new DisRpc(definition, cb);
    }
    if (!service || !(service.definition instanceof ServiceDefinition)) {
      return null;
    }

    /* @ts-ignore: checked above */
    this._add(name + '/RpcIn', service.rpcIn, service.rpcIn.definition);
    /* @ts-ignore: checked above */
    this._add(name + '/RpcOut', service.rpcOut, service.rpcOut.definition);
    /* @ts-ignore: checked above */
    this.serviceList.add(name, service.definition);

    return service;
  }

  /**
   * @param  {string} name
   */
  removeRpc(name) {
    this.removeService(name + '/RpcIn');
    this.removeService(name + '/RpcOut');
    this.serviceList.remove(name);
  }

  /**
   * @param  {ServiceId} sid
   * @return {?DisServiceInfo}
   */
  getServiceBySid(sid) {
    return _.get(this._srvMap, _.get(this._sidMap, sid));
  }

  listen() {
    if (_.isNil(this._listen)) {
      this._listen = this._conn.listen()
      .then(() => {
        this._conn.once('close', this.emit.bind(this, 'close'));
        debug('serving requests on: %s:%d', this._conn.host, this._conn.port);
        this.info.setAddress(this._conn.host, this._conn.port);
        if (utils.isAnyAddress(this.info.name)) {
          this.info.name = this._conn.host;
        }

        /* NAME_MAX includes trailing \0 */
        if (_.size(this.info.name) >= NodeInfo.NAME_MAX) {
          if (!utils.isAnyAddress(this._conn.host) &&
            _.size(this._conn.host) < NodeInfo.NAME_MAX) {
            this.info.name = this._conn.host;
          }
          else {
            this.info.name = utils.getPublicAddress();
          }
          debug('name too long, renaming:', this.info.name);
        }
        return this;
      });
    }
    return this._listen;
  }

  isListening() {
    return _.invoke(this._listen, 'isFulfilled') || false;
  }

  close() {
    debug('closing: %s:%d', this._conn.host, this._conn.port);
    if (this._listen) {
      this._listen = null;
      this._conn.removeListener('connect', this._onConnect);
      this._conn.close();
    }

    _.forEach(this._connMap, (conn, cid) => {
      // @ts-ignore
      this._removeConn(cid);
      _.invoke(conn, 'close');
    });
    this._connMap = {};
    _.forEach(this._srvMap, (service) => _.invoke(service, 'remove'));
  }

  /**
   * @param {DisConn} conn
   */
  _onConnect(conn) {
    var cid = conn.cid = this.cid++;
    this._connMap[cid] = conn;
    conn.once('close', this._removeConn.bind(this, cid));
    conn.on('packet', (pkt) => this._onPacket(pkt, conn));
  }

  /**
   * @param  {Buffer} pkt
   * @param  {DisConn} conn
   */
  _onPacket(pkt, conn) {
    if (!_.has(conn, 'net') && this._onNetPacket(pkt, conn)) { return; }

    var req = DicReq.parse(pkt);
    if (!req) {
      debug('invalid packet:', pkt);
      return;
    }

    var service = this._srvMap[req.service];
    if (_.isNil(service)) {
      debug('invalid service requested:', req.service);
      conn.close();
      return;
    }

    switch (req.type) {
    case DicReq.Type.DELETE:
      service.unsubscribe(conn, req.sid); break;
    case DicReq.Type.COMMAND:
    default:
      service.subscribe(conn, req); break;
    }
  }

  /**
   * @param  {Buffer} pkt
   * @param  {DisConn} conn
   * @return {?DnaNetInfo}
   */
  _onNetPacket(pkt, conn) {
    var net = DnaNet.parse(pkt);
    if (!net) { return null; }

    debug('net packet: name:%s task:%s', net.name, net.task);
    conn.net = net;
    this.clientList.add(net);
    return net;
  }

  /**
   * @param {ConnId} cid
   */
  _removeConn(cid) {
    var conn = this._connMap[cid];
    if (!conn) { debug('unknown connection cid:', cid); return; }

    if (_.has(conn, 'exitHandler')) {
      this.emit('exit', conn.exitHandler, conn);
    }

    debug('removing connection cid:%d net:%o', cid, conn.net);
    conn.removeAllListeners();

    /* DIC connection */
    _.forEach(conn.subscribed, (sub) => sub.remove());
    this.clientList.remove(conn.net);
    delete this._connMap[cid];
  }

  nextSID() {
    this.sid = (this.sid + 1) & 0xFFFFFFFF; /* limit sid to 32 bits */
    return this.sid;
  }

  /**
   * @param {DicReqInfo<any>} req
   * @param {DisConn} conn
   */
  setExitHandler(req, conn) {
    _.set(conn, 'exitHandler', req.value);
  }
}

module.exports = DisNode;
