// @ts-check
const
  DisClient = require('./DisClient'),
  DisCmd = require('./DisCmd'),
  DisRpc = require('./DisRpc'),
  DisNode = require('./DisNode'),
  DisService = require('./DisService');

module.exports = { DisClient, DisCmd, DisRpc, DisNode, DisService };
