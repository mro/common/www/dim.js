// @ts-check
const
  _ = require('lodash'),
  ServiceDefinition = require('../ServiceDefinition'),
  DisService = require('./DisService');

/**
 * @extends DisService<Set<string>>
 */
class ClientList extends DisService {
  constructor() {
    super('C');
    this.value = new Set();
  }

  setValue() { /* disable*/ }

  /**
   * @param {{ name:string, task:string }} nodeInfo
   */
  add(nodeInfo) {
    if (!_.isObject(nodeInfo)) { return; }
    var name = nodeInfo.task + '@' + nodeInfo.name;

    if (!this.value.has(name)) {
      this.value.add(name);
      this.emit('update', '+' + name);
    }
  }

  /**
   * @param {{ name:string, task:string }} nodeInfo
   */
  remove(nodeInfo) {
    if (!_.isObject(nodeInfo)) { return; }
    var name = nodeInfo.task + '@' + nodeInfo.name;

    if (this.value.delete(name)) {
      this.emit('update', '-' + name);
    }
  }

  getValue() {
    return Array.from(this.value).join('|');
  }
}

/**
 * @extends DisService<{ [service: string]: string }>
 */
class ServiceList extends DisService {
  constructor() {
    super('C');
    /** @type {{ [serviceName: string]: string }} */
    this.value = {};
  }

  setValue() { /* disable*/ }

  /**
   * @param  {string} name
   * @param {ServiceDefinition|string} definition
   * @return {string}
   */
  _getDesc(name, definition) {
    if (definition instanceof ServiceDefinition) {
      return name + '|' + definition.toString(true);
    }
    else {
      return name + '|' + definition + '|';
    }
  }

  /**
   * @param  {string} name
   * @param {ServiceDefinition|string} definition
   */
  add(name, definition) {
    if (!_.has(this.value, name)) {
      const desc = this._getDesc(name, definition);
      this.value[name] = this._getDesc(name, definition);
      this.emit('update', '+' + desc);
    }
  }

  /**
   * @param  {string} name
   */
  remove(name) {
    const desc = this.value[name];
    if (desc && _.unset(this.value, name)) {
      this.emit('update', '-' + desc);
    }
  }

  getValue() {
    return _.values(this.value).join('\n');
  }
}

module.exports = { ClientList, ServiceList };
