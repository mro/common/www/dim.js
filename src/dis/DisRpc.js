// @ts-check
const
  q = require('q'),
  DisService = require('./DisService'),
  DisCmd = require('./DisCmd'),
  ServiceDefinition = require('../ServiceDefinition'),
  _ = require('lodash');

/**
 * @typedef {import('./types').DisConn} DisConn
 */

/**
 * @typedef {import('../packets/types').DicReqInfo} DicReqInfo
 * @template T
 */

/**
 * @template ARGS = any
 * @template RET = any
 */
class DisRpc {
  /**
   * @brief create a DIM Rpc service
   * @param {ServiceDefinition|string} definition the rpc definition
   * @param {(req: DicReqInfo<ARGS>, conn: DisConn) => RET} callback the rpc callback
   */
  constructor(definition, callback) {
    this.value = callback;
    this.definition = (definition instanceof ServiceDefinition) ? definition :
      ServiceDefinition.parse(definition);

    if (this.definition instanceof ServiceDefinition) {
      _.set(this.definition, 'type', ServiceDefinition.Type.RPC);
      this.rpcIn = new DisCmd(new ServiceDefinition({
        /* $FlowIgnore: checked above */
        params: this.definition.params,
        type: ServiceDefinition.Type.CMD
      }), this.handle.bind(this));
      this.rpcOut = new DisService(new ServiceDefinition({
        params: this.definition.returns,
        type: ServiceDefinition.Type.SRV
      }));
    }
    else {
      _.unset(this, 'definition');
    }
  }

  /**
   * @param  {DicReqInfo<ARGS>} req
   * @param  {DisConn} conn
   * @return {Q.Promise<void>}
   */
  handle(req, conn) {
    return q()
    .then(() => _.invoke(this, 'value', req, conn))
    .then((ret) => {
      if (this.rpcOut) {
        this.rpcOut.emit('update', ret, conn);
      }
    });
  }
}

module.exports = DisRpc;
