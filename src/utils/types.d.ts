
// extending lodash
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import _ from 'lodash';

// eslint-disable-next-line no-redeclare
declare module "lodash" {
  // eslint-disable-next-line no-redeclare
  declare interface LoDashStatic {
    push<T>(array: ?T[], value: T): T[]
  }
}
