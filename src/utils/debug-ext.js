// @ts-check
const
  debug = require('debug');

/**
 * @param  {any} v
 */
debug.formatters.x = function(v) {
  try {
    return v.toString(16);
  }
  catch (e) { return "NaN"; }
};

/**
 * @param  {number} v
 * @return {string}
 */
debug.formatters.X = function(v) {
  try {
    return v.toString(16).toUpperCase();
  }
  catch (e) { return "NaN"; }
};

module.exports = debug;
