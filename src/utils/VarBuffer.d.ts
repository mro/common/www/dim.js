export = VarBuffer;
declare class VarBuffer {
  constructor(buf?: ?(Buffer | number));

    length: number;

    _buffer: Buffer;

    reset(): void;

    add(data: Buffer): void;

    buffer(): Buffer;

    slice(start: number, end: number): Buffer;

    reserved(): number;

    resize(size: number): void;

    shift(n: number): Buffer|undefined;

    copy(
      target: Buffer | VarBuffer,
      targetStart: number,
      sourceStart: number,
      sourceEnd: number): number;

    write(
      string: string,
      offset?: number,
      length?: number,
      encoding?: string): void

    writeUInt8(value: number, offset: number): number;

    writeUInt16LE(value: number, offset: number): number;

    writeUInt16BE(value: number, offset: number): number;

    writeUInt32LE(value: number, offset: number): number;

    writeUInt32BE(value: number, offset: number): number;

    writeInt8(value: number, offset: number): number;

    writeInt16LE(value: number, offset: number): number;

    writeInt16BE(value: number, offset: number): number;

    writeInt32LE(value: number, offset: number): number;

    writeInt32BE(value: number, offset: number): number;

    writeDoubleLE(value: number, offset: number): number;

    writeDoubleBE(value: number, offset: number): number;

    writeFloatLE(value: number, offset: number): number;

    writeFloatBE(value: number, offset: number): number;
}
// eslint-disable-next-line no-redeclare
declare namespace VarBuffer {
    const BUF_SZ: number;
}
