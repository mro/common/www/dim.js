/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-02-01T08:48:10+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

var _ = require('lodash');

/**
 * @import './types'
 */

/**
 * push an element in an array, mutating the array or creating a new one if it's
 * not an array (null or undefined).
 * @template T
 * @param  {?T[]} list     the array to push in
 * @param  {T}   element  the element to insert
 * @return {T[]}          the mutated or created array
 */ // @ts-ignore: not sure why this duplicates
_.push = function(list, element) {
  if (!_.isArray(list)) {
    return [ element ];
  }
  list.push(element);
  return list;
};

module.exports = _;
