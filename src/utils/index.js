// @ts-check
const
  os = require('os'), // Node only
  url = require('url'),
  _ = require('lodash'),
  { DEFAULT_LOCALHOST } = require('../Consts');

const isNode = (typeof process === 'object');

/**
 * @brief convert string to int (IPv4) address
 * @param  {string|number} addr
 * @return {?number}
 */
function toIntAddr(addr) {
  if (_.isString(addr)) {
    const nums = _.map(addr.split('.'), _.toNumber);
    if (nums.length !== 4) {
      return null;
    }
    /* zero shift right to unsigned */
    addr = ((nums[0] << 24) | (nums[1] << 16) | (nums[2] << 8) | nums[3]) >>> 0;
  }
  return addr;
}

/**
 * @brief convert address (IPv4 int or string) to string representation
 * @param  {string|number} addr
 * @return {?string}
 */
function toStrAddr(addr) {
  if (_.isNumber(addr)) {
    addr = ((addr >> 24) & 0xFF) + '.' +
      ((addr >> 16) & 0xFF) + '.' +
      ((addr >> 8) & 0xFF) + '.' + (addr & 0xFF);
  }
  /* $FlowIgnore: checked with isNumber */
  return addr;
}

/**
 * @brief parse and split an url
 * @param  {string} u url to parse
 * @return {?{ host: ?string, port: number }}
 */
function parseUrl(u) {
  if (!_.includes(u, '://')) {
    u = 'tcp://' + u;
  }
  var parsed = url.parse(u);
  if (parsed) {
    return { host: parsed.hostname, port: _.toInteger(parsed.port) };
  }
  return null;
}

function defaultNodeName() {
  return os.hostname();
}

function defaultTaskName() {
  // used by builtin services, use NodeInfo.local(0, 0, 'name') to create
  // multiple nodes on a single host
  return os.hostname();
}

/**
 * @brief return public address
 * @details if multiple addresses are detected, the first one is returned
 * @return {string} address
 */
function getPublicAddress() {
  const addrList = _.filter(_.flatten(_.values(os.networkInterfaces())),
    { internal: false });
  if (addrList.length > 1) {
    // @ts-ignore: checked above
    const addr = addrList[0].address;
    console.warn('multiple public address detected, using:', addr);
    return addr;
  }
  else if (addrList.length === 0) {
    console.warn(`no public address, using: ${DEFAULT_LOCALHOST}`);
    return DEFAULT_LOCALHOST;
  }
  else {
    return _.get(addrList, [ 0, 'address' ], DEFAULT_LOCALHOST);
  }
}

/**
 * @param  {string}  addr
 * @return {boolean}
 */
function isAnyAddress(addr) {
  return addr === '::' || addr === '0.0.0.0';
}

function pid() {
  return isNode ? process.pid : 0;
}

module.exports = {
  toIntAddr, toStrAddr, defaultNodeName, defaultTaskName, pid, parseUrl,
  getPublicAddress, isAnyAddress
};
