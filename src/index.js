// @ts-check

const
  dic = require('./dic'),
  dis = require('./dis'),
  dns = require('./dns'),
  packets = require('./packets'),
  NodeInfo = require('./NodeInfo'),
  ServiceInfo = require('./ServiceInfo'),
  ServiceDefinition = require('./ServiceDefinition'),
  Errors = require('./Errors');

const _dim = {
  packets, NodeInfo, ServiceInfo, ServiceDefinition, Errors,
  ...dic, ...dis, ...dns };

module.exports = _dim;
