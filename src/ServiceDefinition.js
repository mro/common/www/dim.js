// @ts-check

const
  _ = require('lodash'),
  debug = require('debug')('dim:service'),
  { BufferIn, BufferOut } = require('./utils/IOBuffer'),
  { Service } = require('./Consts');

/**
 * @typedef {number|string} ParamCount
 * @typedef { {fmt: string, count: ParamCount} } Param
 * @typedef { {name: string, size: number} } ParamFmt
 */

/**
 * @param       {string} type
 * @param       {boolean} last
 * @return      {? Param}
 */
function _parseType(type, last) {
  var p = _.split(type, ':');
  var fmt = p[0];
  var count;

  if (!_.has(ServiceDefinition.Fmt, fmt)) {
    debug('no such type:', type);
    return null;
  }
  else if (p[1]) {
    count = _.toInteger(p[1]);
    if (count === 0) {
      debug('failed to parse type:', type);
      return null;
    }
  }
  else {
    count = last ? ServiceDefinition.PARAM_ANY : 1;
  }
  return { fmt, count };
}

/**
 * @param  {any}  values
 * @param  {any}  params
 * @return {boolean}
 */
function _isSingleValue(values, params) {
  /* a single character char won't be detected as a single value with this,
    but it doesn't really matter since 'x'[0] === 'x' */
  return (_.size(params) === 1) &&
    (!_.isArray(values) || _.size(values) > 1);
}

/**
 * @param       {any} value
 * @param       {Param} param
 * @return      {number}
 */
function _paramSize(value, param) {
  /** @type {? ParamFmt} */ /* @ts-ignore */
  var desc = ServiceDefinition.Fmt[param.fmt];
  if (_.isNil(desc)) {
    debug('failed to find desc for:', param);
    return 0;
  }
  else if (param.count === ServiceDefinition.PARAM_ANY) {
    if (param.fmt === 'C') {
      return _.size(value) + 1;
    }
    else if (_.isArray(value)) {
      return _.size(value) * desc.size;
    }
    return desc.size;
  }
  /* @ts-ignore: checked above */
  return desc.size * param.count;
}

/**
 * @param       {string} fmt
 * @param       {ParamCount} count
 * @param       {BufferIn} buffer
 * @param       {number} [format]
 * @return      {any}        [description]
 */
// eslint-disable-next-line complexity
function _unpackValue(fmt, count, buffer, format) {
  var ret;
  if (count === ServiceDefinition.PARAM_ANY) {
    if (fmt === 'C') {
      ret = buffer.readString(buffer.bytesLeft());
    }
    else {
      // @ts-ignore
      var desc = ServiceDefinition.Fmt[fmt];
      if ((buffer.bytesLeft() - desc.size) < desc.size) {
        ret = _unpackValue(fmt, 1, buffer, format);
      }
      else {
        ret = [];
        while (!buffer.fail && (buffer.bytesLeft() >= desc.size)) {
          ret.push(_unpackValue(fmt, 1, buffer, format));
        }
      }
    }
  } /* $FlowIgnore: checked above */
  else if (count > 1) {
    ret = [];
    /* $FlowIgnore: checked above */
    for (let idx = 0; idx < count; ++idx) {
      ret.push(_unpackValue(fmt, 1, buffer, format));
    }
  }
  else {
    var be = format ? ((format & Service.Fmt.BIT_BE) !== 0) : false;
    switch (fmt) {
    case 'I':
    case 'L': ret = buffer.readInt32(be); break;
    case 'C': ret = buffer.readInt8(); break;
    case 'S': ret = buffer.readInt16(be); break;
    case 'F': ret = buffer.readFloat(be); break;
    case 'D': ret = buffer.readDouble(be); break;
    case 'X': ret = buffer.readInt64(be); break;
    }
  }
  return ret;
}

/**
 * @param       {any} value
 * @param       {string} fmt
 * @param       {ParamCount} count
 * @param       {BufferOut} buffer
 * @param       {number} [format]
 * @return      {boolean}
 */
/* eslint-disable-next-line max-len, complexity */
function _packValue(value, fmt, count, buffer, format) {
  if (count === ServiceDefinition.PARAM_ANY) {
    if (fmt === 'C' && _.isString(value)) {
      buffer.writeString(value);
    }
    else if (_.isArray(value)) {
      _.forEach(value,
        (v) => _packValue(v, fmt, 1, buffer, format));
    }
    else {
      _packValue(value, fmt, 1, buffer, format);
    }
    return !buffer.fail;
  } /* $FlowIgnore: checked above */
  else if (count > 1) {
    /* $FlowIgnore: checked above */
    for (var idx = 0; idx < count; ++idx) {
      _packValue(value[idx] || 0, fmt, 1, buffer, format);
    }
  }
  else {
    var be = format ? ((format & Service.Fmt.BIT_BE) !== 0) : false;
    switch (fmt) {
    case 'I':
    case 'L': buffer.writeInt32(value, be); break;
    case 'C': buffer.writeInt8(value); break;
    case 'S': buffer.writeInt16(value, be); break;
    case 'F': buffer.writeFloat(value, be); break;
    case 'D': buffer.writeDouble(value, be); break;
    case 'X': buffer.writeInt64(value, be); break;
    }
  }
  return !buffer.fail;
}

class ServiceDefinition {
  /**
  * @param {Partial<ServiceDefinition>} [other]
  */
  constructor(other) {
    /** @type {?number} */
    this.type = null;

    /** @type {? Array<Param>} */
    this.params = null;

    /** @type {? Array<Param>} */
    this.returns = null;
    _.assign(this, other);
  }

  /**
   * Parses a service-definition string
   * @param  {string|any} [description] the service definition to parse
   * @return {? ServiceDefinition} a ServiceDefinition object or null on error
   *
   * @details also parses RPC definitions, adding a 'returns' attribute.
   */
  static parse(description) {
    if (!_.isString(description)) { return null; }
    var desc = _.split(description, '|');

    const ret = new ServiceDefinition();
    ret.type = _.get(ServiceDefinition.Type, desc[1]);

    if (ret.type === undefined) {
      ret.type = _.includes(description, ',') ?
        ServiceDefinition.Type.RPC : ServiceDefinition.Type.SRV;
    }

    if (ret.type === ServiceDefinition.Type.RPC) {
      const args = _.split(desc[0], ',');
      ret.params = ServiceDefinition._parseParams(args[0]);
      ret.returns = ServiceDefinition._parseParams(args[1]);
    }
    else {
      ret.params = ServiceDefinition._parseParams(desc[0]);
    }
    return ret.isValid() ? ret : null;
  }

  /**
   * @param  {string} params
   * @return {Array<Param>}
   */
  static _parseParams(params) {
    // $FlowFixMe: lodash split works with strings
    const splitParams = _.isEmpty(params) ? [] : _.split(params, ';');
    var last = _.size(splitParams) - 1;
    // @ts-ignore: will be checked using isValid()
    return _.map(splitParams,
      (p, idx) => _parseType(p, idx === last));
  }

  isValid() {
    return !(_.isNil(this.type) || _.some(this.params, _.isNil));
  }

  /**
   * @param  {boolean} trailSep
   * @return {string}
   */
  toString(trailSep) {
    var ret = ServiceDefinition.toStringParams(this.params);
    switch (this.type) {
    case ServiceDefinition.Type.RPC:
      if (_.size(this.returns) > 0) {
        ret += ',' + ServiceDefinition.toStringParams(this.returns);
      }
      ret += '|RPC';
      break;
    case ServiceDefinition.Type.CMD:
      ret += '|CMD';
      break;
    default:
      if (trailSep) {
        ret += '|';
      }
    }
    return ret;
  }

  /**
   * @param  {? Array<Param>} params
   * @return {string}
   */
  static toStringParams(params) {
    return _.reduce(params, (ret, p) => {
      if (!_.isEmpty(ret)) {
        ret = ret + ';';
      }
      if (p.count === ServiceDefinition.PARAM_ANY) {
        return ret + p.fmt;
      }
      return ret + p.fmt + ':' + p.count;
    }, '');
  }

  /**
   * @param {? Array<Param>} params
   * @param {BufferIn|Buffer} buffer
   * @param {number} [format]
   * @return any
   */
  /* eslint-disable-next-line max-len */
  _unpack(params, buffer, format) {
    format = _.defaultTo(format, Service.FMT_DEFAULT);
    var bufferIn = BufferIn.wrap(buffer);

    if (_.size(params) === 1) {
      // @ts-ignore: checked above
      const p = params[0];
      return _unpackValue(p.fmt, p.count, bufferIn, format);
    }
    else {
      /* $FlowIgnore */ /* eslint-disable-next-line max-len */
      return _.map(params, (p) => _unpackValue(p.fmt, p.count, bufferIn, format));
    }
  }

  /**
   * @param  {? Array<Param>} params
   * @param  {any} values
   * @param  {BufferOut|Buffer} buffer
   * @param  {number} [format]
   * @return {any}
   */
  /* eslint-disable-next-line max-len */
  _pack(params, values, buffer, format) {
    format = _.defaultTo(format, Service.FMT_DEFAULT);
    var bufferOut = BufferOut.wrap(buffer);

    if (Buffer.isBuffer(values)) {
      /* prepacked data, let's trust our clients */
      bufferOut.add(values);
    }
    else if (_isSingleValue(values, params)) {
      // @ts-ignore: checked above
      const p = params[0];
      _packValue(values, p.fmt, p.count, bufferOut, format);
    }
    else {
      _.forEach(params,
        (p, idx) => _packValue(values[idx], p.fmt, p.count, bufferOut, format));
    }
    return !bufferOut.fail;
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @param  {number} [format]
   * @return {any}
   */
  unpackParams(buffer, format) {
    return this._unpack(this.params, buffer, format);
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @param  {number} [format]
   * @return {any}
   */
  unpackReturns(buffer, format) {
    return this._unpack(this.returns, buffer, format);
  }

  /**
   * @param  {any} values
   * @param  {BufferOut|Buffer} buffer
   * @param  {number} [format]
   * @return {any}
   */
  /* eslint-disable-next-line max-len */
  packParams(values, buffer, format) {
    return this._pack(this.params, values, buffer, format);
  }

  /**
   * @param  {any} values
   * @param  {BufferOut|Buffer} buffer
   * @param  {number} [format]
   * @return {any}
   */
  /* eslint-disable-next-line max-len */
  packReturns(values, buffer, format) {
    return this._pack(this.returns, values, buffer, format);
  }

  /**
   * @param  {any} values
   * @return {number}
   */
  paramsSize(values) {
    if (_.isNil(values)) {
      return 0;
    }
    else if (Buffer.isBuffer(values)) {
      return values.length;
    }
    else if (_isSingleValue(values, this.params)) {
      // @ts-ignore: checked above
      return _paramSize(values, this.params[0]);
    }
    /* eslint-disable-next-line max-len */
    return _.reduce(this.params, (ret, param, idx) => ret + _paramSize(values[idx], param), 0);
  }
}

ServiceDefinition.PARAM_ANY = '*';
ServiceDefinition.Fmt = {
  I: { name: "integer", size: 4 },
  C: { name: "character", size: 1 },
  L: { name: "long", size: 4 }, /* deprecated */
  S: { name: "short", size: 2 },
  F: { name: "float", size: 4 },
  D: { name: "double", size: 8 },
  X: { name: "extra long", size: 8 }
};
ServiceDefinition.NamedFmt = {
  Int16: 'S', Int32: 'I', Int64: 'X',
  Char: 'C', String: 'C',
  Float: 'F',
  Double: 'D'
};
ServiceDefinition.Type = { RPC: 0, CMD: 1, SRV: 2 };

module.exports = ServiceDefinition;
