export as namespace dimPackets;

import ServiceInfo from '../ServiceInfo';
import NodeInfo from '../NodeInfo';

export interface DicReqInfo<T=any> {
  service: string
  sid: number
  type: number
  stamped: boolean
  timeout: number
  format: number
  data: Buffer|null
  value?: T
}

export interface DicRepInfo<T=any> {
  value?: T
  data: Buffer
  timestamp?: number
  quality?: number
  sid: number
  removed?: true
}

export interface DnaNetInfo {
  name: string,
  task : string
}

export interface DnsDupInfo {
  name: string,
  services: Array<number>,
  task: string,
  pid: number,
  port: number
}

export interface DnsRegInfo {
  node: NodeInfo,
  services: Array<ServiceInfo>
}

export interface DnsCmdInfo {
  cmd: number,
  info: number,
  data: (DnsDupInfo|Buffer|null)
}

export namespace DicReq {
  export enum Type {
    ONCE_ONLY, TIMED, MONITORED, COMMAND, DELETE,
    MONIT_ONLY, UPDATE, TIMED_ONLY, MONIT_FIRST
  }
}
