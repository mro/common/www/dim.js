// @ts-check
const
  _ = require('lodash'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  { DnaHeader } = require('./Dna'),
  NodeInfo = require('../NodeInfo'),
  ServiceInfo = require('../ServiceInfo'),
  { Service, SrcTypes } = require('../Consts');

/**
 * @typedef {{service:string, sid:number}} DnsReqInfo
 */

class DicDnsReq extends DnaHeader {
  /**
   * @param {string=} service
   * @param {number=} sid
   */
  constructor(service, sid) {
    super(DicDnsReq.LEN);
    this.sid = _.defaultTo(sid, 1);

    var w = new BufferOut(this.data());

    w.writeUInt32(DicDnsReq.LEN);
    w.writeUInt32(DicDnsReq.TYPE);
    w.writeString(service || '', Service.NAME_MAX);
    w.writeUInt32(this.sid);
  }

  /**
   * @param  {Buffer|BufferIn} buffer
   * @return {?DnsReqInfo}
   */
  static parse(buffer) {
    buffer = BufferIn.wrap(buffer);

    if (buffer.buffer.length < DicDnsReq.LEN ||
        buffer.readUInt32() !== DicDnsReq.LEN) {
      return null;
    }
    else if (buffer.readUInt32() !== SrcTypes.DIC) {
      return null;
    }
    else {
      const service = buffer.readString(Service.NAME_MAX);
      const sid = buffer.readUInt32();
      return buffer.fail ? null : { service, sid };
    }
  }
}
DicDnsReq.LEN = 144;
DicDnsReq.TYPE = SrcTypes.DIC;

class DicDnsRep extends DnaHeader {
  /**
   * @param {string=} definition
   * @param {(NodeInfo|string|object)=} nodeInfo
   * @param {number=} sid
   */
  constructor(definition, nodeInfo, sid) {
    super(DicDnsRep.LEN);
    this.sid = _.defaultTo(sid, 1);

    var w = new BufferOut(this.data());

    w.writeUInt32(DicDnsRep.LEN);
    w.writeUInt32(this.sid);
    w.writeString(definition || '', Service.DEF_MAX);
    const info = NodeInfo.wrap(nodeInfo || '');
    info.toBuffer(w);
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @return {?ServiceInfo}
   */
  static parse(buffer) {
    buffer = BufferIn.wrap(buffer);

    if (buffer.buffer.length < DicDnsRep.LEN ||
        buffer.readUInt32() !== DicDnsRep.LEN) {
      return null;
    }
    var sid = buffer.readUInt32();
    /** @type {string|null} */
    var definition = buffer.readString(Service.DEF_MAX);
    var node = NodeInfo.fromBuffer(buffer);
    if (!node || !node.isValid()) {
      /* current implentation sends garbage for unknown nodes */
      definition = null;
    }
    return buffer.fail ? null : (new ServiceInfo(null, definition, sid, node));
  }
}

DicDnsRep.LEN = 236;

module.exports = {
  SrcTypes,
  DicDnsReq, DicDnsRep
};
