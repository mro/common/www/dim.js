// @ts-check
const
  _ = require('lodash'),
  debug = require('debug')('dim:pkt'),
  NodeInfo = require('../NodeInfo'),
  ServiceInfo = require('../ServiceInfo'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  { DnaHeader } = require('./Dna'),
  { SrcTypes } = require('../Consts');

/**
 * @template V
 * @param  {{ [key: number]: V }} groups
 * @param  {any} _value
 * @param  {number}   key
 * @return {V}
 */
function reMapKey(groups, _value, key) { return groups[key]; }

/* eslint-disable-next-line max-len */
const dupRe = /^([\w.:+-]*)\(([\d]+)\)@([\w.:+-]*):([\d]+) [\d]+((?: [\da-fA-F]{8})*)$/;
const dupReGroups = reMapKey.bind(null,
  [ 'all', 'task', 'pid', 'name', 'port', 'dups' ]);

/**
 * @typedef {import('./types').DnsDupInfo} DnsDupInfo
 * @typedef {import('./types').DnsRegInfo} DnsRegInfo
 * @typedef {import('./types').DnsCmdInfo} DnsCmdInfo
 */

/**
 * @param  {string} str
 * @return {?DnsDupInfo}
 */
function parseDup(str) {
  var matchs = _.toString(str).match(dupRe);

  if (!matchs || _.isEmpty(matchs)) { return null; }

  const ret = _.mapKeys(matchs, dupReGroups);
  return {
    services: _.map(_.trim(ret.dups).split(' '), (d) => parseInt(d, 16)),
    task: ret.task,
    pid: parseInt(ret.pid, 10),
    name: ret.name,
    port: parseInt(ret.port, 10)
  };
}

/**
 * Dis to Dns register packet
 */
class DisDnsReg extends DnaHeader {
  /**
   * DisDns Registration packet
   * @param {NodeInfo} node NodeInfo object
   * @param {Array<ServiceInfo>|ServiceInfo} services services description
   */
  constructor(node, services) {
    node = NodeInfo.wrap(node);
    if (services instanceof ServiceInfo) {
      services = [ services ];
    }

    var size = DisDnsReg.LEN_MIN +
      (_.size(services) * DisDnsReg.SRV_LEN);
    super(size);

    var w = new BufferOut(this.data());

    w.writeUInt32(size);
    w.writeUInt32(DisDnsReg.TYPE);
    if (!node.toBuffer(w)) {
      debug('failed to serialize NodeInfo');
    }

    w.writeUInt32(_.size(services));
    _.forEach(services, (/** @type {ServiceInfo} */ s) => s.toBuffer(w));
    if (w.fail) {
      /* @ts-ignore: packet is invalid */
      delete this.raw;
    }
  }

  /**
   * @param {BufferIn|Buffer} buffer
   * @return {?DnsRegInfo}
   */
  static parse(buffer) {
    buffer = BufferIn.wrap(buffer);

    if (buffer.buffer.length < DisDnsReg.LEN_MIN) {
      return null;
    }
    var nbServices = (buffer.readUInt32() - DisDnsReg.LEN_MIN) /
      DisDnsReg.SRV_LEN;

    if (buffer.readUInt32() !== SrcTypes.DIS) {
      return null;
    }

    /** @type {DnsRegInfo} *//* @ts-ignore */
    const ret = { node: NodeInfo.fromBuffer(buffer), services: [] };
    if (nbServices !== buffer.readUInt32()) {
      return null;
    }
    while (!buffer.fail && (nbServices-- > 0)) {
      const info = ServiceInfo.fromBuffer(buffer);
      if (info) {
        info.node = ret.node;
        ret.services.push(info);
      }
    }
    /* $FlowIgnore: manual typecheck */
    return buffer.fail ? null : ret;
  }
}

DisDnsReg.LEN_MIN = 108;
DisDnsReg.SRV_LEN = 268;
DisDnsReg.TYPE = SrcTypes.DIS;

/**
 * Dns to Dis command
 * @extends DnaHeader
 */
class DisDnsCmd extends DnaHeader {
  /**
   * @param {number} cmd
   * @param {number} info
   * @param {Buffer|number} data
   */
  constructor(cmd, info, data) {
    // @ts-ignore
    super(DisDnsCmd.LEN_MIN + _.size(data));
    data = _.defaultTo(data, 0);
    var isSize = _.isInteger(data);

    var w = new BufferOut(this.data());

    // @ts-ignore
    w.writeUInt32(DisDnsCmd.LEN_MIN + _.size(data));
    w.writeUInt16(cmd);
    w.writeInt16(info || 0);
    if (!_.isEmpty(data)) {
      if (isSize) {
        this.writer = w;
      }
      else {
        /* @ts-ignore */
        w.add(data);
      }
    }
  }

  /**
   * @param {BufferIn|Buffer} buffer
   * @return {?DnsCmdInfo}
   */
  static parse(buffer) {
    buffer = BufferIn.wrap(buffer);

    if (buffer.buffer.length < DisDnsCmd.LEN_MIN) {
      return null;
    }
    let size = buffer.readUInt32();
    if (size < DisDnsCmd.LEN_MIN) {
      return null;
    }
    size -= DisDnsCmd.LEN_MIN;

    const ret = {};
    ret.cmd = buffer.readUInt16();
    ret.info = buffer.readInt16();
    if (ret.cmd === DisDnsCmd.Cmd.DUPLICATE) {
      const data = buffer.read(size);
      ret.data = parseDup(_.toString(data)) || data;
    }
    else {
      ret.data = buffer.read(size);
    }
    return buffer.fail ? null : ret;
  }

  /**
   * @brief create a DUPLICATE packet
   * @param {number} info The command info
   * @param {NodeInfo} node The dup NodeInfo
   * @param {Array<number>} dups List of duplicate serviceIDs
   * @return {?DisDnsCmd} created packet
   */
  static createDup(info, node, dups) {
    if (_.isNil(node)) { return null; }

    /* eslint-disable-next-line max-len */
    var msg = `${node.task}(${node.pid})@${node.name}:${node.port} ${_.size(dups)}`;
    _.forEach(dups, function(d) {
      msg += ' ' + _.padStart(d.toString(16).toUpperCase(), 8, '0');
    });
    return new DisDnsCmd(DisDnsCmd.Cmd.DUPLICATE, info,
      Buffer.from(msg, 'ascii'));
  }
}

DisDnsCmd.LEN_MIN = 8;
DisDnsCmd.Cmd = {
  REGISTER: 0,
  DUPLICATE: 1,
  STOP: 2,
  EXIT: 3,
  SOFT_EXIT: 4
};

module.exports = { DisDnsReg, DisDnsCmd };
