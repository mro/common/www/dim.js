// @ts-check
const
  _ = require('lodash'),
  debug = require('debug')('dim:pkt'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  { DnaHeader } = require('./Dna'),
  { Service } = require('../Consts');

/**
 * @typedef {import('./types').DicReqInfo} DicReqInfo
 * @typedef {import('./types').DicRepInfo} DicRepInfo
 */


const SID_REMOVED = 0x80000000;
const SID_MASK = 0xFFFFFF;

class DicReq extends DnaHeader {
  /**
   * Dic to Dis request
   * @param {string} service  service to request
   * @param {DicReq.Type} type request type
   * @param {{stamped?: boolean, timeout?: number, format?: number, size?: number }?} [opts] options (see details)
   * @param {number} [sid]    request's tracking number
   *
   * @details available options:
   * - {Boolean} stamped whereas we should request a timestamp with updates
   *                     (default: false)
   * - {Integer} timeout timeout value in seconds
   * - {Integer} format requested format
   * - {Integer} size payload size (used by DicCmd packets)
   */
  constructor(service, type, opts, sid) {
    var size = DicReq.LEN_MIN + _.get(opts, 'size', 0);
    super(size);
    this.sid = _.defaultTo(sid, 1);

    var w = new BufferOut(this.data());

    w.writeUInt32(size);
    w.writeString(service, Service.NAME_MAX);
    w.writeUInt32(this.sid);
    w.writeUInt32(type | (_.get(opts, 'stamped', false) ? DicReq.STAMPED : 0));
    const timeout = _.get(opts, 'timeout', 0) / 1000;
    const timeoutRounded = _.ceil(timeout);
    w.writeUInt32(timeoutRounded);
    if (timeout !== timeoutRounded) {
      debug('timeout rounded up: %d secs', timeoutRounded);
    }
    w.writeUInt32(_.get(opts, 'format', Service.FMT_DEFAULT));
    if (size > DicReq.LEN_MIN) {
      this.writer = w;
    }
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @return {?DicReqInfo}
   */
  static parse(buffer) {
    var bufferIn = BufferIn.wrap(buffer);

    if (bufferIn.buffer.length < DicReq.LEN_MIN) {
      return null;
    }
    var size = bufferIn.readUInt32();
    if ((bufferIn.buffer.length < size) || (size < DicReq.LEN_MIN)) {
      return null;
    }

    const ret = {};
    ret.service = bufferIn.readString(Service.NAME_MAX);
    ret.sid = bufferIn.readUInt32();
    ret.type = bufferIn.readUInt32();
    ret.stamped = (ret.type & DicReq.STAMPED) ? true : false;
    ret.type &= DicReq.TYPE_MASK;
    ret.timeout = bufferIn.readUInt32() * 1000; /* milliseconds */
    ret.format = bufferIn.readUInt32();

    if (size > DicReq.LEN_MIN) {
      ret.data = bufferIn.read(size - DicReq.LEN_MIN);
    }
    return bufferIn.fail ? null : ret;
  }
}
DicReq.LEN_MIN = 152;
/** @enum {number} */
DicReq.Type = {
  ONCE_ONLY: 1, TIMED: 2, MONITORED: 4, COMMAND: 8, DELETE: 0x10,
  MONIT_ONLY: 0x20, UPDATE: 0x40, TIMED_ONLY: 0x80, MONIT_FIRST: 0x100
};
DicReq.STAMPED = 0x1000;
DicReq.TYPE_MASK = 0x1FF;
DicReq.TypeStr = _.invert(DicReq.Type);

class DicCmd extends DicReq {
  /**
   * Dic to Dis command
   * @param {string}        service the service to request
   * @param {number|Buffer} data    the command payload
   * @param {{ timeout?: number }} [opts] options (see details)
   * @param {number}        [sid]     request's tracking number
   *
   * @details available options:
   * - {Integer} timeout timeout value
   */
  constructor(service, data, opts, sid) {
    data = _.defaultTo(data, 0);
    var isSize = _.isInteger(data);

    super(service, DicReq.Type.COMMAND,
      // @ts-ignore: checked with isSize
      _.set(opts || {}, 'size', isSize ? data : _.size(data)), sid);
    if (!isSize && !_.isEmpty(data)) {
      /* @ts-ignore: checked */
      this.writer.add(data);
    }
  }
}

/**
 * @param  {BufferIn} buffer
 * @return {boolean}
 */
function testStamped(buffer) {
  return (buffer.buffer.length >= DicRep.LEN_MIN + DicRep.STAMP_LEN) &&
    ((buffer.buffer.readUInt32LE(20) === DicRep.STAMP_LMAGIC) ||
     (buffer.buffer.readUInt16LE(12) === DicRep.STAMP_SMAGIC));
}

class DicRep extends DnaHeader {
  /**
   * Dic reply constructor
   * @param {?(Buffer|number)} [data]      reply's payload, or only its size
   * @param {?number}       [timestamp] timestamp in milliseconds (optional)
   * @param {?number}        [quality]   reply's quality (ignored if no timestamp)
   * @param {?number}        [sid]       serviceID link to the request
   *
   * @details timestamp and quality are optional and can be replaced directly by
   * sid, having the following signature: constructor(data, sid)
   */
  constructor(data, timestamp, quality, sid) {
    data = _.defaultTo(data, 0);
    var isSize = _.isInteger(data);
    if (_.isNil(sid) && _.isNil(quality)) {
      /* @ts-ignore: args reorg */
      sid = timestamp;
      timestamp = null;
    }
    /* @ts-ignore: checked by isSize  */
    var size = DicRep.LEN_MIN + (_.isNil(timestamp) ? 0 : DicRep.STAMP_LEN) +
      /* @ts-ignore: checked by isSize  */
      (isSize ? data : _.size(data));
    super(size);
    this.sid = _.defaultTo(sid, 1);

    var w = new BufferOut(this.data());
    w.writeUInt32(size);
    w.writeUInt32(this.sid);
    if (!_.isNil(timestamp)) {
      /* $FlowIgnore */
      w.writeUInt16(timestamp % 1000);
      w.writeUInt16(DicRep.STAMP_SMAGIC);
      /* $FlowIgnore */
      w.writeUInt32(Math.floor(timestamp / 1000));
      w.writeUInt32(_.defaultTo(quality, 0));
      w.writeUInt32(DicRep.STAMP_LMAGIC);
      w.writeUInt32(0);
      w.writeUInt32(0);
    }
    if (isSize) {
      this.writer = w;
    }
    else if (!_.isEmpty(data)) {
      /* @ts-ignore: is a Buffer */
      w.add(data);
    }
  }

  /**
   * @param  {number} sid
   * @return {DicRep}
   */
  static createRemoval(sid) {
    return new DicRep(null, null, null, (sid | SID_REMOVED) >>> 0);
  }

  /**
   * parse from buffer
   * @param {Buffer|BufferIn} buffer  buffer to parse
   * @param {?boolean}        [stamped] whereas packet has a timestamp, set to null for auto-detection
   * @return {?DicRepInfo}
   */
  /* eslint-disable-next-line complexity */
  static parse(buffer, stamped) {
    buffer = BufferIn.wrap(buffer);

    stamped = _.isNil(stamped) ? testStamped(buffer) : stamped;
    if (buffer.buffer.length < DicRep.LEN_MIN) {
      return null;
    }
    var size = buffer.readUInt32();
    if ((buffer.buffer.length < size) || (size < DicRep.LEN_MIN)) {
      return null;
    }
    /** @type DicRepInfo */
    const ret = {};
    ret.sid = buffer.readUInt32();
    if (ret.sid & SID_REMOVED) {
      ret.removed = true;
      ret.sid &= SID_MASK;
    }

    if (stamped) {
      if (size < DicRep.LEN_MIN + DicRep.STAMP_LEN) {
        return null;
      }
      ret.timestamp = buffer.readUInt32() & 0x0000FFFF;
      ret.timestamp += buffer.readUInt32() * 1000;
      ret.quality = buffer.readUInt32();
      buffer.fwd(12);
      /* @ts-ignore will be checked in result */
      ret.data = buffer.read(size - (DicRep.LEN_MIN + DicRep.STAMP_LEN));
    }
    else {
      /* @ts-ignore will be checked in result */
      ret.data = buffer.read(size - DicRep.LEN_MIN);
    }
    return buffer.fail ? null : ret;
  }
}

DicRep.LEN_MIN = 8;
DicRep.STAMP_LEN = 24;
DicRep.STAMP_LMAGIC = 0xc0dec0de;
DicRep.STAMP_SMAGIC = 0xc0de;

module.exports = { DicReq, DicRep, DicCmd };
