// @ts-check
const
  DicDis = require('./DicDis'),
  DicDns = require('./DicDns'),
  DisDns = require('./DisDns'),
  Dna = require('./Dna');

module.exports = { ...DicDis, ...DicDns, ...DisDns, ...Dna };
