// @ts-check
const
  _ = require('lodash'),
  debug = require('../utils/debug-ext')('dim:dna'),
  VarBuffer = require('../utils/VarBuffer'),
  { BufferOut, BufferIn } = require('../utils/IOBuffer'),
  utils = require('../utils'),
  NodeInfo = require('../NodeInfo');

/**
 * @typedef {import('./types').DnaNetInfo} DnaNetInfo
 */

class DnaHeader {
  /**
   * @param {?number=} dataSize
   * @param {?number=} magic
   */
  constructor(dataSize, magic) {
    if (dataSize) {
      /** @type {Buffer} */
      this.raw = Buffer.allocUnsafe(DnaHeader.LEN + dataSize);
      this.raw.writeUInt32LE(DnaHeader.LEN, 0);
      this.raw.writeUInt32LE(dataSize, 4);
      this.raw.writeUInt32LE(_.defaultTo(magic, DnaHeader.MAGIC), 8);
    }
  }

  isValid() { return this.raw !== undefined; }

  data() { return this.raw.slice(DnaHeader.LEN); }

  /**
   * @param {Buffer} buffer
   * @return {?{ dataSize:number, magic:number }}
   */
  static parseBuffer(buffer) {
    if (_.get(buffer, 'length', 0) < DnaHeader.LEN) {
      debug("buffer too small");
    }
    else if (buffer.readUInt32LE(0) !== DnaHeader.LEN) {
      debug("invalid LEN: %d", buffer.readUInt32LE(0));
    }
    else {
      var dataSize = buffer.readUInt32LE(4);
      var magic = buffer.readUInt32LE(8);
      return { dataSize, magic };
    }
    return null;
  }

  /**
   * @param {Buffer} buffer
   * @return ?DnaHeader
   */
  static fromBuffer(buffer) {
    if (DnaHeader.parseBuffer(buffer)) {
      const hdr = new DnaHeader();
      hdr.raw = buffer;
      return hdr;
    }
    return null;
  }
}

DnaHeader.LEN = 12;
DnaHeader.MAGIC = 0xFEADFEAD;
DnaHeader.TST_MAGIC = 0x11131517;

class DnaNet extends DnaHeader {
  /**
   * @param {?string=} node
   * @param {?string=} task
   */
  constructor(node, task) {
    super(DnaNet.LEN);
    var w = new BufferOut(this.data());

    w.writeUInt32(DnaNet.MAGIC);
    w.writeString(node || utils.defaultNodeName(), NodeInfo.NAME_MAX);
    w.writeString(task || utils.defaultTaskName(), NodeInfo.TASK_MAX);
    w.writeUInt32(0);
  }

  /**
   * @param {Buffer|BufferIn} buffer
   * @return {?DnaNetInfo}
   */
  static parse(buffer) {
    buffer = BufferIn.wrap(buffer);

    if (buffer.buffer.length < DnaNet.LEN ||
        buffer.readUInt32() !== DnaNet.MAGIC) {
      return null;
    }
    else {
      const name = buffer.readString(NodeInfo.NAME_MAX);
      const task = buffer.readString(NodeInfo.TASK_MAX);
      return buffer.fail ? null : { name, task };
    }
  }
}
DnaNet.LEN = 84;
DnaNet.MAGIC = 0xC0DEC0DE;

class DnaStream {
  /**
   * @param {(data: Buffer, magic: number) => any} cb
   * @param {?number=} timeout
   */
  constructor(cb, timeout) {
    this.cb = cb;
    this._buffer = new VarBuffer();
    this._remaining = 0;
    this._magic = null;
    this.timeout = _.defaultTo(timeout, DnaStream.TIMEOUT);
  }

  _timeout() {
    debug('clearing rcv buffer');
    this._buffer.reset();
    this._remaining = 0;
    this._magic = null;
  }

  /**
   * @param {number} delay
   */
  setTimeout(delay) {
    this.clearTimeout();
    this._rcvToId = setTimeout(this._timeout.bind(this), delay);
  }

  clearTimeout() {
    if (this._rcvToId) {
      clearTimeout(this._rcvToId);
      this._rcvToId = null;
    }
  }

  /**
   * @param  {Buffer} data
   * @return {?Buffer}
   */
  _pktHead(data) {
    var hdr = DnaHeader.parseBuffer(data);
    if (!hdr) {
      debug('invalid data received');
    }
    else if (hdr.magic === DnaHeader.TST_MAGIC) {
      debug('tst packet');
      return data.slice(DnaHeader.LEN);
    }
    else {
      this._remaining = hdr.dataSize;
      this._magic = hdr.magic;
      debug('packet len:%i, magic:%x', hdr.dataSize, hdr.magic);
      return data.slice(DnaHeader.LEN);
    }
    return null;
  }

  /**
   * @param  {Buffer} data
   * @return {?Buffer}
   */
  _findPkt(data) {
    if (this._buffer.length !== 0) {
      this._buffer.add(data);
      data = this._buffer.buffer();
    }
    if (data.length >= DnaHeader.LEN) {
      this._buffer.reset();
      const pkt = this._pktHead(data);
      return pkt ? pkt : data.slice(1);
    }
    else {
      if (this._buffer.length === 0) {
        this._buffer.add(data);
      }
      return null;
    }
  }

  /**
   * @param  {?Buffer} data
   */
  add(data) {
    while (data && data.length > 0) {
      if (this._remaining === 0) {
        data = this._findPkt(data);
      }
      else if (data.length >= this._remaining) {
        this._buffer.add(data.slice(0, this._remaining));
        data = data.slice(this._remaining);
        this._remaining = 0;
        this.cb(this._buffer.buffer(), this._magic || -1);
        this._buffer.reset();
      }
      else {
        this._buffer.add(data);
        this._remaining -= data.length;
        data = null;
      }
    }
    if (this._buffer.length !== 0) {
      this.setTimeout(this.timeout);
    }
    else {
      this.clearTimeout();
    }
  }
}
DnaStream.TIMEOUT = 1000;

module.exports = { DnaHeader, DnaStream, DnaNet };
