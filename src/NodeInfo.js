// @ts-check

const
  _ = require('lodash'),
  utils = require('./utils'),
  { BufferOut, BufferIn } = require('./utils/IOBuffer'),
  { Service } = require('./Consts');

const NodeInfoDefaults = {
  host: 0,
  port: 0,
  name: '',
  task: '',
  pid: 0,
  protocol: 1,
  format: Service.FMT_DEFAULT
};

class NodeInfo {
  /**
  * @param {Partial<NodeInfoDefaults>} [info]
  */
  constructor(info /** ? {} */) {
    this.host = _.get(info, 'host', NodeInfoDefaults.host);
    this.port = _.get(info, 'port', NodeInfoDefaults.port);
    this.name = _.get(info, 'name', NodeInfoDefaults.name);
    this.task = _.get(info, 'task', NodeInfoDefaults.task);
    /* TASK_MAX includes trailing \0 */
    this.task = this.task.slice(0, NodeInfo.TASK_MAX - 1);
    this.pid = _.get(info, 'pid', NodeInfoDefaults.pid);
    this.protocol = _.get(info, 'protocol', NodeInfoDefaults.protocol);
    this.format = _.get(info, 'format', NodeInfoDefaults.format);
  }

  /**
   * @return {string}
   */
  url() {
    if (!this._url) {
      this._url = 'tcp://' + this.name + ':' + this.port;
    }
    return this._url;
  }

  /* eslint-disable-next-line max-len */
  /**
   * @param  {string | number} host
   * @param  {?number=} [port]
   * @param  {?string=} [task]
   * @return {NodeInfo}
   */
  static local(host, port, task) {
    var node = new NodeInfo();
    node.host = utils.toIntAddr(host) || 0;
    node.port = port || 0;
    // $FlowFixMe: lodash isString not supported
    node.name = (_.isString(host) && !utils.isAnyAddress(host)) ?
      /* $FlowIgnore: checked above that host is actually a string */
      host : utils.defaultNodeName();
    node.task = task || utils.defaultTaskName();
    /* TASK_MAX includes trailing \0 */
    node.task = node.task.slice(0, NodeInfo.TASK_MAX - 1);
    node.pid = utils.pid();
    node.protocol = 1;
    node.format = Service.FMT_DEFAULT;
    return node;
  }

  /**
   * @param {string|number} host
   * @param {number} [port]
   */
  setAddress(host, port) {
    this.host = utils.toIntAddr(host) || 0;
    this.port = port || this.port;
  }

  isValid() {
    return (this.port && !_.isEmpty(this.name)) ?
      true : false;
  }

  /**
   * @param  {BufferOut|Buffer} buffer
   * @return {boolean}
   */
  toBuffer(buffer) {
    buffer = BufferOut.wrap(buffer);

    buffer.writeString(this.name, NodeInfo.NAME_MAX);
    buffer.writeString(this.task, NodeInfo.TASK_MAX);
    buffer.writeUInt32(utils.toIntAddr(this.host) || 0, true);
    buffer.writeUInt32(this.pid);
    buffer.writeUInt32(this.port);
    buffer.writeUInt32(this.protocol);
    buffer.writeUInt32(this.format);
    return !buffer.fail;
  }

  /**
   * @param  {BufferIn|Buffer} buffer
   * @return {? NodeInfo}
   */
  static fromBuffer(buffer) {
    var node = new NodeInfo();

    buffer = BufferIn.wrap(buffer);
    node.name = buffer.readString(NodeInfo.NAME_MAX);
    if (node.name.charCodeAt(0) === 0xFF) {
      node.name = '';
    }
    node.task = buffer.readString(NodeInfo.TASK_MAX);
    node.host = buffer.readUInt32(true);
    node.pid = buffer.readUInt32();
    node.port = buffer.readUInt32();
    node.protocol = buffer.readUInt32();
    node.format = buffer.readUInt32();
    return buffer.fail ? null : node;
  }

  /**
   * @param {string} u
   * @return {? { name: any, port: any }}
   */
  static parseUrl(u) {
    var parsed = utils.parseUrl(u);
    if (parsed) {
      return { name: parsed.host, port: parsed.port };
    }
    return null;
  }

  /**
   * @param  {NodeInfo|string|any} object mixed input
   * @return {NodeInfo} the wrapped node
   */
  static wrap(object) {
    if (object instanceof NodeInfo) {
      return object;
    }
    else if (_.isString(object)) {
      var u = NodeInfo.parseUrl(object);
      if (u) {
        return new NodeInfo(u);
      }
    }
    /* @ts-ignore */
    return new NodeInfo(object);
  }
}

NodeInfo.NAME_MAX = 40;
NodeInfo.TASK_MAX = 36;

module.exports = NodeInfo;
