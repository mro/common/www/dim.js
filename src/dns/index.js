// @ts-check
const
  DnsClient = require('./DnsClient'),
  DnsServer = require('./DnsServer');

module.exports = { DnsClient, DnsServer };
