// @ts-check

const
  { EventEmitter } = require('events'),
  _ = require('lodash'),
  debug = require('debug')('dim:dns:server'),

  { Dns, SrcTypes, DEFAULT_LOCALHOST } = require('../Consts'),
  { DicDnsReq, DicDnsRep } = require('../packets/DicDns'),
  { DisDnsReg, DisDnsCmd } = require('../packets/DisDns'),
  { DnaNet } = require('../packets/Dna'),
  { TCPServer } = require('../Conn'),
  DisNode = require('../dis/DisNode'),
  NodeInfo = require('../NodeInfo'),
  { ServerList, killServers, serviceInfo } = require('./DnsServer_builtin');

/**
 * @typedef {import('./types').DnsConn} DnsConn
 * @typedef {import('./types').DnsServiceRecord} DnsServiceRecord
 * @typedef {{service:string, sid:number}} DnsReqInfo
 *
 * @typedef {import('../ServiceInfo')} ServiceInfo
 * @typedef {import('../packets/types').DnaNetInfo} DnaNetInfo
 * @typedef {import('../packets/types').DnsRegInfo} DnsRegInfo
 */

/**
 * @details
 * This class adds the following information on connection items:
 * - net: The contents of incoming DnaNet packets
 * - subscribed: an sid -> serviceName subscription map
 * - registered: an sid -> serviceName definition map
 */
class DnsServer extends EventEmitter {
  /**
   * @param {?string=} host
   * @param {?number=} port
   */
  constructor(host, port) {
    super();

    _.bindAll(this, [ '_onConnect' ]);
    this._conn = new TCPServer(host || DEFAULT_LOCALHOST,
      _.defaultTo(port, Dns.PORT_DEFAULT));
    this._conn.on('connect', this._onConnect);
    this.cid = 1;

    /** @type {{ [cid: number]: DnsConn }} */
    this._connMap = {};
    /** @type {{ [serviceName: string]: DnsServiceRecord }} */
    this._srvMap = {};

    this.dis = new DisNode(null, null, NodeInfo.local(this._conn.host, null,
      'DIS_DNS'));
    this.dis.removeService('DIS_DNS/VERSION_NUMBER');
    this.dis.removeService('DIS_DNS/SERVICE_LIST');
    this.dis.removeService('DIS_DNS/CLIENT_LIST');
    this.dis.removeService('DIS_DNS/SET_EXIT_HANDLER');
    this.dis.removeService('DIS_DNS/EXIT');

    this.serverList = new ServerList();
    this.dis.addService('DIS_DNS/SERVER_LIST', this.serverList);
    // Not implementing deprecated SERVER_INFO
    this.dis.addCmd('DIS_DNS/KILL_SERVERS', 'I|CMD',
      _.partial(killServers, this));
    this.dis.addRpc('DIS_DNS/SERVICE_INFO', 'C,C|RPC',
      _.partial(serviceInfo, this));
  }

  url() {
    if (_.isNil(this._url) && this._conn.listening) {
      this._url = 'tcp://' + this._conn.host + ':' + this._conn.port;
    }
    return this._url || '';
  }

  /**
   * @param {string} service
   * @return {?ServiceInfo}
   */
  serviceInfo(service) {
    return _.get(this._srvMap, [ service, 'info' ], null);
  }

  listen() {
    if (!this._listen) {
      this._listen = this.dis.listen()
      .then(() => {
        _.forEach(this.dis.serviceInfo(), (info) => {
          this._updateService(info, this.dis.info);
        });
        return this._conn.listen();
      })
      .then(() => {
        this._conn.once('close', this.emit.bind(this, 'close'));
        return this;
      });
    }
    return this._listen;
  }

  close() {
    _.unset(this, '_url');
    if (this._listen) {
      this._conn.removeListener('connect', this._onConnect);
      this._conn.close();
      this.dis.close();
      _.forEach(this._connMap, (conn) => conn.close());
      this._listen = null;
    }
  }

  /**
   * @param {DnsConn} conn
   */
  _onConnect(conn) {
    var cid = conn.cid = this.cid++;
    debug('connected: cid:%d', cid);
    this._connMap[cid] = conn;
    conn.once('close', this._removeConn.bind(this, cid));
    conn.on('packet', (pkt) => this._onPacket(pkt, conn));
  }

  /**
   * @param  {Buffer} pkt
   * @param  {DnsConn} conn
   */
  _onPacket(pkt, conn) {
    if (!_.has(conn, 'net') && this._onNetPacket(pkt, conn)) { return; }

    if (_.size(pkt) < 8) {
      debug('invalid packet:', pkt);
      return;
    }
    var srcType = pkt.readUInt32LE(4);
    switch (srcType) {
    case SrcTypes.DIC:
      this._onDicPacket(DicDnsReq.parse(pkt), conn); break;
    case SrcTypes.DIS:
      this._onDisPacket(DisDnsReg.parse(pkt), conn); break;
    default:
      debug('unknown pkt.srcType:', srcType); break;
    }
  }

  /**
   * @param  {Buffer} pkt
   * @param  {DnsConn} conn
   * @return {?DnaNetInfo}
   */
  _onNetPacket(pkt, conn) {
    var net = DnaNet.parse(pkt);
    if (!net) { return null; }

    debug('net packet: name:%s task:%s', net.name, net.task);
    conn.net = net;
    return net;
  }

  /**
   * @param {?DnsReqInfo} pkt
   * @param {DnsConn} conn
   */
  _onDicPacket(pkt, conn) {
    if (!pkt) { debug('invalid DIC packet'); return; }

    _.set(conn, [ 'subscribed', pkt.sid ], pkt.service);
    var service = this._srvMap[pkt.service];
    if (_.isNil(service)) {
      debug('subscribe unknown service:', pkt.service);
      service = this._srvMap[pkt.service] = {};
    }

    debug('adding service listener cid:%d service:%s', conn.cid, pkt.service);
    var clientLink = { cid: conn.cid, sid: pkt.sid };
    if (!_.has(service, 'clients')) {
      service.clients = [ clientLink ];
    }
    else if (!_.some(service.clients, clientLink)) {
      /* @ts-ignore: doesn't understand _.has */
      service.clients.push(clientLink);
    }
    else {
      debug('client already registered');
    }
    conn.send(new DicDnsRep( // @ts-ignore
      _.get(service.info, 'definition'),
      _.get(service.info, 'node'),
      pkt.sid));
  }

  /**
   * @param {?DnsRegInfo}  pkt
   * @param {DnsConn} conn
   */
  _onDisPacket(pkt, conn) {
    if (!pkt) { debug('invalid DIS packet'); return; }

    /* FIXME: check node for name collision */
    /** @type {number[]=} */
    var dups;
    /** @type {?NodeInfo=} */
    var dupNode;
    var dupTask = false;
    debug('service update cid:%d', conn.cid);
    conn.registered = conn.registered || {};
    _.forEach(pkt.services, (service) => {
      var dup = this._getServiceDup(service, pkt.node);
      if (dupTask) {
        if (!_.isNil(dup)) {
          dups = dups || [];
          // @ts-ignore
          dups.push(service.sid);
        }
      }
      else if (!_.isNil(dup)) {
        /* record only the first one */
        dupNode = _.defaultTo(dupNode, dup.node);

        dups = _.defaultTo(dups, []);
        // @ts-ignore
        dups.push(service.sid);
        if (service.name === (pkt.node.task + '/VERSION_NUMBER')) {
          dupTask = true;
        }
      }
      else if (service.removed) {
        this._removeService(service, pkt.node);
      }
      else {
        this._updateService(service, pkt.node);
        // @ts-ignore
        _.set(conn, [ 'registered', service.sid ], service);
      }
    });

    if (dupTask) {
      debug('duplicate task name on', pkt.node.name);
      // @ts-ignore: there are duplicates
      conn.send(DisDnsCmd.createDup(1, dupNode, dups));
      conn.close();
    }
    else {
      /* refresh conn with DisDns packet info */
      _.set(conn, [ 'net', 'name' ], pkt.node.name);
      _.set(conn, [ 'net', 'task' ], pkt.node.task);
      _.set(conn, [ 'net', 'server' ], true);
      this.serverList.add(pkt.node);

      if (!_.isEmpty(dups)) {
        debug('duplicate services on', pkt.node.name);
        // @ts-ignore: there are duplicates
        conn.send(DisDnsCmd.createDup(0, dupNode, dups));
      }
    }
  }

  /**
   * @brief check that a service is owned by the given node
   * @param  {ServiceInfo} info the service to check
   * @param  {NodeInfo}    node the node to ensure this service belongs to
   * @return {?ServiceInfo} duplicate service info or null if there's no dup
   */
  _getServiceDup(info, node) {
    /* @ts-ignore */
    var srvInfo = _.get(this._srvMap, [ info.name ]);

    return (!srvInfo || !srvInfo.info || srvInfo.info.removed ||
      (srvInfo.info.sid === info.sid && _.isEqual(srvInfo.info.node, node))) ?
      null : srvInfo.info;
  }

  /**
   * @param {ServiceInfo} info
   * @param {?NodeInfo=} node
   * @return {boolean}
   */
  _removeService(info, node) {
    /* @ts-ignore: only complete ServiceInfo are sent to DNS */
    var srvInfo = _.get(this._srvMap, [ info.name ]);
    if (!_.isNil(srvInfo) && srvInfo.info && !srvInfo.info.removed) {
      if (srvInfo.info.sid !== info.sid ||
          !_.isEqual(srvInfo.info.node, node)) {
        /* service owned by another node */
        return false;
      }

      debug('service removed:', info.name);
      srvInfo.info = info;
      info.node = null;
      info.removed = true;
      info.sid = null;

      this._sendUpdate(srvInfo);
    }
    return true;
  }

  /**
   * @param {ServiceInfo} info
   * @param {NodeInfo} node
   */
  _updateService(info, node) {
    /* @ts-ignore: only complete ServiceInfo are sent to DNS */
    var srvInfo = _.get(this._srvMap, [ info.name ]);
    var sendUpdate = true;

    if (_.isNil(srvInfo)) {
      debug('new service registered:', info.name);
      srvInfo = { info };
      /* @ts-ignore: only complete ServiceInfo are sent to DNS */
      _.set(this._srvMap, [ info.name ], srvInfo);
    }
    else if (_.get(srvInfo.info, 'removed')) {
      debug('service back online:', info.name);
    }
    else if (!_.isEqual(_.get(srvInfo.info, 'definition'), info.definition)) {
      debug('service updated:', info.name);
    }
    else {
      sendUpdate = false;
    }

    info.node = node;
    srvInfo.info = info;
    if (sendUpdate) {
      this._sendUpdate(srvInfo);
    }
  }

  /**
   * @param {DnsServiceRecord} srvInfo
   */
  _sendUpdate(srvInfo) {
    var info = srvInfo.info;
    if (!info) { return; }

    if (!_.isEmpty(srvInfo.clients)) {
      debug('sending service update:', info.name);
    }
    this.emit('update', info);

    _.forEach(srvInfo.clients, (cli) => {
      var c = this._connMap[cli.cid];
      if (_.isNil(c)) {
        debug('unknown client cid:', cli.cid);
      }
      else {
        debug('sending update to cid:%d', cli.cid);
        /* @ts-ignore: we're sure that deifnition and node are not null */
        c.send(new DicDnsRep(info.definition, info.node, cli.sid));
      }
    });
  }

  /**
   * @param {number} cid
   */
  _removeConn(cid) {
    var conn = this._connMap[cid];
    if (_.isNil(conn)) { debug('unknown connection cid:', cid); return; }

    debug('removing connection cid:%d net:%o', cid, conn.net);
    conn.removeAllListeners();
    /* DIS connection */
    _.forEach(_.get(conn, 'registered'), (srvInfo) => {
      this._removeService(srvInfo, srvInfo.node);
    });

    /* DIC connection */
    if (_.has(conn, 'subscribed')) {
      var crit = { cid };
      _.forEach(conn.subscribed, (name) => {
        // @ts-ignore
        _.remove(_.get(this._srvMap[name], 'clients'), crit);
      });
    }

    if (conn.net && conn.net.server) {
      this.serverList.remove(conn.net);
    }
    delete this._connMap[cid];
  }
}

module.exports = DnsServer;
