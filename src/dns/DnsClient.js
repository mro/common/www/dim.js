/* eslint-disable max-lines */
// @ts-check
const
  { EventEmitter } = require('events'),
  q = require('q'),
  _ = require('../utils/lodash-ext'),
  debug = require('debug')('dim:dns:client'),

  { Dns } = require('../Consts'),
  { DicDnsReq, DicDnsRep } = require('../packets/DicDns'),
  { DisDnsReg, DisDnsCmd } = require('../packets/DisDns'),
  ServiceInfo = require('../ServiceInfo'),
  NodeInfo = require('../NodeInfo'),
  { TCPConn } = require('../Conn'),
  Errors = require('../Errors');

/**
 * @typedef {import('../dis/DisNode')} DisNode
 * @typedef {string} ServiceName
 * @typedef {number} ServiceId
 */

class DnsClient extends EventEmitter {
  /**
   * DnsClient constructor
   * @param {string} host
   * @param {?number=} port
   * @param {?{ retryDelay?: number }=} opts
   *
   * @details valid options:
   * - retryDelay connections retry delay, defaults to DnsClient.RETRY_DELAY,
   *     set to 0 to disable.
   *
   * @details emits:
   * - connect: when server is connected
   * - close: when connection is closed
   */
  constructor(host, port, opts) {
    super();
    _.bindAll(this, [ '_onPacket', '_onClose' ]);
    this.opts = opts;
    this._conn = new TCPConn(host, _.defaultTo(port, Dns.PORT_DEFAULT));
    this.sid = 1;

    this._conn.on('packet', this._onPacket);
    /** @type {{[serviceName: string]: (ServiceInfo|{sid: ServiceId})}} */
    this._srvMap = {};
    /** @type {{[serviceId: number]: ServiceName}} */
    this._sidMap = {};
    /** @type {{[serviceName: string]: Array<q.Deferred<?ServiceInfo>>}} */
    this._defMap = {};
    /** @type {?{ disNode: DisNode,
      up: (serviceInfo: ?ServiceInfo) => any,
      close: (disNode: ?DisNode) => any
    }} */
    this._disInfo = null; /* hooked disNode */

    this._ref = 1;
  }

  ref() {
    if (this._ref++ <= 0) {
      debug('refcounting a dead object');
    }
  }

  unref() {
    if (--this._ref === 0) { this.close(); }
  }

  /**
   * @param  {?(DnsClient|string)=} dns
   * @return {DnsClient|null}
   */
  static wrap(dns) {
    if (dns instanceof DnsClient) {
      dns.ref();
      return dns;
    }
    else if (_.isString(dns)) {
      /* $FlowIgnore */
      var u = NodeInfo.parseUrl(dns);
      if (u && u.name) {
        return new DnsClient(u.name, u.port);
      }
    }
    // $FlowIgnore
    return null;
  }

  connect() {
    // $FlowIgnore: _connProm is a q$Promise
    if (!this._connProm || this._connProm.isRejected()) {
      this._clearTimer();
      this._conn.once('close', this._onClose);
      this._connProm = this._conn.connect()
      .then(() => {
        _.forEach(this._sidMap, // @ts-ignore
          (service, sid) => this._conn.send(
            new DicDnsReq(service, _.toNumber(sid))));
        if (!_.isNil(this._disInfo)) {
          /* $FlowIgnore */
          this.register(this._disInfo.disNode).catch(_.noop);
        }
        debug('connected');
        this.emit('connect');
      });
    }
    this._connProm.catch(_.noop);
    return this._connProm;
  }

  /**
   * @brief query a service
   * @param {ServiceName} service the service to request
   * @return {Promise<ServiceInfo>} a promise resolving a ServiceInfo
   */
  async query(service) {
    var srv = this._srvMap[service];
    if (srv instanceof ServiceInfo) {
      return srv;
    }

    /* register deferred early to handle interruptions */
    var defer = q.defer();
    /* prevent Uncaught Errors since this promise is not in a chain (yet) */
    defer.promise.catch(_.noop);
    this._defMap[service] = _.push(this._defMap[service], defer);

    if (_.isNil(srv)) { /* not requested yet */
      const sid = this.sid++;
      this._srvMap[service] = { sid };
      this._sidMap[sid] = service;

      try {
        await this.connect();
        if (this._conn.connected) {
          debug('querying service:', service);
          const req = new DicDnsReq(service, sid);
          this._conn.send(req);
        }
      }
      catch (err) {
        if (defer.promise.isPending()) {
          defer.reject(err);
        }
      }
    }
    return await defer.promise;
  }

  /**
   * @brief re-query a service
   * @param  {ServiceName}  service
   * @details Legacy DIM DNS (C implementation) drops all subscription on
   * service disconnect, this is used to repeat the request (if dns connection
   * is still alive)
   */
  refreshQuery(service) {
    var srv = this._srvMap[service];
    if (!_.isNil(srv) && _.isNumber(srv.sid) && this._conn.connected) {
      debug('refreshing service query:', service);
      const req = new DicDnsReq(service, srv.sid);
      this._conn.send(req);
    }
  }

  /**
   * @brief return available information about a service
   * @param {string} service the service to request
   * @return {?ServiceInfo}
   */
  getServiceInfo(service) {
    var srv = this._srvMap[service];
    if (srv instanceof ServiceInfo) {
      return srv;
    }
    return null;
  }

  /**
   * @brief register services on a DisNode
   * @param {DisNode} disNode DisNode to register
   * @return {Q.Promise<any>} a registering promise
   *
   * @details also listen for updates
   */
  register(disNode) {
    if (!this._disInfo) {
      var info = { disNode,
        up: this._update.bind(this, disNode),
        close: () => this.unregister(disNode)
      };
      this._disInfo = info;
      disNode.once('close', info.close);
      this.ref();
    }
    else if (this._disInfo.disNode !== disNode) {
      /* eslint-disable-next-line max-len */
      var err = new Errors.Busy('only one disNode can be registered per connection');
      debug(err.message);
      return q.reject(err);
    }

    return q()
    .then(disNode.listen.bind(disNode))
    // @ts-ignore
    .then(() => {
      if (this._conn.connected) {
        // @ts-ignore set a couple lines earlier
        disNode.on('update', this._disInfo.up);
        return this._conn.send(
          new DisDnsReg(disNode.info, disNode.serviceInfo()));
      }
      else {
        return this.connect();
      }
    });
  }

  /**
   * @param {?DisNode} disNode
   */
  unregister(disNode) {
    if (_.isNil(disNode) || _.isNil(this._disInfo)) {
      debug('invalid unregister arguments');
    } /* $FlowIgnore */
    else if (disNode !== this._disInfo.disNode) {
      debug('can only unregister the registered DisNode');
    }
    else {
      debug('unregistering node');
      /* $FlowFixMe: flow doesn't understand isNil */
      disNode.removeListener('update', this._disInfo.up);
      /* $FlowFixMe: flow doesn't understand isNil */
      disNode.removeListener('close', this._disInfo.close);
      this._disInfo = null;

      if (this._conn.connected) {
        /* $FlowFixMe: flow doesn't understand isNil */
        var services = _.map(disNode.serviceInfo(), (s) => {
          if (!s.removed) {
            s = _.create(ServiceInfo.prototype, s);
            s.removed = true;
          }
          return s;
        });
        /* $FlowFixMe: flow doesn't understand isNil */
        this._conn.send(new DisDnsReg(disNode.info, services));
      }
      this.unref();
    }
  }

  close() {
    debug('closing client');
    this._clearTimer();
    if (this._connProm) {
      this._connProm = null;
      this._conn.removeAllListeners('close');
      this._conn.removeListener('packet', this._onPacket);
      this._conn.close();
      this.emit('close');

      var map = this._defMap;
      this._defMap = {};
      _.forEach(map, function(defs) {
        _.forEach(defs, function(def) {
          def.reject(new Errors.Interrupted('dns closed'));
        });
      });
      if (this._disInfo) {
        this.unregister(this._disInfo.disNode);
      }
    }
  }

  /**
   * @param  {Buffer} pkt
   */
  _onPacket(pkt) {
    if (!_.isEmpty(this._sidMap) && this._onDicPacket(pkt)) {
      /* handled dic packet */
    }
    else if (!_.isNil(this._disInfo) && this._onDisPacket(pkt)) {
      /* handled dis packet */
    }
    else {
      debug('unknown packet received', pkt);
    }
  }

  /**
   * @param  {Buffer} pkt
   * @return {boolean}
   */
  _onDisPacket(pkt) {
    var cmd = DisDnsCmd.parse(pkt);
    var info = this._disInfo;
    if (!cmd || !info) { return false; }

    var node = info.disNode;
    switch (cmd.cmd) {
    case DisDnsCmd.Cmd.DUPLICATE:
      if (cmd.info === 0) {
        const services = _.map(_.get(cmd.data, 'services'),
          (s) => _.get(node.getServiceBySid(s), 'info.name', s));
        debug('duplicate services', services);
        node.emit('duplicate:services', services,
          _.omit(cmd.data, 'services'));
      }
      else {
        const dup = _.omit(cmd.data, 'services');
        debug('duplicate:taskName', dup);
        node.emit('duplicate:taskName', dup);
        this.close();
      }
      break;
    case DisDnsCmd.Cmd.STOP:
      debug('stop request');
      this.close();
      node.emit('exit', -1);
      break;
    case DisDnsCmd.Cmd.EXIT:
      debug('exit request');
      this.close();
      node.emit('exit', -1);
      break;
    case DisDnsCmd.Cmd.SOFT_EXIT:
      debug('soft exit request', cmd.info);
      this.close();
      node.emit('exit', cmd.info);
      break;
    default:
      break;
    }
    return true;
  }

  /**
   * @param  {Buffer} pkt
   * @return {boolean}
   */
  _onDicPacket(pkt) {
    var info = DicDnsRep.parse(pkt);
    if (!info) { return false; }

    // @ts-ignore
    var name = _.get(this._sidMap, info.sid);
    if (_.isNil(name)) {
      debug('unknown sid: sid:%d info:%o', info.sid, info);
      return false;
    }
    info.name = name;
    var old = this._srvMap[name];
    if (_.isEqual(old, info)) {
      debug('serviceInfo not modified:', name);
    }
    else {
      this._srvMap[name] = info;
      debug('serviceInfo updated:', name);
      this.emit('service:' + name, info);
      this.emit('service', name, info);
      var defs = this._defMap[name];
      if (defs) {
        _.unset(this._defMap, name);
        _.forEach(defs, function(def) { def.resolve(info); });
      }
    }
    return true;
  }

  _clearTimer() {
    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
  }

  _onClose() {
    var retry = _.get(this.opts, 'retryDelay', DnsClient.CONN_RETRY);

    if (retry) {
      this._clearTimer();
      /* retry to connect after a previous successful connection */
      if (_.invoke(this._connProm, 'isFulfilled')) {
        this._connProm = null;
      }
      this._timer = setTimeout(this.connect.bind(this), retry);
      debug('disconnected');
      this.emit('disconnect');
    }
    else {
      this.close();
    }
  }

  /**
   * @param  {DisNode} disNode
   * @param {?ServiceInfo=} info
   */
  _update(disNode, info) {
    if (this._conn.connected && disNode.info.isValid()) {
      var msg = new DisDnsReg(disNode.info, info || disNode.serviceInfo());
      this._conn.send(msg);
    }
  }
}

DnsClient.CONN_RETRY = 5000;

module.exports = DnsClient;
