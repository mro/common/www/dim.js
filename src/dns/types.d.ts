
export as namespace dimDns;

import ServiceInfo from '../ServiceInfo';
import { TCPConn } from '../Conn';

export type ConnId = number
export type ServiceId = number
export type ServiceName = string

export type DnsServiceRecord = {
  info?: ServiceInfo,
  clients?: Array<{ cid: ConnId, sid: ServiceId }>
}

export class DnsConn extends TCPConn {
  net: ?{ name: string, task: string, server?: boolean }

  subscribed: { [sid: number]: ServiceName }

  registered: { [sid: number]: ServiceInfo }

  cid: ConnId
}
