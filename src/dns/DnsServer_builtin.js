// @ts-check
const
  _ = require('lodash'),
  ServiceDefinition = require('../ServiceDefinition'),
  DisService = require('../dis/DisService'),
  debug = require('debug')('dim:dns:server:builtin'),
  { DisDnsCmd } = require('../packets/DisDns');

/**
 * @typedef {{ name: string, task: string, pid: number }} NodeInfoPart
 * @typedef {import('../packets/types').DicReqInfo} DicReqInfo
 * @typedef {import('./DnsServer')} DnsServer
 * @typedef {import('../ServiceInfo')} ServiceInfo
 */

/**
 * @extends DisService<{ [key: string]: number }>
 */
class ServerList extends DisService {
  constructor() {
    super('C');
    /** @type {{ [key: string]: number }} */
    this.value = {};
  }

  setValue() { /* disable*/ }

  /**
   * @param {{ name: string, task: string, pid: number }} nodeInfo
   */
  add(nodeInfo) {
    var name = nodeInfo.task + '@' + nodeInfo.name;

    debug('adding server:', name);
    if (!_.has(this.value, name)) {
      this.value[name] = nodeInfo.pid;
      this.emit('update', '+' + name + '\0' + nodeInfo.pid);
    }
  }

  /**
   * @param {{ name: string, task: string }} nodeInfo
   */
  remove(nodeInfo) {
    var name = nodeInfo.task + '@' + nodeInfo.name;

    const info = _.get(this.value, name);
    if (!_.isNil(info)) {
      debug('removing server:', name);
      _.unset(this.value, name);
      this.emit('update', '-' + name + '\0' + info);
    }
    else {
      debug('failed to remove unknown server:', name);
    }
  }

  getValue() {
    return _.keys(this.value).join('|') + '\0' + _.values(this.value).join('|');
  }
}

/**
 * @param  {DnsServer} dns
 * @param  {DicReqInfo} req
 */
function killServers(dns, req) {
  var code = _.get(req, 'value[0]');
  /** @type {DisDnsCmd} */
  var pkt;
  if (_.isNil(code)) {
    pkt = new DisDnsCmd(DisDnsCmd.Cmd.EXIT, -1, 0);
  }
  else {
    pkt = new DisDnsCmd(DisDnsCmd.Cmd.SOFT_EXIT, code, 0);
  }

  _.forEach(dns._connMap, function(conn) {
    if (_.has(conn, 'registered') && _.get(conn, 'net.task') !== 'DIS_DNS') {
      /* a DIS connection, but not the DNS one */
      conn.send(pkt);
      conn.close();
    }
  });
}

/**
 * @param {ServiceInfo} info
 * @param {Array<string>} rpc
 * @return {boolean}
 */
function _collectRpc(info, rpc) {
  var type = _.get(info, 'definition.type');
  if (type === ServiceDefinition.Type.CMD && info.name &&
      info.name.endsWith('/RpcIn')) {
    /* $FlowIgnore */
    rpc.push(info.name.slice(0, -6));
    return true;
  }
  else if (type === ServiceDefinition.Type.SRV && info.name &&
           info.name.endsWith('/RpcOut')) {
    return true;
  }
  return false;
}

/**
 * @param {string} value
 * @return {(value?: ?string) => boolean}
 */
function _cmpTest(value) {
  if (value === '*') {
    return _.stubTrue;
  }
  else if (_.first(value) === '*' && _.last(value) === '*') {
    return _.partial(_.includes, _, value.slice(1).slice(0, -1));
  }
  else if (_.first(value) === '*') { // @ts-ignore
    return _.partial(_.endsWith, _, value.slice(1));
  }
  else if (_.last(value) === '*') { // @ts-ignore
    return _.partial(_.startsWith, _, value.slice(0, -1));
  }
  else {
    return _.partial(_.eq, value);
  }
}

/**
 * @param {DnsServer} dns
 * @param {DicReqInfo} req
 * @return {string}
 */
function serviceInfo(dns, req) {
  var value = _.get(req, 'value', '*');
  var ret = '';
  /** @type {string[]} */
  var rpc = [];
  var isMatch = _cmpTest(value);

  _.forEach(dns._srvMap, function(record) {
    var info = record.info;
    if (!info) { return; }

    /** @type {ServiceDefinition} */ /* @ts-ignore */
    var def = info.definition;
    if (!_collectRpc(info, rpc) && isMatch(info.name)) {
      ret += _.toString(info.name) + '|' + def.toString(true) + '\n';
    }
  });
  _.forEach(rpc, function(name) {
    if (isMatch(name)) {
      ret += name + '|' +
        ServiceDefinition.toStringParams(_.get(dns._srvMap[name + '/RpcIn'],
          'info.definition.params')) + ',' +
        ServiceDefinition.toStringParams(_.get(dns._srvMap[name + '/RpcOut'],
          'info.definition.params')) + '|RPC\n';
    }
  });
  return ret;
}

module.exports = { ServerList, killServers, serviceInfo };
