// @ts-check
const Service = {
  Fmt: {
    BIT_LE: 0x01,
    BIT_BE: 0x02,
    VAX_FLOAT: 0x010,
    IEEE_FLOAT: 0x20,
    AXP_FLOAT: 0x30
  },
  FMT_DEFAULT: 0x21,
  NAME_MAX: 132,
  DEF_MAX: 132
};

const Node = {
  NAME_MAX: 40,
  TASK_MAX: 36
};

const Dns = {
  PORT_DEFAULT: 2505
};

const SrcTypes = { NONE: 0, DIS: 1, DIC: 2, DNS: 3, DNA: 4, USR: 5 };

const VersionNumber = 2023;

const DEFAULT_HOST = '0.0.0.0';
const DEFAULT_LOCALHOST = '127.0.0.1';

module.exports = { Service, Node, SrcTypes, Dns, VersionNumber, DEFAULT_HOST, DEFAULT_LOCALHOST };
