// @ts-check
const
  { EventEmitter } = require('events'),
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('dim:client'),

  ServiceInfo = require('../ServiceInfo'),
  NodeInfo = require('../NodeInfo'),
  DnsClient = require('../dns/DnsClient'),
  DisNodeRegistry = require('./DisNodeRegistry');

/**
 * @typedef {import('./DisNodeRegistry').DisClientRef} DisClientRef
 */

/**
 * Service connector
 * @extends EventEmitter
 *
 * @details emits:
 *  - attached: this service is attached on a new node
 *  - detached: this service is being detached from this node
 *  - released: the service has been released
 */
class DicService extends EventEmitter {
  /**
   * @param {string|ServiceInfo} service
   * @param {?(DnsClient|NodeInfo|string)=} dns
   */
  constructor(service, dns) {
    super();
    this.name = '';
    this.node = null;
    this._dnsCallback = this.setServiceInfo.bind(this);
    this._init(service, dns);
  }

  /**
   * @param {string|ServiceInfo} service
   * @param {?(DnsClient|NodeInfo|string)=} dns
   */
  _init(service, dns) {
    if (service instanceof ServiceInfo) {
      this.setServiceInfo(service);
    }
    else {
      this.name = service;
    }
    if (dns instanceof NodeInfo) {
      this.setNode(dns);
    }
    else {
      this.setDns(dns);
    }
  }

  release() {
    this.setDns(null);
    this.setNode(null);
    this.emit('released');
  }

  /**
   * @param {(DnsClient|string)?=} dns
   */
  setDns(dns) {
    const dnsClient = DnsClient.wrap(dns);
    if (this.dns) {
      const old = this.dns;
      old.removeListener('service:' + this.name, this._dnsCallback);
      old.unref();
    }
    this.dns = dnsClient;
    if (dnsClient) {
      dnsClient.query(this.name)
      .then(/* @ts-ignore: we're sure to have a DnsClient here */
        () => this.setServiceInfo(dnsClient.getServiceInfo(this.name)),
        (err) => debug('dns request failed:', err));
      dnsClient.on('service:' + this.name, this._dnsCallback);
    }
  }

  /**
   * @param {ServiceInfo} info
   */
  setServiceInfo(info) {
    if (_.isEmpty(this.name)) {
      // @ts-ignore
      this.name = _.get(info, 'name', '');
    }
    this.info = info;
    var node = _.get(info, 'node');
    if (node) {
      this.setNode(node);
    }
  }

  /**
   * @param {NodeInfo?=} nodeInfo
   * @return {?DisClientRef}
   */
  setNode(nodeInfo) {
    nodeInfo = _.result(nodeInfo, 'isValid', false) ? /* @ts-ignore */
      NodeInfo.wrap(nodeInfo) : null;
    if (this.node) {
      if (nodeInfo && this.node.url() === nodeInfo.url()) {
        return null; /* nothing changed */
      }
      debug('detached: %s service:%s', this.node.url(), this.name);
      this.emit('detached', this.node);
      DicService.registry.release(this.node);
      this.node = null;
    }
    if (nodeInfo) {
      this.node = DicService.registry.get(nodeInfo);
      if (this.node && this.dns) {
        this.node.onRemoteClose =
          this.dns.refreshQuery.bind(this.dns, this.name);
      }
      debug('attached: %s service:%s', nodeInfo.url(), this.name);
      this.emit('attached', this.node);
      return this.node;
    }
    return null;
  }

  /**
   * Convenience function to look for a service
   * @param {string|ServiceInfo}   service service to look for
   * @param {(DnsClient|string)?=} dns  DNS to connect to
   * @return {Q.Promise<ServiceInfo>} promise resolving a ServiceInfo
   *
   * @details RPC ServiceInfo won't be resolved by this method,
   * both RpcIn/RpcOut channels needs to be queried.
   */
  /* eslint-disable-next-line max-len */
  static lookup(service, dns) {
    var cli = DnsClient.wrap(dns);
    if (!cli) {
      return q.reject(new Error('failed to parse arguments'));
    }
    return q() // @ts-ignore
    .then(() => cli.query(_.get(service, 'name', service)))
    .finally(cli.unref.bind(cli));
  }
}

DicService.registry = new DisNodeRegistry();

module.exports = DicService;
