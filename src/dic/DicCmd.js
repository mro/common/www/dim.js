// @ts-check
const
  q = require('q'),
  _ = require('lodash'),

  DicService = require('./DicService'),
  NodeInfo = require('../NodeInfo'),
  ServiceInfo = require('../ServiceInfo'),
  { NotFound } = require('../Errors');

/**
 * @typedef {import('../dns/DnsClient')} DnsClient
 * @typedef {import('../ServiceDefinition')} ServiceDefinition
 */

/**
 * [sendCmd description]
 * @param  {string|ServiceInfo} service
 * @param  {any} args
 * @param  {number?} timeout
 * @param  {NodeInfo} nodeInfo
 * @return {Q.Promise<any>}
 */
function sendCmd(service, args, timeout, nodeInfo) {
  var cli = DicService.registry.get(nodeInfo);
  if (!cli) { /* unlikely to happen */
    return q.reject(new NotFound('invalid NodeInfo parameter'));
  }
  return cli.cmd(service, args, { timeout: timeout })
  .finally(() => DicService.registry.release(cli));
}

/**
 * @brief Dim command utility
 * @param {string|ServiceInfo} service service to send command to
 * @param {any} args    command arguments
 * @param {(DnsClient|NodeInfo|string)?=} dns DNS to connect to
 * @param {number?=} timeout request timeout
 * @return {Q.Promise<any>} promise resolving the value
 */
function invoke(service, args, dns, timeout) {
  timeout = _.defaultTo(timeout, DicCmd.TIMEOUT);
  if (service instanceof ServiceInfo &&
      _.result(service.node, 'isValid', false)) {
    // @ts-ignore: checked above
    return sendCmd(service, args, timeout, service.node);
  }
  if (dns instanceof NodeInfo) {
    return sendCmd(service, args, timeout, dns);
  }
  else if (dns) {
    return DicService.lookup(service, dns)
    .then((ret) => {
      if (_.result(ret.node, 'isValid', false)) {
        return invoke(ret, args, null, timeout);
      }
      throw new NotFound('service not found ' +
        _.get(service, 'name', service));
    });
  }
  else {
    return q.reject(new NotFound('insufficient service information'));
  }
}

const DicCmd = {
  TIMEOUT: 3000,
  invoke: invoke
};

module.exports = DicCmd;
