// @ts-check
const
  _ = require('lodash'),

  NodeInfo = require('../NodeInfo'),
  DisClient = require('../dis/DisClient');

class DisClientRef extends DisClient {
  /**
   * @param {NodeInfo} nodeInfo
   */
  constructor(nodeInfo) {
    super(nodeInfo);
    this.ref = 0;
  }
}

/**
 * @brief registry for DisClient connections
 */
class DisNodeRegistry {
  constructor() {
    /** @type {{ [key:string]: DisClientRef }} */
    this.nodes = {};
  }

  /**
   * @param  {?(NodeInfo|string)=} nodeInfo
   * @return {?DisClientRef}
   */
  get(nodeInfo) {
    nodeInfo = NodeInfo.wrap(nodeInfo);
    if (_.isNil(nodeInfo) || !nodeInfo.isValid()) {
      return null;
    }

    var node = this.nodes[nodeInfo.url()];
    if (_.isNil(node)) {
      node = this.nodes[nodeInfo.url()] = new DisClientRef(nodeInfo);
      node.ref = 0;
    }
    node.ref++;
    return node;
  }

  /**
   * @param {?DisClientRef=} node
   */
  release(node) {
    if (node && --node.ref <= 0) {
      _.unset(this.nodes, node.url());
      node.close();
    }
  }

  /**
   * @param {NodeInfo|string} nodeInfo
   * @return {boolean}
   */
  has(nodeInfo) {
    nodeInfo = NodeInfo.wrap(nodeInfo);
    return nodeInfo && _.has(this.nodes, nodeInfo.url());
  }
}

DisNodeRegistry.DisClientRef = DisClientRef;

module.exports = DisNodeRegistry;
