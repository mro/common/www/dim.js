// @ts-check

const
  _ = require('lodash'),
  q = require('q'),

  DicService = require('./DicService'),
  { DicReq } = require('../packets/DicDis'),
  { DnsClient } = require('../dns'),
  NodeInfo = require('../NodeInfo'),
  ServiceInfo = require('../ServiceInfo'),
  { Interrupted, NotFound } = require('../Errors');

/**
 * @typedef {import('./DisNodeRegistry').DisClientRef} DisClientRef
 */

/**
 * DIM Client Value wrapper
 * @template T=any
 * @details emits:
 *  - 'value' whenever value is updated
 */
class DicValue extends DicService {
  /**
   * create a DicValue object
   * @param {ServiceInfo|string} service the service to fetch
   * @param {?{ timeout?: number, stamped?: boolean }=} options request options
   * @param {?(DnsClient|string|NodeInfo)=} dns  DNS to connect to
   *
   * @details available options:
   * - timeout: request timeout value (default: DisClient.TIMEOUT)
   * - stamped: make a stamped request (default: false)
   */
  constructor(service, options, dns) {
    /* @ts-ignore */
    super();
    if (_.isString(options) ||
        (options instanceof DnsClient) ||
        (options instanceof NodeInfo)) {
      /* $FlowIgnore */
      dns = options;
      options = null;
    }
    /** @type {?T=} */
    this.value = undefined;
    this.options = options;
    /** @type {?number} */
    this.timestamp = null;
    this._init(service, dns);
  }

  release() {
    this.setNode(null);
    super.release();
  }

  /**
   * @param {?NodeInfo} nodeInfo
   * @return {?DisClientRef}
   */
  setNode(nodeInfo) {
    if (super.setNode(nodeInfo)) {
      this.once('detached', (node) => {
        node.abort(this.sid);
        this._setValue(undefined);
      });
      /* @ts-ignore */
      this.sid = this.node.monitor(this.info || this.name, this.options,
        this._setValue.bind(this));
      return this.node;
    }
    return null;
  }

  /**
   * @param {any} rep
   */
  _setValue(rep) {
    var value = _.get(rep, 'value');
    if (value === undefined) { /* accept null or 0 as values */
      value = _.get(rep, 'data');
    }
    if (_.has(rep, 'timestamp')) {
      this.timestamp = rep.timestamp;
      this.value = value;
      this.emit('value', value);
    }
    else if (!_.isEqual(value, this.value) ||
      (_.get(this.options, 'timeout', 0) !== 0 &&
      _.get(this.options, 'type') !== DicReq.Type.MONIT_ONLY &&
      _.get(this.options, 'type') !== DicReq.Type.MONIT_FIRST)) {

      this.value = value;
      this.emit('value', value);
    }
  }

  /**
   * @param {(value?: any) => any} fulfill
   * @param {(reason?: any) => any} reject
   */
  then(fulfill, reject) {
    return this.promise().then(fulfill, reject);
  }

  /**
   * @return {Q.Promise<T|void>}
   */
  promise() {
    if (_.isNil(this.dns) && _.isNil(this.node)) { /* detached */
      return q.reject(new NotFound('released'));
    }
    else if (_.isNil(this.value)) {
      var def = q.defer();
      var reject = () => {
        def.reject(new Interrupted('released'));
        this.removeListener('value', resolve);
      };
      /** @type {(value: T | void) => void} */
      var resolve = (value) => {
        def.resolve(value);
        this.removeListener('released', reject);
      };
      this.once('value', resolve);
      this.once('released', reject);
      return def.promise;
    }
    else {
      return q(this.value);
    }
  }

  /**
   * @template T=any
   * @brief retrieve a value once
   * @param {string|ServiceInfo} service service to fetch
   * @param {?(DnsClient|NodeInfo|string)} dns   DNS to connect to
   * @param {?number=} timeout request timeout
   * @param {boolean} [raw=false] resolve raw buffer
   * @return {Q.Promise<T | void>} promise resolving the value
   */
  static get(service, dns, timeout, raw = false) {
    timeout = _.defaultTo(timeout, DicValue.GET_TIMEOUT);

    if (service instanceof ServiceInfo &&
        _.result(service.node, 'isValid', false)) {
      /* $FlowIgnore */
      return getValue(service, timeout, service.node, raw);
    }
    if (dns instanceof NodeInfo) {
      return getValue(service, timeout, dns, raw);
    }
    else if (dns) {
      return DicService.lookup(service, dns)
      .then((ret) => {
        if (_.result(ret.node, 'isValid', false)) {
          return DicValue.get(ret, null, timeout, raw);
        }
        return undefined;
      });
    }
    else {
      return q();
    }
  }
}

/**
 * @param  {ServiceInfo|string} service
 * @param  {?number=} timeout
 * @param  {?NodeInfo=} nodeInfo
 * @param  {?boolean=} raw
 * @return {Q.Promise<any>}
 */
function getValue(service, timeout, nodeInfo, raw) {
  var cli = DicValue.registry.get(nodeInfo);
  if (!cli) {
    return q();
  }
  // @ts-ignore
  return cli.request(service, { timeout: timeout, raw: raw })
  .then((rep) => {
    if (rep) {
      return raw ? rep.data : _.get(rep, 'value', rep.data);
    }
    else {
      return undefined;
    }
  })
  .finally(() => DicValue.registry.release(cli));
}

DicValue.GET_TIMEOUT = 1000;

module.exports = DicValue;
