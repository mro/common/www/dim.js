// @ts-check

const
  _ = require('lodash'),
  debug = require('debug')('dim:client:servicesList'),

  DicValue  = require('./DicValue'),
  ServiceDefinition = require('../ServiceDefinition');

/**
 * @typedef {import('../dns/DnsClient')} DnsClient
 * @typedef {import('../NodeInfo')} NodeInfo
 */

/**
 * DIM Client Value for retrieving a list of services
 * @extends DicValue<{ [serviceName: string]: ServiceDefinition }>
 */
// @ts-ignore: overridden 'get' property does not require service parameter
class DicServicesList extends DicValue {
  /**
  * create a specific DicValue object to retrieve information about services
  * @param {string} taskName
  * @param {?(DnsClient|string|NodeInfo)=} dns
  * @param {?{ timeout?: number, stamped?: boolean }=} options
  *
  * @details available options:
  * - timeout: request timeout value (default: DisClient.TIMEOUT)
  * - stamped: make a stamped request (default: false)
  */
  constructor(taskName, dns, options) {
    super(taskName + '/SERVICE_LIST', options, dns);
  }

  /**
   * Overridden method for service monitoring purpose
   * @param {any} rep
   */
  _setValue(rep) {
    if (_.has(rep, 'value')) {
      // deep cloning not needed, we won't touch unchanged services
      rep.value = update(rep.value, _.clone(this.value) || {});
    }
    super._setValue(rep);
  }

  /**
   * @brief retrieve services information once from a DIM Service Provider
   * @param {string} taskName
   * @param {?(DnsClient|NodeInfo|string)} dns
   * @param {?number=} timeout
   * @return {Promise<{ [serviceName: string]: ServiceDefinition }|void>}
   */
  static async get(taskName, dns, timeout) {
    const ret = await DicValue.get(taskName + '/SERVICE_LIST', dns, timeout);
    if (_.isNil(ret)) {
      return undefined;
    }
    else if (_.isBuffer(ret)) {
      return update(_.toString(ret), {});
    }
    else {
      return update(ret, {});
    }
  }
}

/**
 * @brief Parse the list of services
 * @param {string} data
 * @param {{ [serviceName: string]: ServiceDefinition }} db already registered services
 * @return {{ [serviceName: string]: ServiceDefinition }}
 */
function update(data, db) {
  data = _.split(data, '\0')[0]; /* remove any null char */
  let removing = false; /* make this sticky */

  return _.transform(_.split(data, '\n'), function(ret, srvDef) {
    var srv = _.toString(srvDef);
    var idx = srv.indexOf('|');

    let name = srv.slice(0, idx);
    if (name[0] === '-' || name[0] === '+') {
      removing = (name[0] === '-');
      name = name.slice(1);
    }

    if (removing) {
      delete ret[name];
    }
    else {
      var def = (idx !== -1) ?
        ServiceDefinition.parse(srv.slice(idx + 1)) : null;
      if (_.isNil(def)) {
        debug('failed to parse definition:', srv);
        return;
      }
      ret[name] = def;
    }
  }, db);
}

module.exports = {
  serviceList: DicServicesList.get,
  DicServicesList
};
