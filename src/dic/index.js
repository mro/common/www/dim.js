// @ts-check
const
  DicService = require('./DicService'),
  DicValue = require('./DicValue'),
  DicCmd = require('./DicCmd'),
  DicRpc = require('./DicRpc'),
  DicDis = require('./DicDis'),
  DicDns = require('./DicDns');

module.exports = { DicService, DicValue, DicDis, DicCmd, DicRpc, DicDns };
