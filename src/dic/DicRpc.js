// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  DicService = require('./DicService'),
  { DicReq } = require('../packets/DicDis'),
  DnsClient = require('../dns/DnsClient'),
  ServiceDefinition = require('../ServiceDefinition'),
  DicCmd = require('./DicCmd'),
  { Timeout } = require('../Errors');

/**
 * @typedef {number} sid
 * @typedef {import('../NodeInfo')} NodeInfo
 * @typedef {import('../ServiceInfo')} ServiceInfo
 * @typedef {import('../packets/types').DicRepInfo} DicRepInfo
 */

/**
 * @param  {ServiceDefinition} rpc
 * @param  {?ServiceDefinition} other
 * @return {?ServiceDefinition}       [description]
 */
function mergeDefinition(rpc, other) {
  var type = _.get(other, 'type', -1);
  switch (type) {
  case ServiceDefinition.Type.RPC:
    return other;
  case ServiceDefinition.Type.CMD:
  case ServiceDefinition.Type.SRV:
    if (!rpc) {
      rpc = new ServiceDefinition({ type: ServiceDefinition.Type.RPC });
    }
    if (type === ServiceDefinition.Type.CMD) {
      // @ts-ignore: checked above (type)
      rpc.params = _.clone(other.params);
    }
    else {
      // @ts-ignore: checked above (type)
      rpc.returns = _.clone(other.params);
    }
    return rpc;
  default:
    return rpc;
  }
}

class DicRpc extends DicService {
  /* FIXME: use RPC serviceinfo */

  /**
   * @param  {string|ServiceInfo} service
   * @param {(DnsClient|NodeInfo|string)=} dns
   */
  _init(service, dns) {
    this._rpcOutDef = q.defer();
    this._prom = this._rpcOutDef.promise
    .then(this._inDefLookup.bind(this));
    super._init(service, dns);
  }

  /**
   * @param {DnsClient|String|null} dns
   */
  setDns(dns) {
    dns = DnsClient.wrap(dns);
    if (this.dns) {
      this.dns.unref();
    }
    this.dns = dns;
    if (dns) {
      DicService.lookup(this.name + '/RpcOut', dns)
      .then(this.setServiceInfo.bind(this));
    }
  }

  /**
   * @param {ServiceInfo} info
   */
  setServiceInfo(info) {
    info.definition = mergeDefinition(
      // @ts-ignore: checked in mergeDefinition
      _.get(this.info, 'definition'), info.definition);
    if (this.name) {
      info.name = this.name;
    }
    super.setServiceInfo(info);
  }

  /**
   * @param {NodeInfo?=} nodeInfo
   */
  setNode(nodeInfo) {
    if (super.setNode(nodeInfo)) {
      this.once('detached', (node) => {
        node.abort(this.rpcInSid);
        this._rpcOutDef = q.defer();
        this._prom = this._prom
        .finally(_.constant(this._rpcOutDef.promise))
        .then(this._inDefLookup.bind(this));
      });
      /* @ts-ignore: setNode returns true only when node set */
      this.rpcInSid = this.node.monitor(this.info,
        { type: DicReq.Type.MONIT_ONLY, timeout: 0 },
        this._reply.bind(this));
    }
    /*
      resolve promise, no mater what
      will unlock rpc for unknown services -> triggering an error
    */
    _.invoke(this._rpcOutDef, 'resolve');
    this._rpcOutDef = null;
    return null;
  }

  /**
   * resolve RpcIn parameters (if dns available and needed)
   */
  _inDefLookup() {
    if (this.dns && _.isNil(_.get(this.info, 'definition.params'))) {
      return DicService.lookup(this.name + '/RpcIn', this.dns)
      .then(this.setServiceInfo.bind(this));
    }
    return undefined;
  }

  /**
   * @param {any} args
   * @param {number=} timeout
   * @return {Q.Promise<DicRepInfo>}
   */
  invoke(args, timeout) {
    var def = q.defer();
    timeout = _.defaultTo(timeout, DicRpc.TIMEOUT);

    /* wait for your turn */
    this._prom = this._prom
    .finally(() => {
      this._pending = def;
      return DicCmd.invoke(this.info || (this.name + '/RpcIn'), args,
        _.get(this.node, 'info'), timeout)
      .catch((/** @type {any} */ err) => {
        this._pending = null;
        def.reject(err);
      });
    });

    let timer = setTimeout(() => {
      // @ts-ignore: resetting it
      timer = null;
      this._pending = null;
      def.reject(new Timeout('request time-out'));
    }, timeout);

    return def.promise.finally(() => clearTimeout(timer));
  }

  /**
   * @param  {DicRepInfo} rep
   */
  _reply(rep) {
    _.invoke(this._pending, 'resolve', rep);
  }

  /**
   * @param {string|ServiceInfo} service
   * @param {any} args
   * @param {?(DnsClient|NodeInfo|string)=} dns
   * @param {number=} timeout
   */
  static invoke(service, args, dns, timeout) {
    var rpc = new DicRpc(service, dns);
    return rpc.invoke(args, timeout)
    .finally(rpc.release.bind(rpc));
  }
}

DicRpc.TIMEOUT = 5000;

module.exports = DicRpc;
