// @ts-check
const
  q = require('q'),
  { constant, invoke, now } = require('lodash'),
  { expect } = require('chai'),
  { describe, it, afterEach } = require('mocha'),

  { waitEvent } = require('./utils');

const
  { DicValue, DisNode, ServiceInfo, DnsServer, Errors } = require('../src');

describe('DicValue', function() {
  var env = {};

  afterEach(function() {
    invoke(env.dis, 'close');
    invoke(env.value, 'release');
    invoke(env.dns, 'close');
    env = {};
  });

  it('can retrieve a value', function() {
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('/test', 'I', 42);
    return env.dis.listen()
    .then(() => {
      env.value = new DicValue(new ServiceInfo('/test', 'I'));
      env.value.setNode(env.dis.info);
      return env.value.promise();
    })
    .then((ret) => expect(ret).to.equal(42));
  });

  it('can retrieve a zero value', function() {
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('/test', 'I', 0);
    return env.dis.listen()
    .then(() => {
      env.value = new DicValue(new ServiceInfo('/test', 'I'));
      env.value.setNode(env.dis.info);
      return env.value.promise();
    })
    .then((ret) => expect(ret).to.equal(0));
  });

  it('can retrieve a timestamped value', function() {
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('/test', 'I', 42);
    return env.dis.listen()
    .then(() => {
      env.value = new DicValue(new ServiceInfo('/test', 'I'),
        { stamped: true });
      env.value.setNode(env.dis.info);
      return env.value.promise();
    })
    .then((ret) => expect(ret).to.equal(42))
    .then(() => {
      expect(env.value.timestamp).to.closeTo(now(), 5000);
    });
  });

  it('can monitor a value', function() {
    env.dis = new DisNode('127.0.0.1');

    var service = env.dis.addService('/test', 'I', 42);
    return env.dis.listen()
    .then(() => {
      env.value = new DicValue(new ServiceInfo('/test', 'I'));
      env.value.setNode(env.dis.info);
      return env.value.promise();
    })
    .then((ret) => expect(ret).to.equal(42))
    .then(() => {
      return q()
      /* $FlowIgnore */
      .then(() => service.setValue(21))
      .then(constant(waitEvent(env.value, 'value', 1)))
      .then((ret) => {
        expect(ret).to.equal(21);
        expect(env.value.value).to.equal(21);
      });
    });
  });

  it('detects disconnections', function() {
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('/test', 'I', 42);
    return env.dis.listen()
    .then(() => {
      env.value = new DicValue(new ServiceInfo('/test', 'I'));
      env.value.setNode(env.dis.info);
      return env.value.promise();
    })
    .then(() => {
      return q()
      .then(() => env.dis.close())
      .then(constant(waitEvent(env.value, 'value', 1)))
      .then((ret) => {
        expect(ret).to.be.undefined();
        expect(env.value.value).to.be.undefined();
      });
    });
  });

  it('aborts on release', async function() {
    env.dns = new DnsServer('127.0.0.1');
    env.value = new DicValue(new ServiceInfo('/test', 'I'), null, '127.0.0.1');
    const prom = env.value.promise();
    env.value.release();

    await prom
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.have.property('code', Errors.Interrupted.CODE));

    /* calling prom again on an already released node */
    await env.value.promise()
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.have.property('code', Errors.NotFound.CODE));
  });
});
