
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  { waitEvent } = require('./utils');

const
  { NodeInfo, DnsServer, DnsClient, DisNode } = require('../src'),
  { DisDnsCmd } = require('../src').packets;

describe('DisNode cmd', function() {
  var env;

  beforeEach(function() {
    env = {};
    env.dns = new DnsServer(null, 0);
    return env.dns.listen();
  });

  afterEach(function() {
    _.invoke(env.dns, 'close');
    _.invoke(env.cli, 'close');

    _.invoke(env.dis, 'close');
    _.invoke(env.disDns, 'close');
    env = {};
  });

  _.forEach([
    { name: 'STOP', cmd: DisDnsCmd.Cmd.STOP, info: null, ret: -1 },
    { name: 'EXIT', cmd: DisDnsCmd.Cmd.EXIT, info: null, ret: -1 },
    { name: 'SOFT_EXIT', cmd: DisDnsCmd.Cmd.SOFT_EXIT, info: 42, ret: 42 }
  ], (test) => it(`handles ${test.name} command`, function() {
    env.dis = new DisNode(NodeInfo.local(0, 0, 'TEST'));

    env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port,
      { retryDelay: 200 });
    return q()
    .then(() => env.disDns.register(env.dis))
    .then(_.constant(waitEvent(env.dns, 'update')))
    .then(() => {
      return q()
      .then(() => _.forEach(env.dns._connMap, (conn) => {
        conn.send(new DisDnsCmd(test.cmd, test.info));
        conn.close();
      }))
      .then(_.constant(waitEvent(env.dis, 'exit')))
      .then((value) => expect(value).to.equal(test.ret));
    })
    .delay(500) /* wait to ensure dns is not reconnecting */
    .then(() => expect(env.disDns._conn.connected).to.be.false());
  }));
});
