
const
  { expect } = require('chai'),
  { describe, it, afterEach } = require('mocha'),
  { waitEvent } = require('./utils');

const
  utils = require('../src/utils'),
  { DisNode, NodeInfo, DnsServer, DnsClient } = require('../src');

describe('DisNode', function() {
  var env = {};

  afterEach(function() {
    if (env.node) {
      env.node.close();
      delete env.node;
    }
    if (env.dns) {
      env.dns.close();
      delete env.dns;
    }
    if (env.client) {
      env.client.close();
      delete env.client;
    }
  });

  it('can listen on 127.0.0.1', function() {
    env.node = new DisNode('127.0.0.1');

    return env.node.listen()
    .then(() => {
      expect(env.node._conn.port).to.be.greaterThan(0);
      expect(env.node._conn.port).to.equal(env.node.info.port);

      expect(env.node.info.host).to.equal(0x7F000001);
    });
  });

  it('can listen on 0.0.0.0', function() {
    env.node = new DisNode('0.0.0.0');

    return env.node.listen()
    .then(() => {
      expect(env.node._conn.port).to.be.greaterThan(0);
      expect(env.node._conn.port).to.equal(env.node.info.port);

      /* resolved when sent to DNS depending on iface */
      expect(env.node.info.host).to.equal(0);
    });
  });

  it('can call listen multiple times', function() {
    env.node = new DisNode('127.0.0.1');
    var port;

    env.node.listen();
    return env.node.listen()
    .then((ret) => {
      expect(ret).to.equal(env.node);
      expect(env.node.info.host).to.equal(0x7F000001);
      port = env.node.info.port;
      expect(port).to.be.greaterThan(0);
      return env.node.listen();
    })
    .then(() => {
      expect(env.node.info.host).to.equal(0x7F000001);
      expect(env.node.info.port).to.equal(port);
    });
  });

  it('handles long hostnames', function() {
    /*
      the node should be named using its address rather than name on some hosts
    */
    var info = NodeInfo.local(0, 0);
    info.name = 'an extra long name with exactly 40 chars';
    env.node = new DisNode('127.0.0.1', 0, info);

    return env.node.listen()
    .then(() => {
      expect(env.node.info.name)
      .to.equal(utils.toStrAddr(env.node.info.host));
    });
  });

  it('handles long hostnames with default host', function()  {
    var info = NodeInfo.local(0, 0);
    info.name = 'an extra long name with exactly 40 chars';
    env.node = new DisNode(0, 0, info);

    return env.node.listen()
    .then(() => {
      expect(env.node.info.name)
      .to.equal(utils.getPublicAddress());
    });
  });

  it('connects on default host when null is provided', function() {
    env.node = new DisNode(null, 0);

    return env.node.listen()
    .then(() => {
      expect(env.node._conn.host).to.equal('0.0.0.0');
      expect(env.node.info.host).to.equal(0);
    });
  });

  it('handles DnsClient refcount correctly', async function() {
    this.timeout(10000);
    env.dns = new DnsServer('127.0.0.1');
    env.node = new DisNode();
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port,
      { retryDelay: 500 });

    expect(env.client).to.have.property('_ref', 1);

    await env.dns.listen();

    await env.node.register(env.client);

    expect(env.client).to.have.property('_ref', 2);
    env.client.unref();

    const clientWaiter = waitEvent(env.client, 'close', 1, 5000);
    await env.node.close();

    await clientWaiter;
    expect(env.client).to.have.property('_ref', 0);
  });
});
