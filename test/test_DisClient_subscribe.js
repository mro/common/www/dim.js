// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, afterEach, beforeEach } = require('mocha'),
  EventEmitter = require('events'),

  utils = require('./utils'),
  { DisNode, DisClient, ServiceInfo } = require('../src');

describe('DisClient', function() {
  var env;

  beforeEach(function() {
    env = {};
    env.dis = new DisNode('127.0.0.1');
    return env.dis.listen();
  });

  afterEach(function() {
    env.dis.close();
  });

  it('can monitor a value', function() {
    var cli = new DisClient(env.dis.info);
    var evt = new EventEmitter();

    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to create service'; }

    cli.monitor(new ServiceInfo('/TEST', 'I'), null,
      (rep) => evt.emit('value', _.get(rep, 'value', rep)));

    return q()
    .then(_.constant(utils.waitEvent(evt, 'value')))
    .then((value) => expect(value).to.equal(42))
    .then(() => srv.setValue(44))
    .then(_.constant(utils.waitEvent(evt, 'value', 2)))
    .then((value) => expect(value).to.deep.equal([ 42, 44 ]))
    .finally(() => cli.close())
    .finally(() => evt.removeAllListeners());
  });

  it('can poll a value', function() {
    var cli = new DisClient(env.dis.info);
    var evt = new EventEmitter();

    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to create service'; }

    cli.interval(new ServiceInfo('/TEST', 'I'), 1000, null,
      (rep) => evt.emit('value', _.get(rep, 'value', rep)));

    var startTime = _.now();
    return q()
    .then(_.constant(utils.waitEvent(evt, 'value')))
    .then((value) => expect(value).to.equal(42))

    /* ensure that we are not tracking modifications */
    .then(() => srv.setValue(44))
    .then(_.constant(utils.waitEvent(evt, 'value', 2, 3000)))
    .tap(() => expect(_.now() - startTime).within(950, 1050))
    .then((value) => expect(value).to.deep.equal([ 42, 44 ]))
    .finally(() => cli.close())
    .finally(() => evt.removeAllListeners());
  });

  it('sends an undefined value when dis is closed', function() {
    var cli = new DisClient(env.dis.info);
    var evt = new EventEmitter();

    var srv = env.dis.addService('/TEST', 'I', 42);
    if (!srv) { throw 'failed to create service'; }

    cli.monitor(new ServiceInfo('/TEST', 'I'), null,
      (rep) => evt.emit('value', _.get(rep, 'value', rep)));

    return q()
    .then(_.constant(utils.waitEvent(evt, 'value')))
    .then((value) => expect(value).to.equal(42))
    .then(() => cli.close())
    .then(_.constant(utils.waitEvent(evt, 'value', 2)))
    .then((value) => expect(value).to.deep.equal([ 42, undefined ]))
    .finally(() => evt.removeAllListeners());
  });
});
