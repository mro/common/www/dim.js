
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  { DnaStream, DnaNet } = require('../src/packets/Dna');

describe('DnaStream', function() {
  it('can stream a packet', function(done) {
    var stream = new DnaStream((pkt) => {
      expect(pkt).ok();
      expect(DnaNet.parse(pkt)).to.deep.equal({ name: 'test', task: 'task' });
      done();
    });
    stream.add(new DnaNet('test', 'task').raw);
  });

  it('can read really fragmented packet', function(done) {
    var stream = new DnaStream((pkt) => {
      expect(pkt).ok();
      expect(DnaNet.parse(pkt)).to.deep.equal({ name: 'test', task: 'task' });
      done();
    });
    var pkt = new DnaNet('test', 'task').raw;
    for (var i = 0; i < pkt.length; ++i) {
      stream.add(pkt.slice(i, i + 1));
    }
  });

  it('can filter out garbage', function(done) {
    var stream = new DnaStream((pkt) => {
      expect(pkt).ok();
      expect(DnaNet.parse(pkt)).to.deep.equal({ name: 'test', task: 'task' });
      done();
    });

    var pkt = Buffer.concat([
      /* add a magic before our content on purpose */
      Buffer.from('17151311', 'hex'),
      new DnaNet('test', 'task').raw
    ]);

    for (var i = 0; i < pkt.length; ++i) {
      stream.add(pkt.slice(i, i + 1));
    }
  });

  it('throws garbage after a while', function() {
    var stream = new DnaStream(_.noop, 100);

    stream.add(Buffer.from('17151311', 'hex'));
    return q()
    .then(() => expect(stream._buffer.length).to.be.above(0))
    .delay(150)
    .then(() => expect(stream._buffer.length).to.be.equal(0));
  });

});
