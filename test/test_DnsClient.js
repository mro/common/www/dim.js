// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  { expect } = require('chai'),
  { describe, it, afterEach, beforeEach } = require('mocha'),
  sinon = require('sinon');

const
  { DnsServer, DnsClient, Errors } = require('../src'),
  { waitEvent } = require('./utils');

describe('DnsClient', function() {
  var env = {};
  var sandbox;

  beforeEach(function() {
    sandbox = sinon.createSandbox();
    env.dns = new DnsServer();
    return env.dns.listen();
  });

  afterEach(function() {
    sandbox.restore();
    _.invoke(env.dns, 'close');
    _.invoke(env.client, 'close');
    env = {};
  });

  it('can make a lot of different requests', function() {
    /* ensure that we can request a lot of services */
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    const total = 1000;

    function loopRequest(count) {
      if (count <= 0) { return q(); }
      count -= 20;

      return q.all(_.times(20, (i) => env.client.query('/srv' + (count + i))))
      .then(() => loopRequest(count));
    }

    return q()
    .then(() => env.client.connect())
    .then(() => loopRequest(total));
  });

  it('can repeat the same request', function() {
    /* this produces a MaxListenersExceededWarning */
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    const total = 20;

    return q()
    .then(() => env.client.connect())
    .then(() => q.all(_.times(total, () => env.client.query('/srv'))));
  });

  it('resolves query on close', function() {
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port);

    /* not pre-connecting on purpose */
    return q()
    .then(() => {
      var prom = env.client.query('/srv');
      env.client.close();
      return prom;
    })
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.have.property('code', Errors.Interrupted.CODE)
    );
  });

  it('retries to connect to the DNS Server on connection failure', function() {
    var connected = null;
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port,
      { retryDelay: 500 });

    env.client.on('connect', () => { connected = true; });
    env.client.on('disconnect', () => { connected = false; });

    env.dns.close();

    return env.client.connect()
    .then(
      () => expect.fail('should fail'),
      (err) => {
        expect(err).to.have.property('code', Errors.NotFound.CODE);
        expect(connected).to.be.false();
        return env.dns.listen();
      }
    )
    .then(_.constant(waitEvent(env.client, 'connect')))
    .then(() => expect(connected).to.be.true());
  });

  it('tries to connect several times on failure', async function() {
    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port,
      { retryDelay: 250 });

    env.dns.close();
    sandbox.stub(env.client, 'connect')
    .callsFake(function() {
      this.emit('attempt');
      return this.connect.wrappedMethod.call(this, ...arguments);
    });

    const waiter = waitEvent(env.client, 'attempt', 3);

    await env.client.connect()
    .then(() => expect.fail('should fail'), _.noop);

    await waiter;
    env.client.close();
  });

  it('adds only one listener for the \'close\' event', async function() {

    env.client = new DnsClient(env.dns._conn.host, env.dns._conn.port,
      { retryDelay: 500 });

    expect(env.client._conn.listenerCount('close')).to.be.equal(0);

    env.client.connect();
    await waitEvent(env.client, 'connect');

    expect(env.client._conn.listenerCount('close')).to.be.equal(1);

    var prom = waitEvent(env.client, 'disconnect');
    env.dns.close(); /* close dns server to trigger client reconnection */
    await prom;

    prom = waitEvent(env.client, 'connect');
    env.dns.listen(); /* restart dns server */
    await prom;

    expect(env.client._conn.listenerCount('close')).to.be.equal(1);
  });

});
