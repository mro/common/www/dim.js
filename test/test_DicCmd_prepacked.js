// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DicCmd, DisNode, ServiceInfo, DnsServer,
    DnsClient } = require('../src');

describe('DicCmd', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    env.dis.addCmd('/test', 'C', (req) => env.ret.push(req.value));
    env.ret = [];

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      return cli.register(env.dis)
      .then(() => cli.unref()); /* drop our ref, now it's managed by DisNode */
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    env = {};
  });

  it('can call a prepacked command', async function() {
    var ret = await DicCmd.invoke('/test', Buffer.from('12\0'), env.dns.url());
    expect(ret).to.equal(null);

    ret = await DicCmd.invoke(
      new ServiceInfo('/test', 'C|CMD', null, env.dis.info),
      Buffer.from('12\0'));
    expect(ret).to.equal(null);

    ret = await DicCmd.invoke(new ServiceInfo('/test', 'C|CMD'),
      Buffer.from('12\0'), env.dis.info);
    expect(ret).to.equal(null);
    await q().delay(100);
    expect(env.ret).to.deep.equal([ '12', '12', '12' ]);
  });
});
