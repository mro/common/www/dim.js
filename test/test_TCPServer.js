
const
  { expect } = require('chai'),
  debug = require('debug')('dim:tests'),
  { describe, it, afterEach } = require('mocha');

const
  { TCPConn, TCPServer } = require('../src/Conn'),
  { DnaNet } = require('../src/packets/Dna');

describe('TCPServer', function() {
  var srv;

  afterEach(function() {
    if (srv) {
      debug('closing server');
      srv.close();
    }
  });

  it('can connect', function(done) {
    var cli;

    srv = new TCPServer();
    srv.once('connect', (conn) => {
      conn.once('packet', (pkt) => {
        expect(DnaNet.parse(pkt)).ok();
        expect(cli).ok();

        cli.once('close', () => done());
        conn.close();
      });
    });

    srv.listen()
    .then(() => {
      cli = new TCPConn(srv.host, srv.port);
      return cli.connect();
    });
  });
});
