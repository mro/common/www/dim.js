
const
  q = require('q'),
  { expect } = require('chai'),
  debug = require('debug')('tests'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  net = require('net'),
  { TCPConn } = require('../src/Conn');

describe('TCPConn', function() {
  var srv;

  beforeEach(function(done) {
    debug('starting server');
    srv = net.createServer((sock) => {
      sock.once('close', () => sock.removeAllListeners());
      sock.resume();
    });
    srv.listen(() => done());
  });

  afterEach(function() {
    srv.close();
    srv.removeAllListeners();
    srv = null;
  });

  function serverSock(srv) {
    var defer = q.defer();
    srv.once('connection', (s) => defer.resolve(s));
    return defer.promise;
  }

  it('can connect', function() {
    var conn = new TCPConn(srv.address().address, srv.address().port);
    return conn.connect()
    .then(() => conn.close());
  });

  it('fails to connect when port closed', function(done) {
    var addr = srv.address().address;
    var port = srv.address().port;
    srv.close();

    var conn = new TCPConn(addr, port);
    conn.connect()
    .then(
      () => { throw 'should have failed'; },
      () => done()
    );
  });

  it('send signal on open/close', function(done) {
    var conn = new TCPConn(srv.address().address, srv.address().port);

    conn.once('connect', function() {
      conn.once('close', (err) => {
        expect(err).to.equal(false);
        done();
      });
      conn.close();
    });
    conn.connect();
  });

  it('sends a signal on connection error', function() {
    var called = false;
    var addr = srv.address().address;
    var port = srv.address().port;
    srv.close();

    var conn = new TCPConn(addr, port);
    conn.once('close', (err) => {
      expect(err).to.equal(true);
      called = true;
    });
    return conn.connect().catch(() => expect(called).to.be.true());
  });

  it('detects FIN packets', function(done) {
    /* should be automatically handled by node (allowHalfOpen:false) */
    var conn = new TCPConn(srv.address().address, srv.address().port);
    var servPromise = serverSock(srv);

    conn.connect()
    .then(() => servPromise)
    .then((sock) => {
      conn.once('close', () => done());
      sock.end();
    });
  });
});
