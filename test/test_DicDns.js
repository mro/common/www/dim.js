// @ts-check
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  { DicDnsReq, DicDnsRep } = require('../src/packets/DicDns'),
  NodeInfo = require('../src/NodeInfo'),
  ServiceInfo = require('../src/ServiceInfo');

describe('DicDns', function() {
  it('can create DicDnsReq packets', function() {
    var pkt = new DicDnsReq("/sample/service", 12);

    var parsed = DicDnsReq.parse(pkt.data());
    expect(parsed).to.deep.equal({ sid: 12, service: '/sample/service' });
  });

  it('can create DicDnsRep packets', function() {
    var info = NodeInfo.local('127.0.0.1', 1234);
    /* test would fail on hosts with hostname > 40,
        another testcase checks this for DisNode */
    info.name = info.task = 'TEST';
    var pkt = new DicDnsRep("def", info, 12);

    var parsed = DicDnsRep.parse(pkt.data());
    expect(parsed).to.deep.equal(new ServiceInfo(null, "def", 12, info));
  });

  it('fails to parse empty packets', function() {
    expect(DicDnsReq.parse(Buffer.from(''))).to.be.null();
    expect(DicDnsReq.parse(Buffer.alloc(DicDnsReq.LEN))).to.be.null();

    expect(DicDnsRep.parse(Buffer.from(''))).to.be.null();
    expect(DicDnsRep.parse(Buffer.alloc(DicDnsRep.LEN))).to.be.null();
  });

  it('can create empty packets', function() {
    expect(new DicDnsReq()).to.deep.equal(new DicDnsReq('', 1));
    expect(new DicDnsRep()).to.deep.equal(new DicDnsRep('', new NodeInfo(), 1));
  });

  it('can parse corrupted packets', function() {
    /* current C implementation sends corrupted definition when service is not
      found */
    var pkt = new DicDnsRep("invalid definition");

    const info = DicDnsRep.parse(pkt.data());
    expect(_.get(info, 'definition')).to.equal(null);
    expect(_.get(info, 'node').isValid()).to.be.false();
  });
});
