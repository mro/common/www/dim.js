
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, afterEach } = require('mocha');

const
  { DisService } = require('../src');

describe('DisService', function() {
  var service;

  afterEach(function() {
    if (!_.isNil(service)) {
      service.removeAllListeners();
    }
  });

  it('sends updates', function() {
    var values = [];

    service = new DisService();
    expect(service.value).to.be.undefined();
    service.on('update', (value) => values.push(value));

    service.setValue(42);
    expect(values).to.deep.equal([ 42 ]);
    expect(service.value).to.equal(42);

    service.setValue(44);
    expect(values).to.deep.equal([ 42, 44 ]);
    expect(service.value).to.equal(44);

    service.setValue(44); /* shouldn't send an update */
    expect(values).to.deep.equal([ 42, 44 ]);
    expect(service.value).to.equal(44);
  });
});
