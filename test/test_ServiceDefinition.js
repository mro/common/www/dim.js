const
  _ = require('lodash'),
  Long = require('long'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  ServiceDefinition = require('../src/ServiceDefinition');

describe('ServiceDefinition', function() {
  it('can parse a definition', function() {
    var definition = ServiceDefinition.parse('I:2;C');
    expect(definition)
    .deep.equal({
      type: ServiceDefinition.Type.SRV,
      params: [
        { fmt: 'I', count: 2 },
        { fmt: 'C', count: ServiceDefinition.PARAM_ANY }
      ],
      returns: null
    });
    expect(_.toString(definition)).to.equal('I:2;C');
  });

  it('can stringify a definition', function() {
    var desc = ServiceDefinition.parse('I:2;F;C');
    expect(desc).ok();
    /* FIXME: check if it should be F:1 or F */
    expect(_.toString(desc)).to.equal('I:2;F:1;C');
  });

  it('can serialize data', function() {
    var desc = ServiceDefinition.parse('I:2;F;C');
    expect(desc).ok();

    var values = [ [ 12, 13 ], 12.44, "this is a test" ];
    expect(desc.paramsSize(values)).to.equal(27);
    var buffer = Buffer.allocUnsafe(desc.paramsSize(values));

    expect(desc.packParams(values, buffer)).to.be.true();
    expect(buffer).to.deep.equal(Buffer.from(_.concat([
      0x0C, 0, 0, 0,
      0x0D, 0, 0, 0,
      0x3d, 0x0a, 0x47, 0x41
    ],
    _.map("this is a test", (s) => s.charCodeAt(0)),
    0)
    ));

    var unpacked = desc.unpackParams(buffer);
    expect(unpacked).ok();
    expect(unpacked[0]).to.deep.equal(values[0]);
    expect(unpacked[1]).to.be.closeTo(values[1], 0.001);
    expect(unpacked[2]).to.equal("this is a test");
  });

  it('can serialize single data', function() {
    _.forEach([
      /* we use count:'*' but we send only one value (on purpose) */
      { desc: 'I', value: 42 },
      { desc: 'I:1', value: 42 },
      { desc: 'C:1', value: 42 },
      { desc: 'C', value: '*' },
      { desc: 'L', value: 42 },
      { desc: 'S', value: 42 },
      { desc: 'F', value: 12.42 },
      { desc: 'D', value: 12.42 },
      { desc: 'X', value: Long.fromValue(42, false) }
    ], function(test) {
      var desc = ServiceDefinition.parse(test.desc);
      expect(desc).ok();

      var buffer = Buffer.allocUnsafe(desc.paramsSize(test.value));
      expect(desc.packParams(test.value, buffer)).to.be.true();

      if (_.isNumber(test.value) && !_.isInteger(test.value)) {
        expect(desc.unpackParams(buffer)).to.be.closeTo(test.value, 0.001);
      }
      else {
        expect(desc.unpackParams(buffer)).to.deep.equal(test.value);
      }
    });
  });

  it('can serialize variable arrays', function() {
    var desc = ServiceDefinition.parse('I');
    expect(desc).ok();

    var buffer = Buffer.allocUnsafe(12);
    expect(desc.packParams([ 12, 13, 14 ], buffer)).to.be.true();

    expect(desc.unpackParams(buffer)).to.deep.equal([ 12, 13, 14 ]);
  });

  it('can describe an RPC service', function() {
    var desc = ServiceDefinition.parse('I:1,C|RPC');
    expect(desc).ok();

    expect(desc.params).to.deep.equal([ { fmt: 'I', count: 1 } ]);
    expect(desc.returns).to.deep.equal([ { fmt: 'C', count: '*' } ]);
    expect(desc.toString()).to.equal('I:1,C|RPC');
  });

  it('supports implicit RPC services', function() {
    var desc = ServiceDefinition.parse('I:1,C');
    expect(desc).ok();
    expect(desc.toString()).to.equal('I:1,C|RPC');
  });

  it('supports commands without arguments', function() {
    let desc = ServiceDefinition.parse('|RPC');
    expect(desc).to.deep.contains({
      params: [], returns: [],
      type: ServiceDefinition.Type.RPC
    }, 'failed to parse empty RPC');
    expect(desc.toString()).to.equal('|RPC');

    desc = ServiceDefinition.parse(',I:2|RPC');
    expect(desc).to.deep.contains({
      params: [], returns: [ { count: 2, fmt: 'I' } ],
      type: ServiceDefinition.Type.RPC
    }, 'failed to parse empty RPC params');
    expect(desc.toString()).to.equal(',I:2|RPC');

    desc = ServiceDefinition.parse('|CMD');
    expect(desc).to.deep.contains({
      params: [], returns: null,
      type: ServiceDefinition.Type.CMD
    }, 'failed to parse empty CMD');
    expect(desc.toString()).to.equal('|CMD');
  });

  it('supports unknown services', function() {
    const desc = ServiceDefinition.parse('');
    expect(desc).to.deep.contains({
      params: [], returns: null,
      type: ServiceDefinition.Type.SRV
    }, 'failed to parse unknown service');
    expect(desc.toString()).to.equal('');
  });

  it('fails to parse invalid definition', function() {
    expect(ServiceDefinition.parse('42')).to.be.null();
    expect(ServiceDefinition.parse('Z')).to.be.null();
    expect(ServiceDefinition.parse('C:Z')).to.be.null();
  });
});
