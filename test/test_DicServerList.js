const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  { waitEvent, waitForValue } = require('./utils'),
  { DicDns, DnsServer, DnsClient, DisNode, NodeInfo } = require('../src');


describe('DicServerList', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    await env.dns.listen();
    const cli = DnsClient.wrap(env.dns.url());
    await cli.register(env.dis);
    cli.unref();
  });

  afterEach(async function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dis2, 'close');
    _.invoke(env.dns, 'close');
    _.invoke(env.cli, 'release');
    env = {};
  });

  it('can retrieve a server list', async function() {
    const list = await DicDns.serverList(env.dns.url());
    expect(list).to.be.deep.equal([ { task: env.dis.info.task,
      node: env.dis.info.name, pid: env.dis.info.pid } ]);
  });

  it('can monitor a server list', async function() {
    env.cli = new DicDns.DicServerList(env.dns.url());

    const serverList = [ { task: env.dis.info.task, node: env.dis.info.name,
      pid: env.dis.info.pid } ];

    let ret = await env.cli.promise();
    expect(ret).to.be.deep.equal(serverList);

    env.cli.on('value', (val) => { ret = val; });

    env.dis.close(); /* remove DIM Server */
    await waitEvent(env.cli, 'value');
    expect(ret).to.be.deep.equal([]);
  });

  it('can handle service updates', async function() {
    env.cli = new DicDns.DicServerList(env.dns.url());

    const values = [];
    env.cli.on('value', (val) => values.push(val));

    await env.cli.promise();

    env.dis2 = new DisNode('127.0.0.1', undefined,
      NodeInfo.local(0, 0, 'another'));
    const dnsCli = DnsClient.wrap(env.dns.url());
    await dnsCli.register(env.dis2);
    dnsCli.unref();

    await waitForValue(() => env.cli.value.length, 2, '2 servers connected');
    env.dis.close(); /* remove DIM server */

    await waitForValue(() => env.cli.value.length, 1, '1 server connected');
    expect(env.cli.value).to.deep.equal([ {
      task: env.dis2.info.task, node: env.dis2.info.name,
      pid: env.dis2.info.pid
    } ]);
    expect(values.length).to.equal(3, '3 updates received');
  });
});
