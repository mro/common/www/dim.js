
const
  q = require('q'),
  _ = require('lodash'),
  sinon = require('sinon'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DicRpc, DicService, DisNode, ServiceInfo, DnsServer,
    DnsClient } = require('../src'),
  ServiceDefinition = require('../src/ServiceDefinition'),
  { NotFound, Timeout } = require('../src').Errors;

describe('DicRpc', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    expect(env.dis.addRpc('/test', 'I,C|RPC',
      (req) => 'ok: ' + req.value)).ok();
    env.ret = [];

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      return cli.register(env.dis)
      .then(() => cli.unref()); /* drop our ref, now it's managed by DisNode */
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    env = {};
  });

  it('can call a rpc', function() {
    var rpc = new DicRpc('/test', env.dns.url());
    return q()
    .then(() => rpc.invoke(12))
    .then((ret) => expect(ret).to.have.property('value', 'ok: 12'))
    .then(() => {
      expect(rpc.info.definition)
      .to.deep.equal(ServiceDefinition.parse('I,C|RPC'));
      expect(rpc.info.name).equal('/test');
    })
    .finally(() => rpc.release());
  });

  it('can make direct services call', function() {
    return q()
    .then(() => DicRpc.invoke(
      new ServiceInfo('/test', 'I,C|RPC'), 12, env.dis.info))
    .then((ret) => expect(ret).to.have.property('value', 'ok: 12'))

    .then(() => DicRpc.invoke(
      new ServiceInfo('/test', 'I,C|RPC', null, env.dis.info), 12))
    .then((ret) => expect(ret).to.have.property('value', 'ok: 12'));
  });

  it('can make a call using partial info and a DNS', function() {
    return q()
    .then(() => DicRpc.invoke(new ServiceInfo('/test', 'I,C|RPC'), 12,
      env.dns.url()))
    .then((ret) => expect(ret).to.have.property('value', 'ok: 12'));
  });

  it('command fails if service doesn\'t exist', function() {
    var rpc = new DicRpc('/test2', env.dns.url());
    return q()
    .then(() => rpc.invoke(12))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.be.instanceOf(NotFound)
    )
    .finally(() => rpc.release());
  });

  it('releases resources', function() {
    return q()
    .then(() => DicRpc.invoke('/test', 12, env.dns.url()))
    .then((ret) => expect(ret).to.have.property('value', 'ok: 12'))
    .then(() => expect(DicService.registry.nodes).to.be.empty());
  });

  it('should fail properly when service doesn\'t exist', function() {
    sinon.spy(DnsClient.prototype, 'query');
    return q()
    .then(() => DicRpc.invoke(new ServiceInfo('/test42', 'I,C|RPC'), 12,
      env.dns.url()))
    .then(
      () => { throw 'should fail'; },
      (err) => {
        expect(err).to.be.instanceOf(NotFound);
        expect(DnsClient.prototype.query.callCount)
        .to.be.equal(1, 'should query the DNS only once');
      })
    .finally(() => DnsClient.prototype.query.restore());
  });

  it('raises an error if reply isn\'t received before timeout (ms)',
    async function() {
      /* add a service with a reply delay of 1.5 seconds */
      expect(env.dis.addRpc('/test_1', 'I,I|RPC', async function(req) {
        await q().delay(1500); /* ms */
        return req.value;
      })).ok();

      let didReturn = false;

      try {
        await DicRpc.invoke('/test_1', 33, env.dns.url(), 1000);
        didReturn = true;
      }
      catch (err) {
        expect(err).to.have.property('code', Timeout.CODE);
      }
      expect(didReturn).to.be.false();
    });
});
