
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, afterEach } = require('mocha'),

  { waitEvent } = require('./utils');

const
  { DisNode, DisClient, NodeInfo, ServiceInfo,
    ServiceDefinition, DicDis } = require('../src'),
  { VersionNumber } = require('../src/Consts');

describe('DisNode built-in', function() {
  var env = {};

  afterEach(function() {
    _.invoke(env.node, 'close');
    _.invoke(env.cli, 'close');
    env = {};
  });

  it('serves VERSION_NUMBER', function() {
    env.node = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));

    return env.node.listen()
    .then(() => {
      env.cli = new DisClient(env.node.info);
      return env.cli.connect();
    })
    .then(() => env.cli.request(new ServiceInfo('TEST/VERSION_NUMBER', 'I')))
    .then((ret) => expect(ret.value).equal(VersionNumber));
  });

  it('serves SERVICE_LIST', function() {
    env.node = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));

    return env.node.listen()
    .then(() => DicDis.serviceList('TEST', env.node.info))
    .then((ret) => {
      expect(ret).to.deep.equal({
        "TEST/SERVICE_LIST": ServiceDefinition.parse('C'),
        "TEST/VERSION_NUMBER": ServiceDefinition.parse('L:1'),
        "TEST/CLIENT_LIST": ServiceDefinition.parse('C'),
        "TEST/SET_EXIT_HANDLER": ServiceDefinition.parse('L:1|CMD'),
        "TEST/EXIT": ServiceDefinition.parse('L:1|CMD')
      });
    });
  });

  it('serves CLIENT_LIST', function() {
    env.node = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));

    return env.node.listen()
    .then(() => {
      env.cli = new DisClient(env.node.info);
      return env.cli.connect();
    })
    .then(() => env.cli.request(new ServiceInfo('TEST/CLIENT_LIST', 'C')))
    .then((ret) => {
      expect(ret.value).to.be.a('string');
      expect(ret.value).to.not.be.empty();
    });
  });

  it('services SET_EXIT_HANDLER command', function() {
    env.node = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));

    return env.node.listen()
    .then(() => {
      env.cli = new DisClient(env.node.info);
      return env.cli.connect();
    })
    .then(() => env.cli.cmd(
      new ServiceInfo('TEST/SET_EXIT_HANDLER', 'L:1|CMD'), 42))
    .then(() => env.cli.close())
    .then(_.constant(waitEvent(env.node, 'exit')))
    .then((code) => expect(code).to.equal(42));
  });

  it('services EXIT command', function() {
    env.node = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));

    return env.node.listen()
    .then(() => {
      env.cli = new DisClient(env.node.info);
      return env.cli.connect();
    })
    .then(() => env.cli.cmd(
      new ServiceInfo('TEST/EXIT', 'L:1|CMD'), 42))
    .then(_.constant(waitEvent(env.node, 'exit')))
    .then((code) => expect(code).to.equal(42));
  });
});
