
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DicCmd, DicService, DisNode, ServiceInfo, DnsServer,
    DnsClient } = require('../src'),
  { NotFound } = require('../src').Errors;

describe('DicCmd', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    env.dis.addCmd('/test', 'I', (req) => env.ret.push(req.value));
    env.ret = [];

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      return cli.register(env.dis)
      .then(() => cli.unref()); /* drop our ref, now it's managed by DisNode */
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    env = {};
  });

  it('can call a command', function() {
    return q()
    .then(() => DicCmd.invoke('/test', 12, env.dns.url()))
    .then((ret) => expect(ret).to.equal(null))

    .then(() => DicCmd.invoke(
      new ServiceInfo('/test', 'I|CMD', null, env.dis.info), 12))
    .then((ret) => expect(ret).to.equal(null))

    .then(() => DicCmd.invoke(new ServiceInfo('/test', 'I|CMD'), 12,
      env.dis.info))
    .then((ret) => expect(ret).to.equal(null))

    .delay(100)
    .then(() => expect(env.ret).to.deep.equal([ 12, 12, 12 ]));
  });

  it('command fails if service doesn\'t exist', function() {
    return q()
    /* this doesn't fail since command don't have a reply,
      still DIS will close the connection */
    .then(() => DicCmd.invoke(new ServiceInfo('/test2', 'I|CMD'), 12,
      env.dis.info))
    .then((ret) => expect(ret).to.equal(null))

    .then(() => DicCmd.invoke(new ServiceInfo('/test2', 'I|CMD'), 12,
      env.dns.url()))
    .then(
      () => { throw 'should fail'; },
      (err) => expect(err).to.be.instanceOf(NotFound));
  });

  it('releases resources', function() {
    return q()
    .then(() => DicCmd.invoke('/test', 12, env.dns.url()))
    .then((ret) => expect(ret).to.equal(null))
    .then(() => expect(DicService.registry.nodes).to.be.empty());
  });
});
