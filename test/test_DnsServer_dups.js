
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  { waitEvent } = require('./utils');

const
  { NodeInfo, DnsServer, DnsClient, DisNode } = require('../src');

describe('DnsServer', function() {
  var env;

  beforeEach(function() {
    env = {};
    env.dns = new DnsServer(null, 0);
    return env.dns.listen();
  });

  afterEach(function() {
    _.invoke(env.dns, 'close');
    _.invoke(env.cli, 'close');

    _.invoke(env.dis, 'close');
    _.invoke(env.disDns, 'close');

    _.invoke(env.dis2, 'close');
    _.invoke(env.disDns2, 'close');
    env = {};
  });

  it('detects dup services', function() {
    env.dis = new DisNode(NodeInfo.local(0, 0, 'TEST'));
    env.dis2 = new DisNode(NodeInfo.local(0, 0, 'TEST2'));

    env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    env.disDns2 = new DnsClient(env.dns._conn.host, env.dns._conn.port);

    env.dis.addService('myvalue', 'I', 12);
    env.dis2.addService('myvalue', 'I', 12);

    return q()
    .then(() => env.disDns.register(env.dis))
    .then(() => env.disDns2.register(env.dis2))
    .then(_.constant(waitEvent(env.dis2, 'duplicate:services')))
    .then((info) => {
      /* events returns duplicate info */
      expect(info).deep.equal([ 'myvalue' ]);
    });
  });

  it('detects dup tasks', function() {
    env.dis = new DisNode(NodeInfo.local(0, 0, 'TEST'));
    env.dis2 = new DisNode(NodeInfo.local(0, 0, 'TEST'));

    env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    env.disDns2 = new DnsClient(env.dns._conn.host, env.dns._conn.port);

    env.dis.addService('myvalue', 'I', 12);
    env.dis2.addService('myvalue', 'I', 12);

    return q()
    .then(() => env.disDns.register(env.dis))
    .then(() => env.disDns2.register(env.dis2))
    .then(_.constant(waitEvent(env.dis2, 'duplicate:taskName')))
    .then((info) => {
      /* events returns duplicate info */
      expect(info).deep.equal(_.pick(env.dis.info,
        [ 'name', 'pid', 'port', 'task' ]));
    })
    .then(_.constant(waitEvent(env.disDns2, 'close')));
  });
});
