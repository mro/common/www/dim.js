// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  debug = require('debug')('test');

function waitEvent(obj,
                   evt,
                   count,
                   timeout) {
  var def = q.defer();
  var ret = [];
  var received = 0;

  var stack = (new Error()).stack;

  count = _.defaultTo(count, 1);
  timeout = _.defaultTo(timeout, 1000);

  var cb = function(e) {
    ret.push(e);
    debug('waitEvent: received:%s', evt);
    /* $FlowIgnore */
    if (++received >= count) { def.resolve((count === 1) ? ret[0] : ret); }
  };
  obj.on(evt, cb);
  return def.promise
  .timeout(timeout)
  .catch((err) => {
    err.stack = stack;
    throw err;
  })
  .finally(function() {
    if (def.promise.isPending()) { def.reject(); }
    obj.removeListener(evt, cb);
  });
}

/**
 * @param  {() => boolean|any} cb
 * @param {string} [msg]
 * @param  {number} [timeout=1000]
 * @return {Q.Promise<any>}
 */
function waitFor(cb, msg = undefined, timeout = 1000) {
  var def = q.defer();
  var resolved = false;
  var err = new Error(msg || 'timeout'); /* create this early to have stacktrace */
  var timer = _.delay(() => {
    resolved = true;
    clearTimeout(nextTimer);
    def.reject(err);
  }, _.defaultTo(timeout, 1000));
  var nextTimer = null;


  function next() {
    if (resolved) { return; }
    try {
      var ret = cb(); /* eslint-disable-line callback-return */
      if (ret) {
        clearTimeout(timer);
        resolved = true;
        def.resolve(ret);
      }
      else {
        nextTimer = _.delay(next, 100);
      }
    }
    catch (e) {
      clearTimeout(timer);
      resolved = true;
      def.reject(_.has(e, 'message') ? e : new Error(e));
    }
  }

  next();
  return def.promise;
}

/**
 * @template T=any
 * @param  {() => T} test
 * @param  {any} value
 * @param  {string} [msg]
 * @param  {number} [timeout]
 * @return {Q.Promise<T>}
 */
function waitForValue(test, value, msg = undefined, timeout = undefined) {
  /** @type {any} */
  var _val;
  return waitFor(() => {
    _val = test();
    return _.isEqual(_val, value);
  }, msg, timeout)
  .catch((err) => {
    const msg = "Invalid result value: " + _.toString(_val) + " != " + value;
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

module.exports = {
  waitEvent, waitFor, waitForValue
};
