
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DicValue, DisNode, ServiceInfo, DnsServer, DnsClient } = require('../src');

describe('DicValue get', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    env.dis.addService('/test', 'I', 42);

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      return cli.register(env.dis)
      .then(() => cli.unref()); /* drop our ref, now it's managed by DisNode */
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    _.forEach(env.clis, (cli) => _.invoke(cli, 'close'));
    env = {};
  });

  it('can get a value', function() {
    return q()
    .then(() => DicValue.get(new ServiceInfo('/test', 'I'), env.dns.url()))
    .then((ret) => expect(ret).to.equal(42))

    .then(() => DicValue.get(new ServiceInfo('/test', 'I', null, env.dis.info)))
    .then((ret) => expect(ret).to.equal(42))

    .then(() => DicValue.get(new ServiceInfo('/test', 'I'), env.dis.info))
    .then((ret) => expect(ret).to.equal(42));
  });

  it('can request a non-existing service', function() {
    return q()
    .then(() => DicValue.get(new ServiceInfo('/test2', 'I'), env.dis.info))
    .then((ret) => expect(ret).to.be.undefined())

    .then(() => DicValue.get(new ServiceInfo('/test2', 'I'), env.dns.url()))
    .then((ret) => expect(ret).to.be.undefined());
  });

  it('releases resources', function() {
    return q()
    .then(() => DicValue.get('/test', env.dns.url()))
    .then((ret) => expect(ret).to.equal(42))
    .then(() => expect(DicValue.registry.nodes).to.be.empty());
  });
});
