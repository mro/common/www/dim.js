// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  { waitEvent } = require('./utils');

const
  { DicValue, DisNode, ServiceInfo, DnsServer, DnsClient } = require('../src');

describe('DicValue dns', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    env.dis.addService('/test', 'I', 42);

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      if (!cli) { throw 'failed to wrap'; }
      env.cli = cli;

      return env.cli.register(env.dis)
      /* drop our ref, now it's managed by DisNode */
      .then(() => env.cli.unref());
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    _.invoke(env.value.dns, 'close');
    _.invoke(env.value, 'release');
    env = {};
  });

  it('can use a dns', function() {
    var cli = DnsClient.wrap(env.dns.url());
    if (!cli) { throw 'failed to wrap'; }

    env.value = new DicValue(new ServiceInfo('/test', 'I'), cli);
    cli.unref(); /* drop our ref */

    return env.value.promise()
    .then((ret) => expect(ret).to.equal(42))

    .then(() => env.value.release())
    .then(() => expect(env.value.dns).to.be.null())
    .then(() => expect(env.value.node).to.be.null());
  });

  it('can use NodeInfo', function() {
    env.value = new DicValue(new ServiceInfo('/test', 'I'), null, env.dis.info);
    return env.value.then((ret) => expect(ret).to.equal(42));
  });

  it('can reconnect a service', function() {
    /* $FlowIgnore: special constructor that omits options part */
    env.value = new DicValue(new ServiceInfo('/test', 'I'), env.dns.url());

    env.cli.ref(); /* prevent dnsClient from dying when DisNode closes */
    return env.value.promise()
    .then((ret) => expect(ret).to.equal(42))
    .then(() => {
      /* remove old service, also unregisters from dnsClient */
      return q()
      .then(() => env.dis.close())
      .then(_.constant(waitEvent(env.dis, 'close')))
      .then(_.constant(waitEvent(env.value, 'value', 1)))
      .then(() => {
        env.dis = new DisNode('127.0.0.1'); /* create a new one */
        env.dis.addService('/test', 'I', 44);

        return env.cli.register(env.dis);
      })
      .then(_.constant(waitEvent(env.value, 'value', 2)))
      .then((ret) => expect(ret).to.deep.equal([ undefined, 44 ]));
    })
    .finally(() => env.cli.unref());
  });
});
