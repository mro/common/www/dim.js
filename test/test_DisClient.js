
const
  q = require('q'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DisClient } = require('../src'),
  utils = require('../src/utils'),
  { TCPServer } = require('../src/Conn'),
  { DicReq, DicRep, DnaNet } = require('../src').packets;

describe('DisClient', function() {
  var srv;
  var conn;

  beforeEach(function() {
    srv = new TCPServer('127.0.0.1');

    var deffered = q.defer();
    conn = deffered.promise;
    srv.once('connect', (conn) => deffered.resolve(conn));
    return srv.listen();
  });

  afterEach(function() {
    srv.close();
    srv = null;

    return conn.then((conn) => conn.close());
  });

  it('can request a value', function() {
    var node = new DisClient(utils.toStrAddr(srv.host) + ':' + srv.port);

    return node.connect()
    .then(() => conn)
    .then((conn) => {
      conn.once('packet', (p) => {
        var net = DnaNet.parse(p);
        expect(net).ok();

        conn.once('packet', (p) => {
          var rep = DicReq.parse(p);
          expect(rep).ok();
          conn.send(new DicRep(Buffer.from('1122', 'hex'), rep.sid));
        });
      });

    })
    .then(() => {
      return node.request('/sample')
      .then((reply) => {
        expect(reply.data.readUInt16LE(0)).equal(0x2211);
      });
    })
    .finally(() => node.close());
  });
});
