
const
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  utils = require('../src/utils');

describe('utils', function() {
  it('can parse an address', function() {
    expect(utils.toIntAddr('128.141.94.119')).to.equal(0x808D5E77);
  });
});
