
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  DisNodeRegistry = require('../src/dic/DisNodeRegistry'),
  { NodeInfo } = require('../src');

describe('DisNodeRegistry', function() {
  it('can get a node', function() {
    var registry = new DisNodeRegistry();

    expect(registry.has('tcp://127.0.0.1:1234')).to.be.false();

    var cli = registry.get('tcp://127.0.0.1:1234');
    expect(cli).ok();
    expect(_.omit(cli.info, [ '_url' ]))
    .to.deep.equal(new NodeInfo({ name: '127.0.0.1', port: 1234 }));

    expect(registry.has('tcp://127.0.0.1:1234')).to.be.true();
    registry.release(cli); /* don't forget to release */
    expect(registry.has('tcp://127.0.0.1:1234')).to.be.false();
  });
});
