
const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  { DicReq, DicRep, DicCmd } = require('../src/packets/DicDis'),
  { Service } = require('../src/Consts');

describe('DicDis', function() {
  it('can create DicReq packets', function() {
    var pkt = new DicReq("/sample/service", DicReq.Type.MONITORED, null, 12);

    var parsed = DicReq.parse(pkt.data());
    expect(parsed).to.deep.equal({
      service: "/sample/service",
      type: DicReq.Type.MONITORED,
      timeout: 0, format: Service.FMT_DEFAULT, stamped: false,
      sid: 12
    });
  });

  it('can create DicReq stamped packets', function() {
    var pkt = new DicReq("/sample/service", DicReq.Type.MONITORED, {
      stamped: true, timeout: 1234, format: 0x20 }, 12);

    var parsed = DicReq.parse(pkt.data());
    expect(parsed).to.deep.equal({
      service: "/sample/service",
      type: DicReq.Type.MONITORED,
      timeout: 2000, /* timeout is rounded up */
      format: 0x20,
      stamped: true,
      sid: 12
    });
  });

  it('can create DicRep packets', function() {
    var pkt = new DicRep(Buffer.from('1234', 'hex'), 12);

    var parsed = DicRep.parse(pkt.data());
    expect(parsed).to.deep.equal({
      data: Buffer.from('1234', 'hex'),
      sid: 12
    });
  });

  it('can create DicRep stamped packets', function() {
    var now = _.now();
    var pkt = new DicRep(Buffer.from('1234', 'hex'), now, 10, 12);

    var parsed = DicRep.parse(pkt.data());
    expect(parsed).to.deep.equal({
      data: Buffer.from('1234', 'hex'),
      quality: 10, timestamp: now,
      sid: 12
    });
  });

  it('can create DicCmd packets', function() {
    var pkt = new DicCmd('/sample/service', 4, null, 12);
    pkt.writer.writeUInt32(0x12345678);

    var parsed = DicReq.parse(pkt.data());
    expect(parsed).to.deep.equal({
      service: "/sample/service",
      type: DicReq.Type.COMMAND,
      timeout: 0, format: Service.FMT_DEFAULT, stamped: false,
      sid: 12, data: Buffer.from('78563412', 'hex')
    });
  });

  it('can create DicCmd packets (with buffer data)', function() {
    var pkt = new DicCmd('/sample/service', Buffer.from('1234', 'hex'), null,
      12);

    var parsed = DicReq.parse(pkt.data());
    expect(parsed).to.deep.equal({
      service: "/sample/service",
      type: DicReq.Type.COMMAND,
      timeout: 0, format: Service.FMT_DEFAULT, stamped: false,
      sid: 12, data: Buffer.from('1234', 'hex')
    });
  });

  it('fails to parse empty packets', function() {
    expect(DicRep.parse(Buffer.from(''))).to.be.null();
    expect(DicRep.parse(Buffer.alloc(DicRep.LEN_MIN))).to.be.null();

    expect(DicReq.parse(Buffer.from(''))).to.be.null();
    expect(DicReq.parse(Buffer.alloc(DicReq.LEN_MIN))).to.be.null();
  });

  it('can create empty packets', function() {
    expect(new DicReq()).to.deep.equal(new DicReq('', 0, null, 1));
    expect(new DicRep()).to.deep.equal(new DicRep(0, 1));
    expect(new DicCmd()).to.deep.equal(new DicCmd('', 0, null, 1));
  });
});
