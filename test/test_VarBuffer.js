
'use strict';

const
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  VarBuffer = require('../src/utils/VarBuffer');

describe('VarBuffer', function() {
  it('can be enlarged on demand', function() {
    var buffer = new VarBuffer(10);
    expect(buffer.reserved()).to.be.equal(10);
    expect(buffer.length).to.be.equal(0);

    const data = Buffer.from("1", "utf8");
    for (let i = 0; i < 15; ++i) {
      buffer.add(data);
    }
    expect(buffer.length).to.be.equal(15);

    buffer.resize(30);
    expect(buffer.length).to.equal(30);
  });

  it('can be used like a Buffer', function() {
    var buffer = new VarBuffer();
    buffer.resize(10);
    expect(buffer.length).to.be.equal(10);

    buffer.writeUInt8(0x11);
    buffer.writeUInt16LE(0x2222, 1);
    buffer.writeUInt32LE(0x33333333, 3);
    buffer.write('444444', 7, 'hex');

    expect(buffer.buffer())
    .to.be.deep.equal(Buffer.from('11222233333333444444', 'hex'));

    // slicing
    buffer = new VarBuffer(Buffer.from('0123456789'));
    expect(buffer.length).to.be.equal(10);

    expect(buffer.slice(3, 7)).to.be.deep.equal(Buffer.from('3456'));
  });

  it('can drop first n bytes', function() {
    var buffer = new VarBuffer(Buffer.from('0123456789'));
    expect(buffer.length).to.be.equal(10);

    expect(buffer.shift(4)).to.be.deep.equal(Buffer.from('0123'));
    expect(buffer.buffer()).to.be.deep.equal(Buffer.from('456789'));
    expect(buffer.length).to.be.equal(6);
    expect(buffer.reserved()).to.be.equal(10);
  });

  it('can be partially copied at a specific offset', function() {
    var bufSource = new VarBuffer(Buffer.from('0123456789'));
    var varBufTarget = new VarBuffer(Buffer.alloc(10, '-'));
    var bufTarget = Buffer.alloc(10, '.');

    bufSource.copy(varBufTarget, 3, 3, 8);
    expect(varBufTarget.buffer()).to.be.deep.equal(Buffer.from('---34567--'));

    bufSource.copy(bufTarget, 2, 0);
    expect(bufTarget).to.be.deep.equal(Buffer.from('..01234567'));
  });
});
