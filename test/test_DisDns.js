
const
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  NodeInfo = require('../src/NodeInfo'),
  ServiceInfo = require('../src/ServiceInfo'),
  { DisDnsReg, DisDnsCmd } = require('../src/packets/DisDns'),
  { Service } = require('../src/Consts');

describe('DisDns', function() {
  it('can create DisDnsReg packets', function() {
    var pkt = new DisDnsReg(NodeInfo.local('127.0.0.1', 1234), [
      new ServiceInfo('/sample', 'C', 1),
      new ServiceInfo('/sample2', 'C', 2)
    ]);
    expect(pkt.isValid()).to.be.true();

    var parsed = DisDnsReg.parse(pkt.data());
    expect(parsed).ok();
    expect(parsed.node).to.deep.include({
      host: 0x7F000001, port: 1234, protocol: 1,
      format: Service.FMT_DEFAULT
    });
    expect(parsed.services).to.deep.equal([
      new ServiceInfo('/sample', 'C', 1, parsed.node),
      new ServiceInfo('/sample2', 'C', 2, parsed.node)
    ]);
    expect(parsed.node.name).to.be.a('string').and.not.be.empty();
    expect(parsed.node.task).to.be.a('string').and.not.be.empty();
  });

  it('can create DisDnsCmd packets', function() {
    var pkt = new DisDnsCmd(DisDnsCmd.Cmd.DUPLICATE, 0,
      Buffer.from('1234', 'hex'));

    var parsed = DisDnsCmd.parse(pkt.data());
    expect(parsed).to.deep.equal({
      cmd: DisDnsCmd.Cmd.DUPLICATE,
      info: 0,
      data: Buffer.from('1234', 'hex')
    });
  });

  it('fails to parse empty packets', function() {
    expect(DisDnsReg.parse(Buffer.from(''))).to.be.null();
    expect(DisDnsReg.parse(Buffer.alloc(DisDnsReg.LEN_MIN))).to.be.null();

    expect(DisDnsCmd.parse(Buffer.from(''))).to.be.null();
    expect(DisDnsCmd.parse(Buffer.alloc(DisDnsCmd.LEN_MIN))).to.be.null();
  });

  it('can create empty packets', function() {
    expect(new DisDnsReg()).to.deep.equal(new DisDnsReg(new NodeInfo(), []));
    expect(new DisDnsCmd()).to.deep.equal(new DisDnsCmd(0, []));
  });

  it('can create DUP packets', function() {
    var info = new NodeInfo({
      host: 0x11223344, port: 4242,
      name: 'test.node', task: 'test.node.task',
      pid: 5454,
      protocol: 1,
      format: Service.FMT_DEFAULT });

    var dup = DisDnsCmd.createDup(1, info, [ 5678, 91011 ]);
    expect(dup.isValid()).to.be.true();
    expect(DisDnsCmd.parse(dup.data())).to.deep.equal({
      cmd: DisDnsCmd.Cmd.DUPLICATE,
      info: 1,
      data: { port: 4242, name: 'test.node', task: 'test.node.task', pid: 5454,
        services: [ 5678, 91011 ] }
    });
  });
});
