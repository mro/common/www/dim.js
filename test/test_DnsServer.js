// @ts-check
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  sinon = require('sinon'),

  { waitEvent, waitForValue, waitFor } = require('./utils');

const
  { ServiceInfo, NodeInfo, DnsServer, DnsClient, DisNode } = require('../src');

describe('DnsServer', function() {
  var env;

  beforeEach(function() {
    env = {};
  });

  afterEach(function() {
    _.invoke(env.dns, 'close');
    _.invoke(env.cli, 'close');
    _.invoke(env.dis, 'close');
    _.invoke(env.disDns, 'close');
    env = {};
  });

  it('can start/stop a server', function() {
    env.dns = new DnsServer(null, 0);
    return env.dns.listen()
    .then(() => env.dns.close())
    .then(_.constant(waitEvent(env.dns, 'close')));
  });

  it('can request a non-existing service', function() {
    env.dns = new DnsServer(null, 0);
    return env.dns.listen()
    .then(() => {
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      return env.cli.connect();
    })
    .then(() => env.cli.query('TEST'))
    .then((info) => {
      expect(info)
      .to.deep.equal(new ServiceInfo('TEST', null, 1, new NodeInfo()));
    });
  });

  it('detects when a service is available', function() {
    env.dns = new DnsServer(null, 0);
    env.dis = new DisNode('127.0.0.1');
    env.dis.addService('TEST', 'I', 42);
    return env.dns.listen()
    .then(() => {
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      return env.cli.connect();
    })
    .then(() => env.cli.query('TEST'))
    .then((info) => {
      expect(info)
      .to.deep.equal(new ServiceInfo('TEST', null, 1, new NodeInfo()));

      return env.dis.register(env.dns.url())
      .then(_.constant(waitEvent(env.cli, 'service:TEST')))
      .then((ret) => expect(ret).to.have.property('node').not.null());
    });
  });

  it('can register services', function() {
    var node;
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => {
      env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    })
    .then(() => env.disDns.register(env.dis))
    /* wait for the dns server to send an event */
    .then(_.constant(waitEvent(env.dns, 'update')))
    /* request newly registered service */
    .then(() => env.cli.query('test'))
    .then((info) => {
      expect(info).to.have.property('name', 'test');
      expect(_.toString(info.definition)).to.equal('I');
      expect(info.node).to.have.property('host', 0x7F000001);
      if (!info.node) { throw 'fail'; }
      expect(info.node.port).to.be.greaterThan(0);
      node = info.node;
    })
    .then(() => {
      env.dis.addService('test2', 'I', 44);
      expect(env.dis.removeService('test')).to.be.true();
    })
    .then(_.constant(waitEvent(env.dns, 'update', 3)))
    .then(() => env.cli.query('test2'))
    .then((info) => {
      expect(info).to.have.property('name', 'test2');
      expect(info.node).to.deep.equal(node);
    })
    .then(() => env.cli.query('test'))
    .then((info) => {
      expect(info).to.have.property('name', 'test');
      if (!info.node) { throw 'fail'; }
      expect(info.node.isValid()).to.be.false();
    });
  });

  it('can register services containing dots', function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1', null,
      NodeInfo.local('127.0.0.1', null, 'loc.al.host'));

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => {
      return q()
      .then(() => env.dis.listen())
      .then(() => env.dis.register(env.dns.url()))
      .then(_.constant(waitEvent(env.dns, 'update')))
      .then(() => {
        expect(env.dns.serviceInfo('loc.al.host/SERVICE_LIST')).to.exist();
      });
    });
  });

  it('can unregister services', function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => {
      env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    })
    .then(() => {
      return env.disDns.register(env.dis)
      .then(_.constant(waitEvent(env.dns, 'update')))
      .then(() => env.cli.query('test')) /* $FlowIgnore */
      .then((info) => expect(info.node.isValid()).to.be.true())

      .then(() => env.disDns.unregister(env.dis))
      .then(_.constant(waitEvent(env.cli, 'service:test', 2)))
      .then(() => env.cli.query('test'))
      .then((info) => {
        if (!info.node) { throw 'fail'; }
        expect(info.node.isValid()).to.be.false();
      });
    });
  });

  it('services are unregistered on DisNode close', function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => {
      env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    })
    .then(() => {
      return env.disDns.register(env.dis)
      .then(_.constant(waitEvent(env.dns, 'update')))
      .then(() => env.dis.close())
      .then(_.constant(waitEvent(env.dns, 'update', 12))) /* 5 builtin + 1 */
      .then(() => env.cli.query('test')) /* $FlowIgnore */
      .then((info) => expect(info.node.isValid()).to.be.false());
    });
  });

  it('services are unregistered when DnsClient disconnect', function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => {
      env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);
      env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    })
    .then(() => {
      return env.disDns.register(env.dis)
      .then(_.constant(waitEvent(env.dns, 'update')))
      .then(() => env.cli.query('test')) /* $FlowIgnore */
      .then((info) => expect(info.node.isValid()).to.be.true())
      .then(() => env.disDns.close())
      .then(_.constant(waitEvent(env.cli, 'service:test', 2)))
      .then(() => env.cli.query('test')) /* $FlowIgnore */
      .then((info) => expect(info.node.isValid()).to.be.false());
    });
  });

  it('disconnects DisNode on close request', function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    env.dis.addService('test', 'I', 42);
    return env.dns.listen()
    .then(() => env.dis.register(env.dns.url()))
    /* $FlowIgnore: is a q$Promise */
    .delay(100)
    .then(() => env.dns.close())
    .then(_.constant(waitEvent(env.dns, 'close')));
  });

  it('can refresh subscription', async function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');
    env.dis.addService('TEST', 'I', 42);

    await env.dns.listen();
    env.cli = new DnsClient(env.dns._conn.host, env.dns._conn.port);
    /*
    duplicate notifications are filtered by DnsClient, so let's hook its
    packet handling method
    */
    const spy = sinon.spy(env.cli, '_onDicPacket');
    await env.cli.connect();

    await env.cli.query('TEST');
    await waitForValue(() => spy.callCount, 1, 'waiting for 1');

    env.cli.refreshQuery('TEST');
    await waitForValue(() => spy.callCount, 2, 'waiting for 2');

    await env.dis.register(env.dns.url());
    await waitForValue(() => spy.callCount, 3, 'waiting for 3', 250);
    await (waitFor(() => (spy.callCount > 4), 'not more than 3 packets', 250)
    .then(
      () => { throw 'only 3 packets should be received'; },
      _.noop));
  });
});
