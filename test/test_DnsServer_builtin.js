// @ts-check
const
  _ = require('lodash'),
  q = require('q'),
  { expect } = require('chai'),
  { describe, it, afterEach, beforeEach } = require('mocha');

const
  { DnsServer, DicDns, ServiceDefinition, DisNode, DnsClient,
    DicCmd, DicValue } = require('../src');

describe('DnsServer built-in', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer();
    return env.dns.listen();
  });

  afterEach(function() {
    _.invoke(env.dns, 'close');
    _.invoke(env.dis, 'close');
    _.invoke(env.disDns, 'close');
    env = {};
  });

  it('serves SERVICE_INFO', function() {
    return DicDns.serviceInfo('*', env.dns.url())
    .then((ret) => {
      expect(ret).to.deep.equal({
        'DIS_DNS/SERVER_LIST': ServiceDefinition.parse('C'),
        'DIS_DNS/KILL_SERVERS': ServiceDefinition.parse('I|CMD'),
        'DIS_DNS/SERVICE_INFO': ServiceDefinition.parse('C,C|RPC')
      });
    });
  });

  it('services SERVICE_INFO when unknown services are registered', function() {
    var cli = DnsClient.wrap(env.dns.url());
    /* request an unknown service */
    return cli.query('notexist')
    .finally(() => cli.unref())
    /* now list services */
    .then(() => DicDns.serviceInfo('*', env.dns.url()))
    .then((ret) => {
      expect(ret).to.deep.equal({
        'DIS_DNS/SERVER_LIST': ServiceDefinition.parse('C'),
        'DIS_DNS/KILL_SERVERS': ServiceDefinition.parse('I|CMD'),
        'DIS_DNS/SERVICE_INFO': ServiceDefinition.parse('C,C|RPC')
      });
    });
  });

  it('supports wildcard prefix', function() {
    return DicDns.serviceInfo('*_LIST', env.dns.url())
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/SERVER_LIST': ServiceDefinition.parse('C')
    }));
  });

  it('supports wildcard postfix', function() {
    return DicDns.serviceInfo('DIS_DNS/SERV*', env.dns.url())
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/SERVER_LIST': ServiceDefinition.parse('C'),
      'DIS_DNS/SERVICE_INFO': ServiceDefinition.parse('C,C|RPC')
    }));
  });

  it('supports wildcard prefix and postfix', function() {
    return DicDns.serviceInfo('*_DNS/SERVICE*', env.dns.url())
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/SERVICE_INFO': ServiceDefinition.parse('C,C|RPC')
    }));
  });

  it('supports fullname', function() {
    return DicDns.serviceInfo('DIS_DNS/KILL_SERVERS', env.dns.url())
    .then((ret) => expect(ret).to.deep.equal({
      'DIS_DNS/KILL_SERVERS': ServiceDefinition.parse('I|CMD')
    }));
  });

  it('can kill servers', async function() {
    env.dis = new DisNode();
    env.dis.addService('test', 'I', 42);
    env.disDns = new DnsClient(env.dns._conn.host, env.dns._conn.port);

    env.disDns.register(env.dis);
    var def = q.defer();
    env.dis.once('exit', () => def.resolve());
    DicCmd.invoke('DIS_DNS/KILL_SERVERS', [], env.dns.url());
    await def.promise;

    const value = await DicValue.get('DIS_DNS/SERVER_LIST', env.dns.url());
    return expect(value).to.be.empty();
  });
});
