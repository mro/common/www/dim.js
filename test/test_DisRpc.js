
const
  q = require('q'),
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  { DicRpc, DisNode, ServiceInfo, DnsServer, DnsClient } = require('../src');

describe('DisRpc', function() {
  var env = {};

  beforeEach(function() {
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode('127.0.0.1');

    return env.dns.listen()
    .then(() => {
      var cli = DnsClient.wrap(env.dns.url());
      return cli.register(env.dis)
      .then(() => cli.unref()); /* drop our ref, now it's managed by DisNode */
    });
  });

  afterEach(function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dns, 'close');
    env = {};
  });

  it('can return a promise', function() {
    env.dis.addRpc('/test', 'I,I|RPC', (req) => q(req.value + 1).delay(100));
    return q()
    .then(() => DicRpc.invoke(new ServiceInfo('/test', 'I,I|RPC'), 12,
      env.dis.info))
    .then((ret) => expect(ret).to.have.property('value', 13));
  });
});
