const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  { waitEvent, waitForValue } = require('./utils'),
  { DicDis, DnsServer, DisNode, NodeInfo,
    DnsClient, ServiceDefinition } = require('../src');


describe('DicServicesList', function() {
  var env = {};

  beforeEach(async function() {
    env.servicesList = {
      "TEST/SERVICE_LIST": ServiceDefinition.parse('C'),
      "TEST/VERSION_NUMBER": ServiceDefinition.parse('L:1'),
      "TEST/CLIENT_LIST": ServiceDefinition.parse('C'),
      "TEST/SET_EXIT_HANDLER": ServiceDefinition.parse('L:1|CMD'),
      "TEST/EXIT": ServiceDefinition.parse('L:1|CMD')
    };
    env.dns = new DnsServer('127.0.0.1');
    env.dis = new DisNode(NodeInfo.local('127.0.0.1', 0, 'TEST'));
    await env.dns.listen();
    const cli = DnsClient.wrap(env.dns.url());
    await cli.register(env.dis);
    cli.unref();
  });

  afterEach(async function() {
    _.invoke(env.dis, 'close');
    _.invoke(env.dis2, 'close');
    _.invoke(env.dns, 'close');
    _.invoke(env.cli, 'release');
    env = {};
  });

  it('can retrieve a services list', async function() {
    const list = await DicDis.serviceList('TEST', env.dns.url());
    expect(list).to.be.deep.equal(env.servicesList);
  });

  it('can monitor a services list', async function() {
    env.cli = new DicDis.DicServicesList('TEST', env.dns.url());

    let ret = await env.cli.promise();
    expect(ret).to.be.deep.equal(env.servicesList);

    env.cli.on('value', (val) => { ret = val; });
    /* add service */
    env.servicesList['test'] = ServiceDefinition.parse('I');
    env.dis.addService('test', 'I', 123);

    await waitEvent(env.cli, 'value');
    expect(ret).to.be.deep.equal(env.servicesList);
  });

  it('can handle service updates', async function() {
    env.cli = new DicDis.DicServicesList('TEST', env.dns.url());

    const values = [];
    env.cli.on('value', (val) => values.push(val));

    ret = await env.cli.promise();
    env.dis.addService('test', 'I', 123);
    env.dis.addService('test2', 'I', 123);

    await waitForValue(() => _.size(env.cli.value),
      _.size(env.servicesList) + 2, '2 services');

    env.dis.removeService('test');
    await waitForValue(() => _.size(env.cli.value),
      _.size(env.servicesList) + 1, '1 service');

    /* add service */
    env.servicesList['test2'] = ServiceDefinition.parse('I');
    expect(env.cli.value).to.be.deep.equal(env.servicesList);

    // this implementation sends 1 signal per add/remove operation
    expect(values.length).to.equal(4, '4 updates received');
  });
});
