const
  { isEqual } = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const
  ServiceInfo = require('../src/ServiceInfo'),
  NodeInfo = require('../src/NodeInfo');

describe('ServiceInfo', function() {
  it('can compare ServiceInfo', function() {
    /* as achieved by DnsClient */
    const reference = new ServiceInfo("TEST", "I", 12,
      NodeInfo.local('127.0.0.1', 1234, 'test'));

    /*
    using isEqual on purpose:
    expect().to.deep.equal would consider inherited properties when
    isEqual doesn't
    */
    expect(isEqual(reference,
      new ServiceInfo("TEST", "I", 12,
        NodeInfo.local('127.0.0.1', 1234, 'test'))))
    .to.equal(true);

    expect(isEqual(reference,
      new ServiceInfo("TEST", "I", 13, reference.node)))
    .to.equal(false);

    expect(isEqual(reference,
      new ServiceInfo("TEST", "I:2", 12, reference.node)))
    .to.equal(false);

    expect(isEqual(reference,
      new ServiceInfo("TEST", "I", 12)))
    .to.equal(false);

    expect(isEqual(reference,
      new ServiceInfo("TEST", "I", 12,
        NodeInfo.local('127.0.0.1', 1236, 'test'))))
    .to.equal(false);
  });
});
